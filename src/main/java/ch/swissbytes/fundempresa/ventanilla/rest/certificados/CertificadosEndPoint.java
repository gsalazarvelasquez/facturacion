package ch.swissbytes.fundempresa.ventanilla.rest.certificados;

import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import ch.swissbytes.fundempresa.ventanilla.data.model.TipoCertificado;
import ch.swissbytes.fundempresa.ventanilla.services.CertificadosService;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("/forms/certificados")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
public class CertificadosEndPoint {

    @Inject
    private CertificadosService certificadosService;

    @GET
    @Path("/getTipoCertificadoListByTipo/{tipo}")
    public JsonResult getTipoCertificadoListByTipo(@PathParam("tipo") String tipo){
        JsonResult result = new JsonResult();
        try {
            List<TipoCertificado> list = certificadosService.getTipoCertificadoList(tipo);
            result.setSuccess(true);
            result.setData(list);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

//    @POST
//    @Path("/especiales")
////    public JsonResult especiales(CertificadosRequestDto requestDto) {
//    public JsonResult especiales() {
//        /**************************         DATOS DE PRUEBA     ***********************/
//        CertificadosRequestDto requestDto = new CertificadosRequestDto();
//        requestDto.setUserName("PREPBYTE");
////        requestDto.setSolicitante("Jose Carlos");
////        requestDto.setCi("12345678");
////        requestDto.setExpci("sc");
////        requestDto.setTelefono("76620400");
////        requestDto.setDireccion("Av. Banzer 7mo anillo");
////        requestDto.setCiudad("Santa Cruz");
////        requestDto.setProvincia("Andres Ibanez");
//
//        requestDto.setMatricula("00131804");
////        requestDto.setMontoPagado("100.00");
//        requestDto.setPayAmount("100.00");
//        requestDto.setEjemplares("2");
//        requestDto.setCertificado("01010308");//01010401=No Inscripcion,01010307=Especial,01010308=Tradicion Comercial
//        requestDto.setDescripcion("Solicito certificado de tradicion comercial...");
//        requestDto.setSucursal("02");
//        /******************************************************************************/
//        JsonResult result = new JsonResult();
//        try {
//            CertificadosResponseDto tramite = certificadosService.especial(requestDto);
//            result.setSuccess(true);
//            result.setData(tramite);
//            result.setMessage("Procesado correctamente");
//        } catch (Exception e) {
//            result.setSuccess(false);
//            result.setData(null);
//            result.setMessage(e.getMessage());
//            e.printStackTrace();
//        }
//        return result;
//    }

    @POST
    @Path("/submitCertificadoEspecial")
    public JsonResult submitCertificadoEspecial(CertificadosRequestDto requestDto) {
        JsonResult result = new JsonResult();
        try {
            CertificadosResponseDto tramite = certificadosService.especial(requestDto);
            result.setSuccess(true);
            result.setData(tramite);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

//    @POST
//    @Path("/enlinea")
////    public JsonResult enlinea(CertificadosRequestDto requestDto) {
//    public JsonResult enlinea() {
//        /**************************         DATOS DE PRUEBA     ***********************/
//        CertificadosRequestDto requestDto = new CertificadosRequestDto();
//        requestDto.setUserName("PREPBYTE");
////        requestDto.setSolicitante("Jose Carlos");
////        requestDto.setCi("12345678");
////        requestDto.setExpci("sc");
////        requestDto.setTelefono("76620400");
////        requestDto.setMontoPagado("100.00");
//        requestDto.setPayAmount("100.00");
//
//        List<CertificadoDto> certificados = new ArrayList<CertificadoDto>();
//        CertificadoDto c = new CertificadoDto();
//        c.setCodigo("01010202");//01010202=INSCRIPCION,01010210=REGISTRO DE DOCUMENTO, 010102001=ACTUALIZACION
//        c.setMatricula("00131804");
//        c.setLibro("21");
//        c.setRegistro("00629439");
//        c.setEjemplares("2");
//        c.setSubtotal("10.00");
//        certificados.add(c);
//
//        c = new CertificadoDto();
//        c.setCodigo("01010210");//01010202=INSCRIPCION,01010210=REGISTRO DE DOCUMENTO, 010102001=ACTUALIZACION
//        c.setMatricula("00131804");
//        c.setLibro("21");
//        c.setRegistro("00629439");
//        c.setEjemplares("1");
//        c.setSubtotal("20.00");
//        certificados.add(c);
//
//        requestDto.setDocumentos(certificados);
//        /******************************************************************************/
//        JsonResult result = new JsonResult();
//        try {
//            CertificadosResponseDto tramite = certificadosService.enlinea(requestDto);
//            result.setSuccess(true);
//            result.setData(tramite);
//            result.setMessage("Procesado correctamente");
//        } catch (Exception e) {
//            result.setSuccess(false);
//            result.setData(null);
//            result.setMessage(e.getMessage());
//            e.printStackTrace();
//        }
//        return result;
//    }

    @POST
    @Path("/submitCertificadoEnLinea")
    public JsonResult submitCertificadoEnLinea(CertificadosRequestDto requestDto) {
        JsonResult result = new JsonResult();
        try {
            CertificadosResponseDto tramite = certificadosService.enlinea(requestDto);
            result.setSuccess(true);
            result.setData(tramite);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/saveDraftEspeciales")
    public JsonResult saveDraftEspeciales(CertificadosRequestDto requestDto) {
        JsonResult result = new JsonResult();
        Long response = null;
        try {
            response = certificadosService.saveDraftEspeciales(requestDto);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/saveDraftEnlinea")
    public JsonResult saveDraftEnlinea(CertificadosRequestDto requestDto) {
        JsonResult result = new JsonResult();
        Long response = null;
        try {
            response = certificadosService.saveDraftEnlinea(requestDto);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/validar")
    public JsonResult validar(CertificadosRequestDto requestDto) {
        JsonResult result = new JsonResult();
        try {
            requestDto.setFechaCertificado(System.currentTimeMillis());
            CertificadosResponseDto tramite = certificadosService.validar(requestDto);
            result.setSuccess(true);
            result.setData(tramite);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }
}