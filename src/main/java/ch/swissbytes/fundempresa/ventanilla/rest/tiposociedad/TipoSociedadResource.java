package ch.swissbytes.fundempresa.ventanilla.rest.tiposociedad;

import ch.swissbytes.fundempresa.ventanilla.data.dto.JsonResult;
import ch.swissbytes.fundempresa.ventanilla.services.TipoSociedadService;
import ch.swissbytes.fundempresa.ventanilla.data.model.TipoSociedad;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.List;


/**
 * Created by jorgeburgos on 2/1/16.
 */
@Path("/forms/tipossociedad")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class TipoSociedadResource {

    @Inject
    public TipoSociedadService tipoSociedadService;

    @GET
    @Path("/getAll")
    public JsonResult getAll() {
        JsonResult result = new JsonResult();
        try {
            List<TipoSociedad> tipoSociedadList = tipoSociedadService.findAll();
            result.setSuccess(true);
            result.setData(tipoSociedadList);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }

        return result;
    }
}
