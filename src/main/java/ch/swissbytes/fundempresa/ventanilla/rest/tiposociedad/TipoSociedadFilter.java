package ch.swissbytes.fundempresa.ventanilla.rest.tiposociedad;

import ch.swissbytes.shared.filter.GenericFilter;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by jorgeburgos on 1/29/16.
 */
@Getter @Setter
public class TipoSociedadFilter extends GenericFilter {
    private String valor;


    public TipoSociedadFilter() {
    }

    public TipoSociedadFilter(String valor) {
        this.valor = valor;

    }

    @Override
    public String toString() {
        return "TipoSociedadFilter{" +
                "valor='" + valor + '\'' +
                '}';
    }
}
