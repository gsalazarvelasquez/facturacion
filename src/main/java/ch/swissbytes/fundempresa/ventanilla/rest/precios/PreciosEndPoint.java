package ch.swissbytes.fundempresa.ventanilla.rest.precios;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import ch.swissbytes.fundempresa.ventanilla.services.PreciosService;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Path("/forms/precios")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
public class PreciosEndPoint {

    @Inject
    public PreciosService preciosService;

    @POST
    @Path("/tramite")
    public JsonResult precioTramite(PreciosRequestDto requestDto) {
        JsonResult result = new JsonResult();
        try {
            PreciosResponseDto arancel = preciosService.tramite(requestDto);
            result.setSuccess(true);
            result.setData(arancel);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/ventabd/listado")
    public JsonResult precioListado() {
//    public JsonResult precioListado(VentaBDListadoDto requestDto) {
        /**************** TESTING REQUEST *************************/
        VentaBDListadoDto requestDto = new VentaBDListadoDto();

        List<String> dptos = new ArrayList<String>();
        dptos.add("01");
        dptos.add("02");
        dptos.add("03");
        dptos.add("04");
        dptos.add("05");
        requestDto.setDepartamentos(dptos);
        List<String> municipios = new ArrayList<String>();
        municipios.add("2345");
        requestDto.setMunicipios(municipios);
        requestDto.setTodosDepartamentos(false);

        List<String> tipSoc = new ArrayList<String>();
        tipSoc.add("01");
        tipSoc.add("07");
        tipSoc.add("08");
        requestDto.setTiposSociedad(tipSoc);
        requestDto.setTodosTiposSociedad(false);

        List<String> estados = new ArrayList<String>();
        estados.add("1");
        estados.add("2");
        requestDto.setEstados(estados);
        requestDto.setTodosEstados(false);
        requestDto.setAnhosInscritas("2014,2015");
        requestDto.setAnhosCanceladas("2015,2016");

        List<String> secciones = new ArrayList<String>();
        secciones.add("A");
        secciones.add("C");
        secciones.add("T");
        requestDto.setSecciones(secciones);
        List<String> clases = new ArrayList<String>();
        clases.add("01");
        clases.add("02");
        clases.add("03");
        requestDto.setClasesCIIU(clases);
        requestDto.setTodasSecciones(false);
        /**********************************************************/
        JsonResult result = new JsonResult();
        try {
            BigDecimal arancel = preciosService.listadoEmpresas(requestDto);
            result.setSuccess(true);
            result.setData(arancel);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/ventabd/precioEstadBasica")
//    public JsonResult precioEstadBasica() {
    public JsonResult precioEstadBasica(VentaBDEstBasicaDto requestDto) {
        /**************************     DATOS DE PRUEBA       *************************/
//        VentaBDEstBasicaDto requestDto = new VentaBDEstBasicaDto();
//        requestDto.setMensuales(true);
//        requestDto.setDepartamentales(true);
//        requestDto.setActEconomica(true);
//        requestDto.setAnuarioEstadistico(true);
//        requestDto.setDatosFinancieros(true);
        /******************************************************************************/
        JsonResult result = new JsonResult();
        try {
            BigDecimal arancel = preciosService.estadisticaBasica(requestDto);
            result.setSuccess(true);
            result.setData(arancel);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/ventabd/crucevar")
    public JsonResult precioCruceVar() {
//    public JsonResult precioCruceVar(VentaBDCruceVarDto requestDto) {
        /**************** TESTING REQUEST *************************/
        VentaBDCruceVarDto requestDto = new VentaBDCruceVarDto();

        //ESTADOS DE MATRICULA
        requestDto.setTodosEstados(false);
        List<String> estados = new ArrayList<String>();
        estados.add("1");
        estados.add("2");
        requestDto.setEstados(estados);
        requestDto.setAnhoInscripcion("2014,2015");
        requestDto.setAnhoRenovacion("2015");
        requestDto.setAnhoCancelacion("2015");

        //DEPARTAMENTOS Y MUNICIPIOS
        requestDto.setTodosDptos(false);
        List<String> dptos = new ArrayList<String>();
        dptos.add("01");
        dptos.add("02");
        dptos.add("03");
        dptos.add("04");
        dptos.add("05");
        requestDto.setDepartamentos(dptos);
        List<String> municipios = new ArrayList<String>();
        municipios.add("2345");
        requestDto.setMunicipios(municipios);

        //TIPOS DE SOCIEDAD
        requestDto.setTodosTipoSoc(false);
        List<String> tipSoc = new ArrayList<String>();
        tipSoc.add("01");
        tipSoc.add("07");
        tipSoc.add("08");
        requestDto.setTiposSociedad(tipSoc);

        //SECCIONES Y CLASES
        requestDto.setTodasSeccion(false);
        List<String> secciones = new ArrayList<String>();
        secciones.add("A");
        secciones.add("C");
        secciones.add("T1");
        requestDto.setSecciones(secciones);
        List<String> clases = new ArrayList<String>();
        clases.add("01");
        clases.add("02");
        clases.add("03");
        requestDto.setClasesCIIU(clases);

        //VARIABLES FINANCIERAS
        List<String> activos = new ArrayList<String>();
        activos.add("1000-2000");
        activos.add("2500-3500");
        requestDto.setVFinancActivos(activos);
        List<String> patrimonios = new ArrayList<String>();
        patrimonios.add("2000-3000");
        patrimonios.add("3500-4500");
        requestDto.setVFinancPatrimonio(patrimonios);
        List<String> ventas = new ArrayList<String>();
        ventas.add("3000-4000");
        ventas.add("4500-5500");
        requestDto.setVFinancVentas(ventas);
        List<String> utilidades = new ArrayList<String>();
        utilidades.add("4000-5000");
        utilidades.add("5500-6500");
        requestDto.setVFinancUtilidades(utilidades);
        List<String> capital = new ArrayList<String>();
        capital.add("5000-6000");
        capital.add("6500-7500");
        requestDto.setVFinancCapital(capital);
        /**********************************************************/
        JsonResult result = new JsonResult();
        try {
            BigDecimal arancel = preciosService.cruceVariables(requestDto);
            result.setSuccess(true);
            result.setData(arancel);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }
}
