package ch.swissbytes.fundempresa.ventanilla.rest;

import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/user")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces({MediaType.APPLICATION_JSON})
public class HomeEndPoint {

    @GET
    @Path("/login/{username}/{password}")
    public String adabasd(@PathParam("username") String username, @PathParam("password") String password) {
        boolean b = false;
        try{
            b = AdabasD.login(username, password);
        }catch (Exception e){
            e.printStackTrace();
        }
        JsonObject obj = new JsonObject();
        obj.addProperty("login", b);
        return obj.toString();
    }

    @GET
    @Path("/profile/{username}")
    public String profile(@PathParam("username") String username) {
        UserProfileDto b = new UserProfileDto();
        try{
            b = AdabasD.profile(username);
        }catch (Exception e){
            e.printStackTrace();
        }

        Gson gson = new GsonBuilder().create();
        JsonElement x = gson.toJsonTree(b, UserProfileDto.class);

        JsonObject obj = x.getAsJsonObject();
        return obj.toString();
    }
}