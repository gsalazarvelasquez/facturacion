package ch.swissbytes.fundempresa.ventanilla.rest.homonimia;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.ventanilla.data.dto.HomonimiaResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.JsonResult;
import ch.swissbytes.fundempresa.ventanilla.data.dto.HomonimiaRequestDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteResponseDto;
import ch.swissbytes.fundempresa.ventanilla.services.HomonimiaService;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.net.URISyntaxException;

@Path("/forms/homonimia")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class HomonimiaEndPoint {

    @Inject
    private HomonimiaService homonimiaService;

    @POST
    @Path("/ingresarHomonimia")
    public JsonResult ingresarHomonimia(HomonimiaRequestDto homonimiaRequestDto) {
        HomonimiaResponseDto homonimiaResponseDto = null;
        JsonResult result = new JsonResult();
        try {
            homonimiaResponseDto = homonimiaService.ingresarHomonimia(homonimiaRequestDto);
//            homonimiaResponseDto = new HomonimiaResponseDto();
            result.setSuccess(true);
            result.setData(homonimiaResponseDto);
            result.setMessage("Ingreso de tramite satisfactorio");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(homonimiaResponseDto);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/reingresarHomonimia")
    public JsonResult reingresarHomonimia(HomonimiaRequestDto homonimiaRequestDto) {

        HomonimiaResponseDto homonimiaResponseDto = null;
        JsonResult result = new JsonResult();
        try {
            homonimiaResponseDto = homonimiaService.reingresarHomonimia(homonimiaRequestDto);
            result.setSuccess(true);
            result.setData(homonimiaResponseDto);
            result.setMessage("Reingreso de tramite satisfactorio");
        } catch (AdabasCRequestException e) {
            result.setSuccess(false);
            result.setData(homonimiaResponseDto);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }

        return result;
    }

    @POST
    @Path("/saveDraft")
    public JsonResult saveDraft(HomonimiaRequestDto homonimiaRequestDto) {
        JsonResult result = new JsonResult();
        Long response = null;
        try {
            response = homonimiaService.saveDraft(homonimiaRequestDto);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Guardado de estado satisfactorio");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(response);
            result.setMessage("Error al guardar borrador");
            e.printStackTrace();
        }

        return result;
    }

    @GET
    @Path("/getDraft/{pktramite}")
    public JsonResult getDraft(@PathParam("pktramite") Long pktramite) {

        HomonimiaRequestDto homonimiaRequestDto = null;
        JsonResult result = new JsonResult();
        try {
            homonimiaRequestDto = homonimiaService.getDraft(pktramite);
            result.setSuccess(true);
            result.setData(homonimiaRequestDto);
            result.setMessage("Consulta exitosa.");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(homonimiaRequestDto);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }

        return result;
    }

    @GET
    @Path("/getEstadoHomonimia/{codigoTramite}")
    public JsonResult getEstadoHomonimia(@PathParam("codigoTramite") String codigoTramite) {

        HomonimiaResponseDto homonimiaResponseDto = null;
        JsonResult result = new JsonResult();
        try {
            homonimiaResponseDto = homonimiaService.getEstadoHomonimia(codigoTramite);
            result.setSuccess(true);
            result.setData(homonimiaResponseDto);
            result.setMessage("Consulta exitosa.");
        } catch (AdabasCRequestException e) {
            result.setSuccess(false);
            result.setData(homonimiaResponseDto);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }

        return result;
    }

    @GET
    @Path("/redirectTo")
    public Response redirectTo(){
        java.net.URI location = null; //TODO: save the pay code and payamount and state = PAGADO
        try {
            location = new java.net.URI("http://192.168.0.239:8080/fundempresa/#/homonimia/59-2-TIGOMONEY");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return Response.temporaryRedirect(location).build();
    }
}