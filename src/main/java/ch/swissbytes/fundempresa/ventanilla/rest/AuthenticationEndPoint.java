package ch.swissbytes.fundempresa.ventanilla.rest;

import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.JsonResult;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteDraftResponseDto;
import ch.swissbytes.fundempresa.ventanilla.services.AuthenticationService;
import ch.swissbytes.fundempresa.ventanilla.services.TramiteService;
//import ch.swissbytes.shared.security.Identity;
import ch.swissbytes.shared.security.dto.AccountDto;
import ch.swissbytes.shared.security.dto.UsernamePasswordCredential;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by daniel on 04/04/2016.
 */
@Path("/authentication")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AuthenticationEndPoint {

    @Inject
    private AuthenticationService authenticationService;

    @POST
    @Path("/authenticate")
    public JsonResult authenticate(UsernamePasswordCredential credential) {
        JsonResult result = new JsonResult();

        try {
            AccountDto accountDto = authenticationService.authenticate(credential);
            result.setSuccess(true);
            result.setData(accountDto);
            result.setMessage("Consulta realizada satisfactoria.");
        } catch (Exception e){
            result.setSuccess(false);
            result.setData("");
            result.setMessage(e.getMessage());
        }

        return result;
    }

//    @Inject
//    private TramiteService tramiteService;
//
//    @Inject
//    private Identity identity;
//
//    @POST
//    @Path("/authenticate")
//    public JsonResult getAllTramiteList(UsernamePasswordCredential credential) {
////        List<TramiteDraftResponseDto> listaTramitesUsuario = null;
//        JsonResult result = new JsonResult();
//
//        if (!identity.isLoggedIn()){
//            result.setSuccess(true);
//            result.setData("");
//            result.setMessage("Consulta realizada satisfactoria.");
//        } else {
//            result.setSuccess(true);
//            result.setData(identity.getAccountJSON());
//            result.setMessage("Error en la autenticacion.");
//        }
//
//        return result;
//    }
//
//    @GET
//    @Path("/getUserProfile/{username}")
//    public JsonResult getUserProfile(@PathParam("username") String userName) {
//        UserProfileDto userProfileDto = null;
//        JsonResult result = new JsonResult();
//        try {
//            userProfileDto = tramiteService.getUserProfile(userName);
//            result.setSuccess(true);
//            result.setData(userProfileDto);
//            result.setMessage("Consulta realizada satisfactoria.");
//        } catch (Exception e) {
//            result.setSuccess(false);
//            result.setData(userProfileDto);
//            result.setMessage(e.getMessage());
//            e.printStackTrace();
//        }
//
//        return result;
//    }
}
