package ch.swissbytes.fundempresa.ventanilla.rest.tiposociedad;

import ch.swissbytes.fundempresa.ventanilla.data.model.TipoSociedad;
import ch.swissbytes.shared.json.EntityJonConverter;
import ch.swissbytes.shared.json.JsonReader;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.enterprise.context.ApplicationScoped;

/**
 * Created by jorgeburgos on 2/1/16.
 */
@ApplicationScoped
public class TipoSociedadJsonConverter extends EntityJonConverter<TipoSociedad> {


    @Override
    public TipoSociedad convertFrom(String json) {
        final JsonObject jsonObject = JsonReader.readAsJsonObject(json);

        final TipoSociedad tipoSociedad = new TipoSociedad();
        tipoSociedad.setValor(JsonReader.getStringOrNull(jsonObject, "valor"));

        return tipoSociedad;
    }

    @Override
    public JsonElement convertToJsonElement(TipoSociedad tipoSociedad) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", tipoSociedad.getId());
        jsonObject.addProperty("valor", tipoSociedad.getValor());

        return jsonObject;
    }
}
