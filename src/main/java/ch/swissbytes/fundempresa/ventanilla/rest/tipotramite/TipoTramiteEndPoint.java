package ch.swissbytes.fundempresa.ventanilla.rest.tipotramite;

import ch.swissbytes.fundempresa.ventanilla.data.model.TipoTramite;
import ch.swissbytes.fundempresa.ventanilla.services.TipoTramiteService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by daniel on 24/02/2016.
 */
@Stateless
@Path("/forms/tipotramite")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TipoTramiteEndPoint {

    @Inject
    private TipoTramiteService tipoTramiteService;

    @GET
    @Path("/getAllTipoTramite")
    public Response getAllTipoTramite() {
        List<TipoTramite> tipoSociedadList = tipoTramiteService.getAll();
        Response result = Response.ok(tipoSociedadList).build();
        return result;
    }
}