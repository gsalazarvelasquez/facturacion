package ch.swissbytes.fundempresa.ventanilla.rest;

import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.JsonResult;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteDraftResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteResponseDto;
import ch.swissbytes.fundempresa.ventanilla.services.TramiteService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by daniel on 04/04/2016.
 */
@Path("/forms/tramite")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TramiteEndPoint {

    @Inject
    private TramiteService tramiteService;

    @GET
    @Path("/getAllTramiteList/{username}")
    public JsonResult getAllTramiteList(@PathParam("username") String userName) {
        List<TramiteDraftResponseDto> listaTramitesUsuario = null;
        JsonResult result = new JsonResult();
        try {
            listaTramitesUsuario = tramiteService.getAllTramiteList(userName);
            result.setSuccess(true);
            result.setData(listaTramitesUsuario);
            result.setMessage("Consulta realizada satisfactoria.");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(listaTramitesUsuario);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }

        return result;
    }

    @GET
    @Path("/getUserProfile/{username}")
    public JsonResult getUserProfile(@PathParam("username") String userName) {
        UserProfileDto userProfileDto = null;
        JsonResult result = new JsonResult();
        try {
            userProfileDto = tramiteService.getUserProfile(userName);
            result.setSuccess(true);
            result.setData(userProfileDto);
            result.setMessage("Consulta realizada satisfactoria.");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(userProfileDto);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }

        return result;
    }
}
