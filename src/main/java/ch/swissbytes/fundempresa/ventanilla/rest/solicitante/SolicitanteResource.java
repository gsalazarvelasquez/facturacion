package ch.swissbytes.fundempresa.ventanilla.rest.solicitante;

import ch.swissbytes.fundempresa.ventanilla.data.model.Solicitante;
import ch.swissbytes.fundempresa.ventanilla.services.SolicitanteService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by daniel on 16/02/2016.
 */
@Stateless
@Path("/forms/solicitante")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SolicitanteResource {

    @Inject
    private SolicitanteService solicitanteService;

    @GET
    @Path("/getSolicitanteById/{idSolicitante}")
    public Response getSolicitanteById(@PathParam("idSolicitante") Long idSolicitante){
        Solicitante solicitante = solicitanteService.getSolicitanteById(idSolicitante);
        Response result = Response.ok(solicitante).build();

        return result;
    }
}
