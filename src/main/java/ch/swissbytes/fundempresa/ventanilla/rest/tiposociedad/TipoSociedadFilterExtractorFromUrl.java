package ch.swissbytes.fundempresa.ventanilla.rest.tiposociedad;



import ch.swissbytes.shared.filter.AbstractFilterExtractorFromUrl;

import javax.ws.rs.core.UriInfo;

/**
 * Created by jorgeburgos on 2/1/16.
 */
public class TipoSociedadFilterExtractorFromUrl extends AbstractFilterExtractorFromUrl {

    public TipoSociedadFilterExtractorFromUrl(UriInfo uriInfo) {
        super(uriInfo);
    }

    public TipoSociedadFilter getFilter() {
        TipoSociedadFilter tipoSociedadFilter = new TipoSociedadFilter();
        tipoSociedadFilter.setPaginationData(extractPaginationData());
        tipoSociedadFilter.setValor(getUriInfo().getQueryParameters().getFirst("valor"));

        return tipoSociedadFilter;
    }

    @Override
    protected String getDefaultSortField() {
        return "valor";
    }
}
