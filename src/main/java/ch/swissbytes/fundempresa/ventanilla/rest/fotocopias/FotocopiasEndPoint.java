package ch.swissbytes.fundempresa.ventanilla.rest.fotocopias;

import ch.swissbytes.fundempresa.adabasd.dto.SucursalDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import ch.swissbytes.fundempresa.ventanilla.services.FotocopiasService;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/forms/fotocopias")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
public class FotocopiasEndPoint {

    @Inject
    private FotocopiasService fotocopiasService;

    @POST
    @Path("/ingresar")
    public JsonResult ingresar(FotocopiasRequestDto requestDto) {
        /**************************         DATOS DE PRUEBA     ***********************/
//        requestDto = new FotocopiasRequestDto();
//        requestDto.setUserName("PREPBYTE");
//        requestDto.setSolicitante("Jose Carlos");
//        requestDto.setCi("12345678");
//        requestDto.setExpci("sc");
//        requestDto.setTelefono("76620400");
//        requestDto.setMatricula("00131804");
//        requestDto.setMontoPagado("100.00");
//        requestDto.setDescripcion("Solicito Informacion de la empresa...");
//        requestDto.setSucursal("02");
//
//        List<DocumentoFotocopiasDto> documentos = new ArrayList<DocumentoFotocopiasDto>();
//        DocumentoFotocopiasDto d = new DocumentoFotocopiasDto();
//            d.setIdLibro("21");
//            d.setNumRegistro("00629439");
//        documentos.add(d);
//
//        d = new DocumentoFotocopiasDto();
//        d.setIdLibro("21");
//        d.setNumRegistro("00629440");
//        documentos.add(d);
//
//        d = new DocumentoFotocopiasDto();
//        d.setIdLibro("13");
//        d.setNumRegistro("00164959");
//        documentos.add(d);
//
//        requestDto.setDocumentos(documentos);
        /******************************************************************************/
        JsonResult result = new JsonResult();
        try{
            TramiteResponseDto response = fotocopiasService.ingresarFotocopia(requestDto);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

//    @GET
//    @Path("/documentos/{matricula}/{ultimoId}")
//    public JsonResult documentos(@PathParam("matricula") String matricula, @PathParam("ultimoId") String ultimoId) {
//        JsonResult result = new JsonResult();
//        try {
////            FotocopiasResponseDto listado = fotocopiasService.consultaFotocopias(matricula, ultimoId);
//            FotocopiaListDto listado = fotocopiasService.consultaFotocopias(matricula, ultimoId);
//            result.setSuccess(true);
//            result.setData(listado);
//            result.setMessage("Procesado correctamente");
//        } catch (Exception e) {
//            result.setSuccess(false);
//            result.setData(null);
//            result.setMessage(e.getMessage());
//            e.printStackTrace();
//        }
//        return result;
//    }

    @GET
    @Path("/getDocumentosByMatricula/{matricula}/{ultimoId}")
    public JsonResult getDocumentosByMatricula(@PathParam("matricula") String matricula, @PathParam("ultimoId") String ultimoId) {
        JsonResult result = new JsonResult();
        try {
//            List<DocumentoFotocopiasDto> listado = fotocopiasService.consultaFotocopias(matricula, "0").getDocumentos();
            FotocopiaListDto listado = fotocopiasService.consultaFotocopias(matricula, ultimoId);
            result.setSuccess(true);
            result.setData(listado);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/saveDraft")
    public JsonResult saveDraft(FotocopiasRequestDto fotocopiasRequestDto) {
        JsonResult result = new JsonResult();
        Long response = null;
        try {
            response = fotocopiasService.saveDraft(fotocopiasRequestDto);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @GET
    @Path("/getDraft/{pktramite}")
    public JsonResult getDraft(@PathParam("pktramite") Long pktramite) {

        FotocopiasRequestDto requestDto = null;
        JsonResult result = new JsonResult();
        try {
            requestDto = fotocopiasService.getDraft(pktramite);
            result.setSuccess(true);
            result.setData(requestDto);
            result.setMessage("Consulta exitosa.");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(requestDto);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }

        return result;
    }

    @GET
    @Path("/entidad/{matricula}")
    public JsonResult entidad(@PathParam("matricula") String matricula) {
        JsonResult result = new JsonResult();
        try {
//            FotocopiasResponseDto entidad = fotocopiasService.consultaEntidad(matricula);
            EntidadDto entidad = fotocopiasService.consultaEntidad(matricula);
            result.setSuccess(true);
            result.setData(entidad);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @GET
    @Path("/getListaEmpresasByName/{name}/{ultimoId}")
    public JsonResult getListaEmpresasByName(@PathParam("name") String name, @PathParam("ultimoId") String ultimoId) {
        JsonResult result = new JsonResult();
        try {
//            List<EntidadDto> listado = fotocopiasService.consultaEntidades(name, ultimoId).getEntidades();
            EntidadListDto listado = fotocopiasService.consultaEntidades(name, ultimoId);
            result.setSuccess(true);
            result.setData(listado);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

//    @GET
//    @Path("/entidades/listado/{razonsocial}/{ultimoId}")
//    public JsonResult entidades(@PathParam("razonsocial") String razonsocial, @PathParam("ultimoId") String ultimoId) {
//        JsonResult result = new JsonResult();
//        try {
////            FotocopiasResponseDto listado = fotocopiasService.consultaEntidades(razonsocial, ultimoId);
//            EntidadListDto listado = fotocopiasService.consultaEntidades(razonsocial, ultimoId);
//            result.setSuccess(true);
//            result.setData(listado);
//            result.setMessage("Procesado correctamente");
//        } catch (Exception e) {
//            result.setSuccess(false);
//            result.setData(null);
//            result.setMessage(e.getMessage());
//            e.printStackTrace();
//        }
//        return result;
//    }

    @GET
    @Path("/getAllSucursalesList")
    public JsonResult getAllSucursalesList() {
        JsonResult result = new JsonResult();
        try {
//            FotocopiasResponseDto listado = fotocopiasService.sucursales();
            List<SucursalDto> listado = fotocopiasService.sucursales();
            result.setSuccess(true);
            result.setData(listado);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }
}