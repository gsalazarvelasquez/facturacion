package ch.swissbytes.fundempresa.ventanilla.rest.ventabd;

import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import ch.swissbytes.fundempresa.ventanilla.services.VentaBDListadoService;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

@Path("/forms/ventabd/listado")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
public class ListadoEmpresasEndPoint {

    @Inject
    private VentaBDListadoService ventaBDService;

    private static String ADABASC_T005_LISTADO = "VVSERVESL";//CAMBIAR CON LAS CREADAS EN EL T005

    @POST
    @Path("/ingresarSolicitud")
    public JsonResult ingresarSolicitud() {
//    public JsonResult listaempresas(VentaBDListadoDto requestDto) {
        /**************************         DATOS DE PRUEBA     ***********************/
        VentaBDListadoDto requestDto = new VentaBDListadoDto();
        requestDto.setUserName("PREPBYTE");
        requestDto.setUsoInformacion("Motivo por el que solicita la informacion.");
        requestDto.setPayAmount("100.00");
        requestDto.setSucursal("02");

        List<String> dptos = new ArrayList<String>();
        dptos.add("01");
        dptos.add("02");
        dptos.add("03");
        dptos.add("04");
        dptos.add("05");
        dptos.add("06");
        dptos.add("07");
        dptos.add("08");
        requestDto.setDepartamentos(dptos);
        List<String> municipios = new ArrayList<String>();
        municipios.add("2345");
        requestDto.setMunicipios(municipios);
        requestDto.setTodosDepartamentos(false);

        List<String> tipSoc = new ArrayList<String>();
        tipSoc.add("01");
        tipSoc.add("07");
        tipSoc.add("08");
        requestDto.setTiposSociedad(tipSoc);
        requestDto.setTodosTiposSociedad(false);

        List<String> estados = new ArrayList<String>();
        estados.add("1");
        estados.add("2");
        requestDto.setEstados(estados);
        requestDto.setTodosEstados(false);
        requestDto.setAnhosInscritas("2014,2015");
        requestDto.setAnhosCanceladas("2015,2016");

        List<String> secciones = new ArrayList<String>();
        secciones.add("A");
        secciones.add("C");
        secciones.add("T1");
        requestDto.setSecciones(secciones);
        List<String> clases = new ArrayList<String>();
        clases.add("01");
        clases.add("02");
        clases.add("03");
        requestDto.setClasesCIIU(clases);
        requestDto.setTodasSecciones(false);
        /******************************************************************************/
        JsonResult result = new JsonResult();
        try {
            VentaBDResponseDto tramite = ventaBDService.ingresarSolicitud(requestDto, ADABASC_T005_LISTADO);
            result.setSuccess(true);
            result.setData(tramite);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/descargar")
    @Produces("application/vnd.ms-excel")
    public Response descargar(Object anything) {
//    public Response descargar(VentaBDListadoDto requestDto) {
        final VentaBDListadoDto requestDto = new VentaBDListadoDto();
        List<String> dptos = new ArrayList<String>();
        dptos.add("01");
        dptos.add("02");
        dptos.add("03");
        dptos.add("04");
        dptos.add("05");
        requestDto.setDepartamentos(dptos);
        List<String> municipios = new ArrayList<String>();
        municipios.add("2345");
        requestDto.setMunicipios(municipios);
        requestDto.setTodosDepartamentos(false);

        requestDto.setTodosTiposSociedad(true);
        requestDto.setTodosEstados(true);
        requestDto.setAnhosInscritas("2014,2015");

        List<String> secciones = new ArrayList<String>();
        secciones.add("A");
        secciones.add("C");
        requestDto.setSecciones(secciones);
        List<String> clases = new ArrayList<String>();
        clases.add("01");
        requestDto.setClasesCIIU(clases);
        requestDto.setTodasSecciones(false);

        StreamingOutput stream = new StreamingOutput() {
            @Override
            public void write(OutputStream os) throws IOException, WebApplicationException {
                try {
                    ventaBDService.descargar(os, requestDto);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        return Response.ok(stream).build();
    }

    @POST
    @Path("/saveDraft")
    public JsonResult saveDraft(VentaBDListadoDto requestDto) {
        JsonResult result = new JsonResult();
        Long response = null;
        try {
            response = ventaBDService.saveDraft(requestDto);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Guardado de estado satisfactorio");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(response);
            result.setMessage("Error al guardar borrador");
            e.printStackTrace();
        }

        return result;
    }

    @GET
    @Path("/getDraft/{pktramite}")
    public JsonResult getDraft(@PathParam("pktramite") Long pktramite) {

        VentaBDListadoDto response = null;
        JsonResult result = new JsonResult();
        try {
            response = ventaBDService.getDraft(pktramite);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Consulta exitosa.");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(response);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }

        return result;
    }
}