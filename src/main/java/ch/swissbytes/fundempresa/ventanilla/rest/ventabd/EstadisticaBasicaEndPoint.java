package ch.swissbytes.fundempresa.ventanilla.rest.ventabd;

import ch.swissbytes.fundempresa.ventanilla.data.dto.JsonResult;
import ch.swissbytes.fundempresa.ventanilla.data.dto.VentaBDEstBasicaDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.VentaBDResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.model.TipoEstadisticaBasica;
import ch.swissbytes.fundempresa.ventanilla.services.VentaBDEstBasicaService;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/forms/ventabd/estbasica")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
public class EstadisticaBasicaEndPoint {

    @Inject
    private VentaBDEstBasicaService ventaBDService;

    private static String ADABASC_T005_ESTBASICA = "VVSERVEBS";

    @POST
    @Path("/ingresarSolicitud")
    public JsonResult ingresarSolicitud(VentaBDEstBasicaDto requestDto) {
        JsonResult result = new JsonResult();
        try {
            VentaBDResponseDto tramite = ventaBDService.estbasica(requestDto, ADABASC_T005_ESTBASICA);
            result.setSuccess(true);
            result.setData(tramite);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @GET
    @Path("/getAllTipoEstadisticaBasica")
    public JsonResult getAllTipoEstadisticaBasica() {
        JsonResult result = new JsonResult();
        try {
            List<TipoEstadisticaBasica> listado = ventaBDService.getAllTipoEstadisticaBasica();
            result.setSuccess(true);
            result.setData(listado);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/saveDraft")
    public JsonResult saveDraft(VentaBDEstBasicaDto requestDto) {
        JsonResult result = new JsonResult();
        Long response = null;
        try {
            response = ventaBDService.saveDraft(requestDto);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Guardado de estado satisfactorio");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(response);
            result.setMessage("Error al guardar borrador");
            e.printStackTrace();
        }

        return result;
    }

    @GET
    @Path("/getDraft/{pktramite}")
    public JsonResult getDraft(@PathParam("pktramite") Long pktramite) {
        VentaBDEstBasicaDto response = null;
        JsonResult result = new JsonResult();
        try {
            response = ventaBDService.getDraft(pktramite);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Consulta exitosa.");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(response);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }

        return result;
    }
}