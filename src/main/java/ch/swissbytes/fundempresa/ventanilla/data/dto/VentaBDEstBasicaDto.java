package ch.swissbytes.fundempresa.ventanilla.data.dto;

import ch.swissbytes.fundempresa.adabasd.dto.SucursalDto;
import ch.swissbytes.fundempresa.ventanilla.data.model.EstadisticaBasica;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class VentaBDEstBasicaDto extends TramiteRequestDto {

    private String usoInformacion;
    private String descripcion;
    private SucursalDto sucursal;

    private boolean mensuales;
    private boolean departamentales;
    private boolean actEconomica;
    private boolean anuarioEstadistico;
    private boolean datosFinancieros;

    VentaBDEstBasicaDto(EstadisticaBasica entity) {
        this.userName = entity.getUserName();
        this.payCode = entity.getPayCode();
        this.payMode = entity.getPayMode();
        this.payAmount = entity.getPayAmount();

        this.usoInformacion = entity.getInformacionUso();
        this.descripcion = entity.getDescripcionSolicitud();
        this.sucursal = new SucursalDto();
        this.sucursal.setIdSucursal(entity.getSucursal());
    }
}
