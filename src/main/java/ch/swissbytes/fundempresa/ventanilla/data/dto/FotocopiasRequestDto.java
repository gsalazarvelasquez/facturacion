package ch.swissbytes.fundempresa.ventanilla.data.dto;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasDRequestException;
import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.adabasd.dto.SucursalDto;
import ch.swissbytes.fundempresa.ventanilla.data.model.Fotocopia;
import ch.swissbytes.fundempresa.ventanilla.data.model.TipoSociedad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jose on 22/03/2016.
 */
@Getter
@Setter
public class FotocopiasRequestDto extends TramiteRequestDto implements Serializable {

    private String solicitante;
    private String ci;
    private String expci;
    private String telefono;
    private String matricula;
    private List<DocumentoFotocopiasDto> documentos;
    private String descripcion;
    private SucursalDto sucursal;

    public FotocopiasRequestDto() {
    }

    public FotocopiasRequestDto(Fotocopia fotocopia){
        super(fotocopia.getId(), fotocopia.getUserName(), fotocopia.getPayCode(), fotocopia.getPayMode(), fotocopia.getPayAmount());
        this.pkTramite = fotocopia.getId();
        this.matricula = fotocopia.getMatricula();
        this.descripcion = fotocopia.getDescripcion();
        this.sucursal = new SucursalDto();
        this.sucursal.setIdSucursal(fotocopia.getSucursal());

        Gson gson = new GsonBuilder().create();
        Type listType = new TypeToken<ArrayList<DocumentoFotocopiasDto>>() {}.getType();
        this.documentos = gson.fromJson(fotocopia.getDocumentos(), listType);

        this.payCode = fotocopia.getPayCode();
        this.payMode = fotocopia.getPayMode();
        this.payAmount = fotocopia.getPayAmount();
    }
}