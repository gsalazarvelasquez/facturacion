package ch.swissbytes.fundempresa.ventanilla.data.dto;

import ch.swissbytes.fundempresa.ventanilla.data.model.Homonimia;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by daniel on 31/03/2016.
 */
@Getter
@Setter
public class PreciosResponseDto extends TramiteResponseDto {

    private String arancel;
}
