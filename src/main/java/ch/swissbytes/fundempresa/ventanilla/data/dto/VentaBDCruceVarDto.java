package ch.swissbytes.fundempresa.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class VentaBDCruceVarDto extends TramiteResponseDto {

    //PARA INGRESAR SOLICITUD
    private String username;
    private String usoInformacion;
    private String montoPagado;
    private String sucursal;

    //PARA CALCULAR PRECIO
    private List<String> estados;
    private List<String> departamentos;
    private List<String> municipios;
    private List<String> tiposSociedad;
    private List<String> secciones;
    private List<String> clasesCIIU;

    private List<String> vFinancActivos;
    private List<String> vFinancPatrimonio;
    private List<String> vFinancVentas;
    private List<String> vFinancUtilidades;
    private List<String> vFinancCapital;

    private boolean todosEstados; //seleccionado padron
    private boolean todosDptos;
    private boolean todosTipoSoc;
    private boolean todasSeccion;

    private String anhoInscripcion;
    private String anhoRenovacion;
    private String anhoCancelacion;

}
