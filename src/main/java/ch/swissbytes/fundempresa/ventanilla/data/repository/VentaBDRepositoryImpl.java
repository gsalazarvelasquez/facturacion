package ch.swissbytes.fundempresa.ventanilla.data.repository;

import ch.swissbytes.fundempresa.ventanilla.data.dto.VentaBDListadoDto;
import ch.swissbytes.fundempresa.ventanilla.data.model.TipoSociedad;
import ch.swissbytes.fundempresa.ventanilla.data.model.VentabdParametros;
import ch.swissbytes.fundempresa.ventanilla.rest.tiposociedad.TipoSociedadFilter;
import ch.swissbytes.shared.filter.PaginatedData;
import ch.swissbytes.shared.persistence.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by daniel on 11/02/2016.
 */
public class VentaBDRepositoryImpl extends Repository {

    public Optional<VentabdParametros> getConfig() {
        return getById(VentabdParametros.class, 1L);
    }

    @Override
    protected Class getPersistentClass() {
        return this.getClass();
    }
}
