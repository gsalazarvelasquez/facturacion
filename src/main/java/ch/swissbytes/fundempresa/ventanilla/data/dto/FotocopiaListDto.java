package ch.swissbytes.fundempresa.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by daniel on 27/04/2016.
 */
@Getter
@Setter
public class FotocopiaListDto {

    private String ultimoId;
    private List<DocumentoFotocopiasDto> documentos;
}
