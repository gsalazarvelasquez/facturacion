package ch.swissbytes.fundempresa.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jose on 28/03/2016.
 */
@Getter
@Setter
public class VentaBDVariablesAdabasDto {

    private String variable;
    private String valor;

}
