package ch.swissbytes.fundempresa.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class VentaBDListadoDto extends TramiteRequestDto {

    //PARA INGRESAR SOLICITUD
    private String usoInformacion;
    private String sucursal;

    //PARA CALCULAR PRECIO
    private List<String> departamentos;
    private List<String> municipios;
    private List<String> tiposSociedad;
    private List<String> estados;
    private List<String> secciones;
    private List<String> clasesCIIU;

    private boolean todosDepartamentos;
    private boolean todosTiposSociedad;
    private boolean todosEstados; //seleccionado padron
    private boolean todasSecciones;

    private String anhosInscritas;
    private String anhosCanceladas;

}
