package ch.swissbytes.fundempresa.ventanilla.data.repository;

import ch.swissbytes.fundempresa.ventanilla.data.dto.HomonimiaResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.model.Homonimia;
import ch.swissbytes.fundempresa.ventanilla.data.model.TipoSociedad;
import ch.swissbytes.shared.persistence.Repository;

import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.*;

@Stateless
public class HomonimiaRepositoryImpl extends Repository {

    public List<Homonimia> getAllListaPendientesByUser(String userName){

        Query query = em.createQuery("SELECT homonimia FROM Homonimia homonimia " +
                "WHERE homonimia.userName = :userName " +
                "AND (homonimia.estadoTramite = :estadoAdabas OR homonimia.estadoTramite = :estadoPago)" +
                "ORDER BY homonimia.fechaCreacion DESC");
        query.setParameter("userName", userName);
        query.setParameter("estadoAdabas", HomonimiaResponseDto.EstadoTramite.EN_PROCESO.name());
        query.setParameter("estadoPago", HomonimiaResponseDto.EstadoTramite.PENDIENTE_PAGO.name());

        List<Homonimia> result = query.getResultList();

        return result;
    }

    public List<Homonimia> getListaEnProcesoByUser(String userName){

        Query query = em.createQuery("SELECT homonimia FROM Homonimia homonimia " +
                "WHERE homonimia.userName = :userName " +
                "AND homonimia.estadoTramite = :estadoAdabas");
        query.setParameter("userName", userName);
        query.setParameter("estadoAdabas", HomonimiaResponseDto.EstadoTramite.EN_PROCESO.name());

        List<Homonimia> result = query.getResultList();

        return result;
    }

    public List<Homonimia> getAllOrderByCreationDate(){
        List<Homonimia> homonimiaList = this.findAll(Homonimia.class);
        Collections.sort(homonimiaList, new Comparator<Homonimia>() {
            @Override
            public int compare(Homonimia o1, Homonimia o2) {
                return o2.getFechaCreacion() != null && o1.getFechaCreacion() != null
                        ? o2.getFechaCreacion().compareTo(o1.getFechaCreacion())
                        : o2.getPkHomonimia().compareTo(o1.getPkHomonimia());
            }
        });

        return homonimiaList;
    }

    public Homonimia getByPk(Long pk){
        Homonimia result = this.getBy(Homonimia.class, "pkHomonimia", pk).get();
        return result;
    }

    @Override
    protected Class getPersistentClass() {
        return this.getClass();
    }
}
