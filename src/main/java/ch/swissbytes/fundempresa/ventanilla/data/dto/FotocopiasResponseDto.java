package ch.swissbytes.fundempresa.ventanilla.data.dto;

import ch.swissbytes.fundempresa.adabasd.dto.SucursalDto;
import ch.swissbytes.fundempresa.ventanilla.data.model.Fotocopia;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by daniel on 31/03/2016.
 */
@Getter
@Setter
public class FotocopiasResponseDto extends TramiteResponseDto {

    private String matricula;
    private String nombreEmpresa;
    private String ultimoId;
    private String estadoMatricula;

    private List<DocumentoFotocopiasDto> documentos;
    private List<EntidadDto> entidades;
    private List<SucursalDto> sucursales;

    public FotocopiasResponseDto(Fotocopia fotocopia){
        this.setPkTramite(fotocopia.getId());
        this.setCodigoTramite(fotocopia.getCodigoTramite());
        this.setCodigoError(fotocopia.getCodigoError());
        this.setMensajeError(fotocopia.getMensajeError());
        this.setEstadoTramite(fotocopia.getEstadoTramite());
        this.setRequestXml(fotocopia.getRequestXml());
        this.setResponseXml(fotocopia.getResponseXml());
    }
}
