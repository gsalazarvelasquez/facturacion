package ch.swissbytes.fundempresa.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jose on 28/03/2016.
 */
@Getter
@Setter
public class CertificadoDto {

    private String codigo; //codigo certificado en linea
    private String matricula;
    private String libro; //id-libro
    private String registro; //numRegistro
    private String ejemplares; //siempre uno
    private String subtotal; //0
    private long codSeguridad; //0

    private String codImpresion; //respuesta
    private String pinImpresion; //respuesta

}
