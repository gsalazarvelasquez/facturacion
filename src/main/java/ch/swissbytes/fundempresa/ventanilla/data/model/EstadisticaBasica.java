package ch.swissbytes.fundempresa.ventanilla.data.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by daniel on 04/05/2016.
 */
@Entity
@Table(name = "estadistica_basica", schema = "fundempresa_local", catalog = "")
public class EstadisticaBasica {
    private Long id;
    private String informacionUso;
    private String descripcionSolicitud;
    private String sucursal;
    private String variables;
    private String userName;
    private String payAmount;
    private String payCode;
    private String payMode;
    private String requestXml;
    private String responseXml;
    private String codigoTramite;
    private String codigoError;
    private String mensajeError;
    private String estadoTramite;
    private Timestamp fechaCreacion;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "informacion_uso", nullable = true, length = 128)
    public String getInformacionUso() {
        return informacionUso;
    }

    public void setInformacionUso(String informacionUso) {
        this.informacionUso = informacionUso;
    }

    @Basic
    @Column(name = "descripcion_solicitud", nullable = true, length = 256)
    public String getDescripcionSolicitud() {
        return descripcionSolicitud;
    }

    public void setDescripcionSolicitud(String descripcionSolicitud) {
        this.descripcionSolicitud = descripcionSolicitud;
    }

    @Basic
    @Column(name = "sucursal", nullable = true, length = 64)
    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    @Basic
    @Column(name = "variables", nullable = true, columnDefinition = "Text")
    public String getVariables() {
        return variables;
    }

    public void setVariables(String variables) {
        this.variables = variables;
    }

    @Basic
    @Column(name = "user_name", nullable = true, length = 128)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "pay_amount", nullable = true, length = 128)
    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    @Basic
    @Column(name = "pay_code", nullable = true, length = 128)
    public String getPayCode() {
        return payCode;
    }

    public void setPayCode(String payCode) {
        this.payCode = payCode;
    }

    @Basic
    @Column(name = "pay_mode", nullable = true, length = 128)
    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    @Basic
    @Column(name = "request_xml", nullable = true, columnDefinition = "Text")
    public String getRequestXml() {
        return requestXml;
    }

    public void setRequestXml(String requestXml) {
        this.requestXml = requestXml;
    }

    @Basic
    @Column(name = "response_xml", nullable = true, columnDefinition = "Text")
    public String getResponseXml() {
        return responseXml;
    }

    public void setResponseXml(String responseXml) {
        this.responseXml = responseXml;
    }

    @Basic
    @Column(name = "codigo_tramite", nullable = true, length = 128)
    public String getCodigoTramite() {
        return codigoTramite;
    }

    public void setCodigoTramite(String codigoTramite) {
        this.codigoTramite = codigoTramite;
    }

    @Basic
    @Column(name = "codigo_error", nullable = true, length = 128)
    public String getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(String codigoError) {
        this.codigoError = codigoError;
    }

    @Basic
    @Column(name = "mensaje_error", nullable = true, length = 128)
    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    @Basic
    @Column(name = "estado_tramite", nullable = true, length = 128)
    public String getEstadoTramite() {
        return estadoTramite;
    }

    public void setEstadoTramite(String estadoTramite) {
        this.estadoTramite = estadoTramite;
    }

    @Basic
    @Column(name = "fecha_creacion", nullable = true)
    public Timestamp getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Timestamp fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EstadisticaBasica that = (EstadisticaBasica) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (informacionUso != null ? !informacionUso.equals(that.informacionUso) : that.informacionUso != null)
            return false;
        if (descripcionSolicitud != null ? !descripcionSolicitud.equals(that.descripcionSolicitud) : that.descripcionSolicitud != null)
            return false;
        if (sucursal != null ? !sucursal.equals(that.sucursal) : that.sucursal != null) return false;
        if (variables != null ? !variables.equals(that.variables) : that.variables != null) return false;
        if (userName != null ? !userName.equals(that.userName) : that.userName != null) return false;
        if (payAmount != null ? !payAmount.equals(that.payAmount) : that.payAmount != null) return false;
        if (payCode != null ? !payCode.equals(that.payCode) : that.payCode != null) return false;
        if (payMode != null ? !payMode.equals(that.payMode) : that.payMode != null) return false;
        if (requestXml != null ? !requestXml.equals(that.requestXml) : that.requestXml != null) return false;
        if (responseXml != null ? !responseXml.equals(that.responseXml) : that.responseXml != null) return false;
        if (codigoTramite != null ? !codigoTramite.equals(that.codigoTramite) : that.codigoTramite != null)
            return false;
        if (codigoError != null ? !codigoError.equals(that.codigoError) : that.codigoError != null) return false;
        if (mensajeError != null ? !mensajeError.equals(that.mensajeError) : that.mensajeError != null) return false;
        if (estadoTramite != null ? !estadoTramite.equals(that.estadoTramite) : that.estadoTramite != null)
            return false;
        if (fechaCreacion != null ? !fechaCreacion.equals(that.fechaCreacion) : that.fechaCreacion != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (informacionUso != null ? informacionUso.hashCode() : 0);
        result = 31 * result + (descripcionSolicitud != null ? descripcionSolicitud.hashCode() : 0);
        result = 31 * result + (sucursal != null ? sucursal.hashCode() : 0);
        result = 31 * result + (variables != null ? variables.hashCode() : 0);
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (payAmount != null ? payAmount.hashCode() : 0);
        result = 31 * result + (payCode != null ? payCode.hashCode() : 0);
        result = 31 * result + (payMode != null ? payMode.hashCode() : 0);
        result = 31 * result + (requestXml != null ? requestXml.hashCode() : 0);
        result = 31 * result + (responseXml != null ? responseXml.hashCode() : 0);
        result = 31 * result + (codigoTramite != null ? codigoTramite.hashCode() : 0);
        result = 31 * result + (codigoError != null ? codigoError.hashCode() : 0);
        result = 31 * result + (mensajeError != null ? mensajeError.hashCode() : 0);
        result = 31 * result + (estadoTramite != null ? estadoTramite.hashCode() : 0);
        result = 31 * result + (fechaCreacion != null ? fechaCreacion.hashCode() : 0);
        return result;
    }
}
