package ch.swissbytes.fundempresa.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jose on 22/03/2016.
 */
@Getter
@Setter
public class EspecialesRequestDto {

    private String username;
    private String solicitante;
    private String ci;
    private String expci;
    private String telefono;
    private String direccion;
    private String ciudad;
    private String provincia;
    private String matricula;
    private String montoPagado;
    private String ejemplares;
    private String certificado;
    private String descripcion;
    private String sucursal;
    private long codSeguridad;

}
