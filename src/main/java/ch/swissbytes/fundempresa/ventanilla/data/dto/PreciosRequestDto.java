package ch.swissbytes.fundempresa.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by jose on 06/04/2016.
 */
@Getter
@Setter
public class PreciosRequestDto extends TramiteRequestDto implements Serializable {

    private int tipoTramite;
    private String tipoSociedad;
    private String matricula;
    private int docsSeleccionados;

}
