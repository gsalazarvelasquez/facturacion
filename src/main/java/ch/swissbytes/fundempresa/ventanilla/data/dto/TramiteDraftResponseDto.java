package ch.swissbytes.fundempresa.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by daniel on 12/04/2016.
 */

@Getter
@Setter
public class TramiteDraftResponseDto {

    private String tipoTramite;
    private Date fechaCreacion;
    private TramiteRequestDto tramiteRequestDto;
    private TramiteResponseDto tramiteResponseDto;

    public TramiteDraftResponseDto(){

    }

    public TramiteDraftResponseDto(String tipoTramite, Date fechaCreacion, TramiteResponseDto tramiteResponseDto){
        this.tipoTramite = tipoTramite;
        this.fechaCreacion = fechaCreacion;
        this.tramiteResponseDto = tramiteResponseDto;
    }
}
