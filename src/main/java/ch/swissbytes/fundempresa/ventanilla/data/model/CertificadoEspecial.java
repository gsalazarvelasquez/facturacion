package ch.swissbytes.fundempresa.ventanilla.data.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by daniel on 23/05/2016.
 */
@Entity
@javax.persistence.Table(name = "certificado_especial", schema = "fundempresa_local", catalog = "")
public class CertificadoEspecial {
    private Long id;

    @Id
    @javax.persistence.Column(name = "id", nullable = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private String matricula;

    @Basic
    @javax.persistence.Column(name = "matricula", nullable = true, length = 32)
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    private String codigoCertificado;

    @Basic
    @javax.persistence.Column(name = "codigo_certificado", nullable = true, length = 32)
    public String getCodigoCertificado() {
        return codigoCertificado;
    }

    public void setCodigoCertificado(String codigoCertificado) {
        this.codigoCertificado = codigoCertificado;
    }

    private Long cantidadEjemplares;

    @Basic
    @javax.persistence.Column(name = "cantidad_ejemplares", nullable = true)
    public Long getCantidadEjemplares() {
        return cantidadEjemplares;
    }

    public void setCantidadEjemplares(Long cantidadEjemplares) {
        this.cantidadEjemplares = cantidadEjemplares;
    }

    private String descripcion;

    @Basic
    @javax.persistence.Column(name = "descripcion", nullable = true, length = 2048)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    private String sucursal;

    @Basic
    @javax.persistence.Column(name = "sucursal", nullable = true, length = 1024)
    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    private String userName;

    @Basic
    @javax.persistence.Column(name = "user_name", nullable = true, length = 128)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    private String requestXml;

    @Basic
    @javax.persistence.Column(name = "request_xml", nullable = true, columnDefinition = "Text")
    public String getRequestXml() {
        return requestXml;
    }

    public void setRequestXml(String requestXml) {
        this.requestXml = requestXml;
    }

    private String responseXml;

    @Basic
    @javax.persistence.Column(name = "response_xml", nullable = true, columnDefinition = "Text")
    public String getResponseXml() {
        return responseXml;
    }

    public void setResponseXml(String responseXml) {
        this.responseXml = responseXml;
    }

    private String payCode;

    @Basic
    @javax.persistence.Column(name = "pay_code", nullable = true, length = 64)
    public String getPayCode() {
        return payCode;
    }

    public void setPayCode(String payCode) {
        this.payCode = payCode;
    }

    private String payMode;

    @Basic
    @javax.persistence.Column(name = "pay_mode", nullable = true, length = 64)
    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    private String payAmount;

    @Basic
    @javax.persistence.Column(name = "pay_amount", nullable = true, length = 64)
    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    private String codigoTramite;

    @Basic
    @javax.persistence.Column(name = "codigo_tramite", nullable = true, length = 64)
    public String getCodigoTramite() {
        return codigoTramite;
    }

    public void setCodigoTramite(String codigoTramite) {
        this.codigoTramite = codigoTramite;
    }

    private String codigoError;

    @Basic
    @javax.persistence.Column(name = "codigo_error", nullable = true, length = 64)
    public String getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(String codigoError) {
        this.codigoError = codigoError;
    }

    private String mensajeError;

    @Basic
    @javax.persistence.Column(name = "mensaje_error", nullable = true, length = 128)
    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    private String estadoTramite;

    @Basic
    @javax.persistence.Column(name = "estado_tramite", nullable = true, length = 64)
    public String getEstadoTramite() {
        return estadoTramite;
    }

    public void setEstadoTramite(String estadoTramite) {
        this.estadoTramite = estadoTramite;
    }

    private Timestamp fechaCreacion;

    @Basic
    @javax.persistence.Column(name = "fecha_creacion", nullable = true)
    public Timestamp getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Timestamp fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    private Timestamp fechaIngreso;

    @Basic
    @javax.persistence.Column(name = "fecha_ingreso", nullable = true)
    public Timestamp getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Timestamp fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CertificadoEspecial that = (CertificadoEspecial) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (matricula != null ? !matricula.equals(that.matricula) : that.matricula != null) return false;
        if (codigoCertificado != null ? !codigoCertificado.equals(that.codigoCertificado) : that.codigoCertificado != null)
            return false;
        if (cantidadEjemplares != null ? !cantidadEjemplares.equals(that.cantidadEjemplares) : that.cantidadEjemplares != null)
            return false;
        if (descripcion != null ? !descripcion.equals(that.descripcion) : that.descripcion != null) return false;
        if (sucursal != null ? !sucursal.equals(that.sucursal) : that.sucursal != null) return false;
        if (userName != null ? !userName.equals(that.userName) : that.userName != null) return false;
        if (requestXml != null ? !requestXml.equals(that.requestXml) : that.requestXml != null) return false;
        if (responseXml != null ? !responseXml.equals(that.responseXml) : that.responseXml != null) return false;
        if (payCode != null ? !payCode.equals(that.payCode) : that.payCode != null) return false;
        if (payMode != null ? !payMode.equals(that.payMode) : that.payMode != null) return false;
        if (payAmount != null ? !payAmount.equals(that.payAmount) : that.payAmount != null) return false;
        if (codigoTramite != null ? !codigoTramite.equals(that.codigoTramite) : that.codigoTramite != null)
            return false;
        if (codigoError != null ? !codigoError.equals(that.codigoError) : that.codigoError != null) return false;
        if (mensajeError != null ? !mensajeError.equals(that.mensajeError) : that.mensajeError != null) return false;
        if (estadoTramite != null ? !estadoTramite.equals(that.estadoTramite) : that.estadoTramite != null)
            return false;
        if (fechaCreacion != null ? !fechaCreacion.equals(that.fechaCreacion) : that.fechaCreacion != null)
            return false;
        if (fechaIngreso != null ? !fechaIngreso.equals(that.fechaIngreso) : that.fechaIngreso != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (matricula != null ? matricula.hashCode() : 0);
        result = 31 * result + (codigoCertificado != null ? codigoCertificado.hashCode() : 0);
        result = 31 * result + (cantidadEjemplares != null ? cantidadEjemplares.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        result = 31 * result + (sucursal != null ? sucursal.hashCode() : 0);
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (requestXml != null ? requestXml.hashCode() : 0);
        result = 31 * result + (responseXml != null ? responseXml.hashCode() : 0);
        result = 31 * result + (payCode != null ? payCode.hashCode() : 0);
        result = 31 * result + (payMode != null ? payMode.hashCode() : 0);
        result = 31 * result + (payAmount != null ? payAmount.hashCode() : 0);
        result = 31 * result + (codigoTramite != null ? codigoTramite.hashCode() : 0);
        result = 31 * result + (codigoError != null ? codigoError.hashCode() : 0);
        result = 31 * result + (mensajeError != null ? mensajeError.hashCode() : 0);
        result = 31 * result + (estadoTramite != null ? estadoTramite.hashCode() : 0);
        result = 31 * result + (fechaCreacion != null ? fechaCreacion.hashCode() : 0);
        result = 31 * result + (fechaIngreso != null ? fechaIngreso.hashCode() : 0);
        return result;
    }
}
