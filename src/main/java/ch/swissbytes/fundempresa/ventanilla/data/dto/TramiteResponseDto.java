package ch.swissbytes.fundempresa.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by daniel on 31/03/2016.
 */
@Getter
@Setter
public class TramiteResponseDto {

    public enum EstadoTramite {
        EN_PROCESO,
        PROCESADO,
        OBSERVADO,
        ENTREGADO,
        PENDIENTE_PAGO,
        PAGADO
    }

    public enum TipoTramite {
        HOMONIMIA,
        FOTOCOPIA,
        LISTADO_EMPRESAS,
        ESTADISTICA_BASICA,
        CRUCE_VARIABLES,
        CERTIFICADO_ESPECIAL,
        CERTIFICADO_LINEA
    }

    protected Long pkTramite;
    protected String codigoTramite;

    protected String pinQuiosco;
    protected String codigoError;
    protected String mensajeError;
    protected String estadoTramite;

    protected String requestXml;
    protected String responseXml;

    public static String getEstadoTramite(int ordinal){
        String result = EstadoTramite.values()[ordinal].name();
        return result;
    }
}
