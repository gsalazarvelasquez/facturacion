package ch.swissbytes.fundempresa.ventanilla.data.repository;

import ch.swissbytes.fundempresa.ventanilla.data.model.Solicitante;
import ch.swissbytes.shared.persistence.Repository;

import java.util.Optional;

/**
 * Created by daniel on 16/02/2016.
 */
public class SolicitanteRepositoryImpl extends Repository {

    public Solicitante getSolicitanteById(Long id){
        Optional<Solicitante> result = this.findById(Solicitante.class, id);
        return result.get();
    }

    @Override
    protected Class getPersistentClass() {
        return this.getClass();
    }
}
