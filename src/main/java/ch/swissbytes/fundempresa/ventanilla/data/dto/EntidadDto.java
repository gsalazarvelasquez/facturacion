package ch.swissbytes.fundempresa.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jose on 23/03/2016.
 */
@Getter
@Setter
public class EntidadDto {

    private String codError;
    private String descError;
    private String matricula;
    private String nombreEmpresa;
    private String estadoMatricula;

}
