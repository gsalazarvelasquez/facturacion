package ch.swissbytes.fundempresa.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jose on 22/03/2016.
 */
@Getter
@Setter
public class VentaBDRequestAdabasDto extends TramiteRequestDto implements Serializable {

    private String codServicio;
    private String solicitante;
    private String ci;
    private String expci;
    private String telefono;
    private String email;
    private String usoInformacion;
    private String descripcion;
    private String sucursal;

    private List<VentaBDVariablesAdabasDto> variables;
}