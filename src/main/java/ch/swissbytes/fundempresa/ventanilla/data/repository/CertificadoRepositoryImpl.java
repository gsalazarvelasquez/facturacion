package ch.swissbytes.fundempresa.ventanilla.data.repository;

import ch.swissbytes.fundempresa.ventanilla.data.model.TipoCertificado;
import ch.swissbytes.shared.persistence.Repository;

import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by daniel on 18/05/2016.
 */
public class CertificadoRepositoryImpl extends Repository {

    public List<TipoCertificado> getTipoCertificadoListByTipo(String tipo){

        Query query = em.createQuery("SELECT tipoCertificado FROM TipoCertificado tipoCertificado " +
                "WHERE tipoCertificado.tipo= :tipo ");
        query.setParameter("tipo", tipo);

        List<TipoCertificado> result = query.getResultList();

        return result;
    }

    @Override
    protected Class getPersistentClass() {
        return this.getClass();
    }
}
