package ch.swissbytes.fundempresa.ventanilla.data.dto;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasDRequestException;
import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.ventanilla.data.model.Homonimia;
import ch.swissbytes.fundempresa.ventanilla.data.model.TipoSociedad;
import lombok.*;

import java.io.Serializable;

/**
 * Created by jose on 18/03/2016.
 */
@Getter
@Setter
public class HomonimiaRequestDto extends TramiteRequestDto implements Serializable {

    public enum  TIPO_INGRESO {NUEVO, REINGRESO};

    private TipoSociedad tipoSocietario;
    private String opcion1;
    private String opcion2;
    private String opcion3;
    private String actividadEconomica;
    private String codigoTramite;


    public HomonimiaRequestDto() {
    }

    public HomonimiaRequestDto(Homonimia homonimia){
        super(homonimia.getPkHomonimia(), homonimia.getUserName(), homonimia.getPayCode(),
                homonimia.getPayMode(), homonimia.getPrecio());

        this.codigoTramite = homonimia.getCodigoTramite();

//        this.tipoSocietario = homonimia.getTipoSocietario();
        try {
            this.tipoSocietario = AdabasD.tipoSocietarioById(homonimia.getTipoSocietario());
        } catch (AdabasDRequestException e) {
            this.tipoSocietario = new TipoSociedad(3L, "Sociedad Anonima");
            e.printStackTrace();
        }
        this.opcion1 = homonimia.getOpcion1();
        this.opcion2 = homonimia.getOpcion2();
        this.opcion3 = homonimia.getOpcion3();
        this.actividadEconomica = homonimia.getActividadEconomica();

        this.payCode = homonimia.getPayCode();
        this.payMode = homonimia.getPayMode();
        this.payAmount = homonimia.getPrecio();
    }

    @Override
    public String toString() {
        return "HomonimiaRequestDto{" +
                "tipoSocietario=" + tipoSocietario +
                ", opcion1='" + opcion1 + '\'' +
                ", opcion2='" + opcion2 + '\'' +
                ", opcion3='" + opcion3 + '\'' +
                ", actividadEconomica='" + actividadEconomica + '\'' +
                ", codigoTramite='" + codigoTramite + '\'' +
                '}';
    }
}