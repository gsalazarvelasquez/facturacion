package ch.swissbytes.fundempresa.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by jose on 22/03/2016.
 */
@Getter
@Setter
public class EnlineaRequestDto {

    private String username;
    private String solicitante;
    private String ci;
    private String expci;
    private String telefono;
    private String montoPagado;
    private List<CertificadoDto> certificados;
    private long codSeguridad;

//    import java.security.SecureRandom;
//    import java.security.NoSuchAlgorithmException;
//    try {
//        SecureRandom number = SecureRandom.getInstance("SHA1PRNG");
//        // Generate 20 integers 0..20
//        for (int i = 0; i < 20; i++) {
//            System.out.println(number.nextInt(999999));
//        }
//    } catch (NoSuchAlgorithmException nsae) {
//        // Forward to handler
//    }
}
