package ch.swissbytes.fundempresa.ventanilla.data.repository;

import ch.swissbytes.fundempresa.ventanilla.data.model.TipoSociedad;
import ch.swissbytes.fundempresa.ventanilla.rest.tiposociedad.TipoSociedadFilter;
import ch.swissbytes.shared.filter.PaginatedData;
import ch.swissbytes.shared.persistence.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel on 11/02/2016.
 */
public class TipoSociedadRepositoryImpl extends Repository {

    @Override
    protected Class getPersistentClass() {
        return TipoSociedad.class;
    }
}
