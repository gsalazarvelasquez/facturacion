package ch.swissbytes.fundempresa.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jose on 26/04/2016.
 */
@Getter
@Setter
public class ListadoCalcDto {

    private String idTipoSocietario;
    private String cantidad;
    private String factor;

}
