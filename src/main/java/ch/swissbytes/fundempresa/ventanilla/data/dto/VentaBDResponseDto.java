package ch.swissbytes.fundempresa.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by daniel on 31/03/2016.
 */
@Getter
@Setter
public class VentaBDResponseDto extends TramiteResponseDto {

    private String matricula;

}
