package ch.swissbytes.fundempresa.ventanilla.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by daniel on 31/03/2016.
 */
@Getter
@Setter
public class CertificadosResponseDto extends TramiteResponseDto {

    private String matricula; //enlinea
    private String nombreEmpresa;
    private String ultimoId;
    private String estadoMatricula;

    private String textCertificado;
    private int validSolicitadas;
    private int validRestantes;
    private List<CertificadoDto> certificados;//en linea

}
