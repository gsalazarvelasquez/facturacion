package ch.swissbytes.fundempresa.ventanilla.data.repository;

import ch.swissbytes.fundempresa.ventanilla.data.model.VentabdParametros;
import ch.swissbytes.shared.persistence.Repository;

import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

/**
 * Created by daniel on 11/02/2016.
 */
public class PerfilesRepositoryImpl extends Repository {

    public List<Integer> local(String perfilAdabas) {
        Query query = em.createQuery("SELECT p.perfilLocal FROM PerfilesAdabas p WHERE p.perfilAdabas = :perfil");
        query.setParameter("perfil", perfilAdabas);
        List<Integer> result = query.getResultList();
        return result;
    }

    @Override
    protected Class getPersistentClass() {
        return this.getClass();
    }
}
