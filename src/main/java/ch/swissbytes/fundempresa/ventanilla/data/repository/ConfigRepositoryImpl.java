package ch.swissbytes.fundempresa.ventanilla.data.repository;

import ch.swissbytes.fundempresa.ventanilla.data.model.PerfilesAdabas;
import ch.swissbytes.fundempresa.ventanilla.data.model.PerfilesLocal;
import ch.swissbytes.shared.persistence.Repository;
import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class ConfigRepositoryImpl extends Repository {

    public List<PerfilesLocal> getPerfiles(){
        Query query = em.createQuery("SELECT perfil FROM PerfilesLocal perfil");
        List<PerfilesLocal> result = query.getResultList();
        return result;
    }

    public PerfilesAdabas getAsociacion(int id){
        Query query = em.createQuery("SELECT perfil FROM PerfilesAdabas perfil WHERE perfil.id=:id");
        query.setParameter("id", id);
        List result = query.getResultList();
        if(result != null && !result.isEmpty())
            return (PerfilesAdabas) result.get(0);
        return null;
    }

    public List<PerfilesAdabas> getAsociaciones(){
        Query query = em.createQuery("SELECT perfil FROM PerfilesAdabas perfil");
        List<PerfilesAdabas> result = query.getResultList();
        return result;
    }

    public PerfilesAdabas getPerfil(String adabas, int local){
        Query query = em.createQuery("SELECT perfil FROM PerfilesAdabas perfil WHERE perfil.perfilAdabas=:adabas AND perfil.perfilLocal=:local");
        query.setParameter("adabas", adabas);
        query.setParameter("local", local);
        List result = query.getResultList();
        if(result != null && !result.isEmpty())
            return (PerfilesAdabas) result.get(0);
        return null;
    }

    @Override
    protected Class getPersistentClass() {
        return this.getClass();
    }
}
