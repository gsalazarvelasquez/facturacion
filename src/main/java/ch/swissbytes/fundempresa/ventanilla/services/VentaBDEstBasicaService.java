package ch.swissbytes.fundempresa.ventanilla.services;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.ventanilla.data.dto.VentaBDEstBasicaDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.VentaBDResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.model.TipoEstadisticaBasica;
import java.util.List;

/**
 * Created by daniel on 24/02/2016.
 */
public interface VentaBDEstBasicaService {

    VentaBDResponseDto estbasica(VentaBDEstBasicaDto requestDto, String servicio) throws AdabasCRequestException;

    List<TipoEstadisticaBasica> getAllTipoEstadisticaBasica();

    Long saveDraft(VentaBDEstBasicaDto requestDto);

    VentaBDEstBasicaDto getDraft(Long pktramite);
}
