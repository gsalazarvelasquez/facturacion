package ch.swissbytes.fundempresa.ventanilla.services.internal;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasc.interfaces.VentaBDAdabasService;
import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import ch.swissbytes.fundempresa.ventanilla.data.repository.VentaBDRepositoryImpl;
import ch.swissbytes.fundempresa.ventanilla.services.VentaBDCruceVarService;
import lombok.extern.slf4j.Slf4j;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class VentaBDCruceVarServiceImpl implements VentaBDCruceVarService {

    @Inject
    private VentaBDAdabasService ventaBDAdabasService;

    @Inject
    private VentaBDRepositoryImpl ventaBDRepository;

    @Override
    public Long saveDraft(VentaBDCruceVarDto requestDto) {
        return null;
    }

    @Override
    public VentaBDCruceVarDto getDraft(Long pktramite) {
        return null;
    }

    @Override
    public VentaBDResponseDto crucevariables(VentaBDCruceVarDto requestDto, String servicio) throws AdabasCRequestException {
        VentaBDRequestAdabasDto ventaBdRequestDto = new VentaBDRequestAdabasDto();

        UserProfileDto userProfileDto = AdabasD.profile(requestDto.getUsername());
        String codigoServicio = AdabasD.codigoServicio(servicio);
        ventaBdRequestDto.setCodServicio(codigoServicio);
        ventaBdRequestDto.setSolicitante(userProfileDto.getFullName());
        ventaBdRequestDto.setCi(userProfileDto.getCi());
        ventaBdRequestDto.setExpci(userProfileDto.getExpci());
        ventaBdRequestDto.setTelefono(userProfileDto.getPhone());
        ventaBdRequestDto.setEmail(userProfileDto.getEmail());
        ventaBdRequestDto.setUsoInformacion(requestDto.getUsoInformacion());
        ventaBdRequestDto.setPayAmount(requestDto.getMontoPagado());
        ventaBdRequestDto.setDescripcion(requestDto.getUsoInformacion());
        ventaBdRequestDto.setSucursal(requestDto.getSucursal());

        List<VentaBDVariablesAdabasDto> variables = getVariablesCruceVar(requestDto);
        ventaBdRequestDto.setVariables(variables);

        VentaBDResponseDto ventaBdResponseDto = ventaBDAdabasService.solicitar(ventaBdRequestDto);
        return ventaBdResponseDto;
    }

    private List<VentaBDVariablesAdabasDto> getVariablesCruceVar(VentaBDCruceVarDto requestDto) {
        List<VentaBDVariablesAdabasDto> response = new ArrayList<VentaBDVariablesAdabasDto>();

        //ADD ESTADOS
        VentaBDVariablesAdabasDto states = new VentaBDVariablesAdabasDto();
        states.setVariable("TODOS_ESTADO_MATRICULA");
        states.setValor(String.valueOf(requestDto.isTodosEstados()));
        response.add(states);
        if(!requestDto.isTodosEstados()) {
            List<String> listado = requestDto.getEstados();
            if(listado != null && !listado.isEmpty()){
                VentaBDVariablesAdabasDto items = str(listado, "ESTADO_MATRICULA");
                response.add(items);
            }
        }
        String inscritas = requestDto.getAnhoInscripcion();
        inscritas = inscritas == null ? "" : inscritas;
        if(!inscritas.isEmpty()){
            VentaBDVariablesAdabasDto item = new VentaBDVariablesAdabasDto();
            item.setVariable("ANHOS_INSCRIPCION");
            item.setValor(inscritas);
            response.add(item);
        }
        String renovadas = requestDto.getAnhoRenovacion();
        renovadas = renovadas == null ? "" : renovadas;
        if(!renovadas.isEmpty()){
            VentaBDVariablesAdabasDto item = new VentaBDVariablesAdabasDto();
            item.setVariable("ANHOS_RENOVACION");
            item.setValor(renovadas);
            response.add(item);
        }
        String canceladas = requestDto.getAnhoCancelacion();
        canceladas = canceladas == null ? "" : canceladas;
        if(!canceladas.isEmpty()){
            VentaBDVariablesAdabasDto item = new VentaBDVariablesAdabasDto();
            item.setVariable("ANHOS_CANCELACION");
            item.setValor(canceladas);
            response.add(item);
        }

        //ADD DEPARTAMENTOS
        VentaBDVariablesAdabasDto dptos = new VentaBDVariablesAdabasDto();
        dptos.setVariable("TODOS_DEPARTAMENTOS");
        dptos.setValor(String.valueOf(requestDto.isTodosDptos()));
        response.add(dptos);
        if(!requestDto.isTodosDptos()) {
            List<String> listado = requestDto.getDepartamentos();
            if(listado != null && !listado.isEmpty()){
                VentaBDVariablesAdabasDto items = str(listado, "DEPARTAMENTOS");
                response.add(items);
            }
            listado = requestDto.getMunicipios();
            if(listado != null && !listado.isEmpty()){
                VentaBDVariablesAdabasDto items = str(listado, "MUNICIPIOS");
                response.add(items);
            }
        }

        //ADD TIPOS_SOCIEDAD
        VentaBDVariablesAdabasDto tSoc = new VentaBDVariablesAdabasDto();
        tSoc.setVariable("TODOS_TIPOS_SOCIEDAD");
        tSoc.setValor(String.valueOf(requestDto.isTodosTipoSoc()));
        response.add(tSoc);
        if(!requestDto.isTodosTipoSoc()) {
            List<String> listado = requestDto.getTiposSociedad();
            if(listado != null && !listado.isEmpty()){
                VentaBDVariablesAdabasDto items = str(listado, "TIPOS_SOCIEDAD");
                response.add(items);
            }
        }

        //ADD SECCIONES
        VentaBDVariablesAdabasDto secciones = new VentaBDVariablesAdabasDto();
        secciones.setVariable("TODOS_SECCIONES");
        secciones.setValor(String.valueOf(requestDto.isTodasSeccion()));
        response.add(secciones);
        if(!requestDto.isTodasSeccion()) {
            List<String> listado = requestDto.getSecciones();
            if(listado != null && !listado.isEmpty()){
                VentaBDVariablesAdabasDto items = str(listado, "SECCIONES");
                response.add(items);
            }
            listado = requestDto.getClasesCIIU();
            if(listado != null && !listado.isEmpty()){
                VentaBDVariablesAdabasDto items = str(listado, "CLASES_CIIU");
                response.add(items);
            }
        }

        //ADD VARIABLES FINANCIERAS
        List<String> listado = requestDto.getVFinancActivos();
        if(listado != null && !listado.isEmpty()){
            VentaBDVariablesAdabasDto items = str(listado, "VARIABLES_FINANCIERAS_ACTIVOS");
            response.add(items);
        }
        listado = requestDto.getVFinancPatrimonio();
        if(listado != null && !listado.isEmpty()){
            VentaBDVariablesAdabasDto items = str(listado, "VARIABLES_FINANCIERAS_PATRIMONIO");
            response.add(items);
        }
        listado = requestDto.getVFinancVentas();
        if(listado != null && !listado.isEmpty()){
            VentaBDVariablesAdabasDto items = str(listado, "VARIABLES_FINANCIERAS_VENTAS");
            response.add(items);
        }
        listado = requestDto.getVFinancUtilidades();
        if(listado != null && !listado.isEmpty()){
            VentaBDVariablesAdabasDto items = str(listado, "VARIABLES_FINANCIERAS_UTILIDADES");
            response.add(items);
        }
        listado = requestDto.getVFinancCapital();
        if(listado != null && !listado.isEmpty()){
            VentaBDVariablesAdabasDto items = str(listado, "VARIABLES_FINANCIERAS_CAPITAL");
            response.add(items);
        }

        return response;
    }

    private VentaBDVariablesAdabasDto str(List<String> listado, String variable) {
        VentaBDVariablesAdabasDto response = new VentaBDVariablesAdabasDto();
        String valores = "";
        int max = listado.size();
        for (int i = 0; i < max; i++) {
            String item = listado.get(i);
            valores += item;
            valores += (i + 1 < max) ? "," : "";
        }
        response.setVariable(variable);
        response.setValor(valores);
        return response;
    }
}
