package ch.swissbytes.fundempresa.ventanilla.services.internal;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasc.exception.AdabasDRequestException;
import ch.swissbytes.fundempresa.adabasc.interfaces.FotocopiasAdabasService;
import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.adabasd.dto.SucursalDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import ch.swissbytes.fundempresa.ventanilla.data.model.Fotocopia;
import ch.swissbytes.fundempresa.ventanilla.data.repository.FotocopiasRepositoryImpl;
import ch.swissbytes.fundempresa.ventanilla.services.FotocopiasService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.*;

//@Slf4j
public class FotocopiasServiceImpl implements FotocopiasService {

    @Inject
    private FotocopiasRepositoryImpl fotocopiasRepository;

    @Inject
    private FotocopiasAdabasService fotocopiasAdabasService;

    private static String ADABASC_FIELD_FOTOCOPIAS = "VVSERVSLF";

    @Override
    public TramiteResponseDto ingresarFotocopia(FotocopiasRequestDto fotocopiasRequestDto) throws AdabasCRequestException {
        String matricula = fotocopiasRequestDto.getMatricula();
        String username = fotocopiasRequestDto.getUserName();
        ResponseDto resp = new ResponseDto();
        resp.setArancel(fotocopiasRequestDto.getPayAmount());
        resp.setDescTramite(ADABASC_FIELD_FOTOCOPIAS);
        AdabasD.transaction(resp, username, matricula, "", "");

        TramiteResponseDto fotocopiasResponseDto = fotocopiasAdabasService.ingresarFotocopia(fotocopiasRequestDto);
        fotocopiasResponseDto.setEstadoTramite(TramiteResponseDto.EstadoTramite.PROCESADO.name());

        if (fotocopiasRequestDto.getPkTramite() == null){
            Fotocopia fotocopia = new Fotocopia();
            setFotocopiaSubmit(fotocopia, fotocopiasRequestDto, fotocopiasResponseDto);
            fotocopia.setEstadoTramite(fotocopiasResponseDto.getEstadoTramite());
            fotocopia.setFechaCreacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));

            fotocopiasRepository.save(fotocopia);
        } else{
            Fotocopia previous = fotocopiasRepository.getById(Fotocopia.class, fotocopiasRequestDto.getPkTramite()).get();
            previous.setEstadoTramite(fotocopiasResponseDto.getEstadoTramite());
            setFotocopiaSubmit(previous, fotocopiasRequestDto, fotocopiasResponseDto);

            fotocopiasRepository.update(previous);
        }

        return fotocopiasResponseDto;
    }

    @Override
    public EntidadListDto consultaEntidades(String razonsocial, String ultimoId) throws AdabasCRequestException {
        EntidadListDto result = fotocopiasAdabasService.consultaEntidades(razonsocial, ultimoId);
        return result;
    }

    @Override
    public EntidadDto consultaEntidad(String matricula) throws AdabasCRequestException {
        EntidadDto fotocopiasResponseDto = fotocopiasAdabasService.consultaEntidad(matricula);
        return fotocopiasResponseDto;
    }

    @Override
    public Fotocopia get(String pkTramite) {
        Optional<Fotocopia> optional = fotocopiasRepository.getBy(Fotocopia.class, "id", Long.parseLong(pkTramite));
        return optional.get();
    }

    @Override
    public void update(Fotocopia tramite) {
        fotocopiasRepository.update(tramite);
    }

    @Override
    public Long saveDraft(FotocopiasRequestDto fotocopiasRequestDto){
        Long result = null;
        try {
            if (fotocopiasRequestDto.getPkTramite() == null){
                Fotocopia fotocopia = getNewFotocopiaBasic(fotocopiasRequestDto);
                fotocopiasRepository.saveAndFlush(fotocopia);
                result = fotocopia.getId();
            }else{
                Fotocopia fotocopia = fotocopiasRepository.getById(Fotocopia.class, fotocopiasRequestDto.getPkTramite()).get();
                updateHomonimiaDraft(fotocopiasRequestDto, fotocopia);
                fotocopiasRepository.update(fotocopia);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public FotocopiasRequestDto getDraft(Long id) {
        Fotocopia fotocopia = fotocopiasRepository.getBy(Fotocopia.class, "id", id).get();
        return new FotocopiasRequestDto(fotocopia);
    }

    private void updateHomonimiaDraft(FotocopiasRequestDto request, Fotocopia fotocopia) {
        fotocopia.setId(request.getPkTramite());
        fotocopia.setMatricula(request.getMatricula());
        fotocopia.setDocumentos(formatDocumentosToJson(request.getDocumentos()));
        fotocopia.setDescripcion(request.getDescripcion());
        fotocopia.setSucursal(request.getSucursal().getIdSucursal());
        fotocopia.setUserName(request.getUserName());
        fotocopia.setPayCode(request.getPayCode());
        fotocopia.setPayMode(request.getPayMode());
        fotocopia.setPayAmount(request.getPayAmount());
        fotocopia.setFechaCreacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        fotocopia.setEstadoTramite(getEstadoTramite(request));
    }

    private Fotocopia getNewFotocopiaBasic(FotocopiasRequestDto request) {
        Fotocopia fotocopia = new Fotocopia();
        fotocopia.setId(request.getPkTramite());
        fotocopia.setMatricula(request.getMatricula());
        fotocopia.setDocumentos(formatDocumentosToJson(request.getDocumentos()));
        fotocopia.setDescripcion(request.getDescripcion());
        fotocopia.setSucursal(request.getSucursal().getIdSucursal());
        fotocopia.setUserName(request.getUserName());
        fotocopia.setPayCode(request.getPayCode());
        fotocopia.setPayMode(request.getPayMode());
        fotocopia.setPayAmount(request.getPayAmount());
        fotocopia.setFechaCreacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        fotocopia.setEstadoTramite(getEstadoTramite(request));
        return fotocopia;
    }

    private String getEstadoTramite(FotocopiasRequestDto request) {
        String estadoTramite;
        if (!request.getPayCode().isEmpty()){
            if (request.getPkTramite() == null){
                estadoTramite = TramiteResponseDto.EstadoTramite.PAGADO.name();
            } else {
                estadoTramite = TramiteResponseDto.EstadoTramite.PROCESADO.name();
            }
        } else {
            estadoTramite = TramiteResponseDto.EstadoTramite.PENDIENTE_PAGO.name();
        }
        return estadoTramite;
    }

    @Override
    public List<TramiteDraftResponseDto> getAllTramiteList(String userName){
        List<Fotocopia> fotocopiaList = fotocopiasRepository.getAllOrderByCreationDate();
        List<TramiteDraftResponseDto> responseResult = new LinkedList<TramiteDraftResponseDto>();
        for (Fotocopia fotocopia: fotocopiaList){
            TramiteDraftResponseDto tramiteDraftResponseDto = new TramiteDraftResponseDto();
            tramiteDraftResponseDto.setTipoTramite(TramiteResponseDto.TipoTramite.FOTOCOPIA.name());
            Date fechaCreacion = fotocopia.getFechaCreacion() != null
                    ? new Date(fotocopia.getFechaCreacion().getTime())
                    : null;
            tramiteDraftResponseDto.setFechaCreacion(fechaCreacion);
//            tramiteDraftResponseDto.setTramiteRequestDto(new FotocopiasRequestDto(fotocopia));
            tramiteDraftResponseDto.setTramiteResponseDto(new FotocopiasResponseDto(fotocopia));
            responseResult.add(tramiteDraftResponseDto);
        }
        return responseResult;
    }

    @Override
    public FotocopiaListDto consultaFotocopias(String matricula, String ultimoId) throws AdabasCRequestException {
        FotocopiaListDto fotocopiaListDto = fotocopiasAdabasService.consultaFotocopias(matricula, ultimoId);
        return fotocopiaListDto;
    }

    @Override
    public List<SucursalDto> sucursales() throws AdabasDRequestException {
        List<SucursalDto> sucursalDtoList = AdabasD.sucursales();
        return sucursalDtoList;
    }

    private void setFotocopiaSubmit(Fotocopia fotocopia, FotocopiasRequestDto request, TramiteResponseDto response){

//        Fotocopia fotocopia = new Fotocopia();
        fotocopia.setId(request.getPkTramite());
        fotocopia.setMatricula(request.getMatricula());
//        fotocopia.setDocumentos(request.getDocumentos());
        fotocopia.setDocumentos(formatDocumentosToJson(request.getDocumentos()));
        fotocopia.setDescripcion(request.getDescripcion());
        fotocopia.setSucursal(request.getSucursal().getIdSucursal());

        fotocopia.setUserName(request.getUserName());
        fotocopia.setPayCode(request.getPayCode());
        fotocopia.setPayMode(request.getPayMode());
        fotocopia.setPayAmount(request.getPayAmount());

        fotocopia.setCodigoTramite(response.getCodigoTramite());
        fotocopia.setCodigoError(response.getCodigoError());
        fotocopia.setMensajeError(response.getMensajeError());
//        fotocopia.setEstadoTramite(response.getEstadoTramite());
//        fotocopia.setFechaCreacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        fotocopia.setRequestXml(response.getRequestXml());
        fotocopia.setResponseXml(response.getResponseXml());

//        return fotocopia;
    }

    private String formatDocumentosToJson(List<DocumentoFotocopiasDto> documentos) {
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(documentos);

        return json;
    }

}
