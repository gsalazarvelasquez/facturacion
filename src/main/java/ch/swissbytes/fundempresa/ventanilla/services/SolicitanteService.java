package ch.swissbytes.fundempresa.ventanilla.services;

import ch.swissbytes.fundempresa.ventanilla.data.model.Solicitante;

/**
 * Created by daniel on 16/02/2016.
 */
public interface SolicitanteService {

    Solicitante getSolicitanteById(Long id);
}
