package ch.swissbytes.fundempresa.ventanilla.services.internal;

import ch.swissbytes.fundempresa.ventanilla.data.model.TipoTramite;
import ch.swissbytes.fundempresa.ventanilla.data.repository.TipoTramiteRepositoryImpl;
import ch.swissbytes.fundempresa.ventanilla.services.TipoTramiteService;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by daniel on 24/02/2016.
 */
public class TipoTramiteServiceImpl implements TipoTramiteService{

    @Inject
    private TipoTramiteRepositoryImpl tipoTramiteRepository;

    @Override
    public List<TipoTramite> getAll() {
        List<TipoTramite> result = this.tipoTramiteRepository.findAll(TipoTramite.class);
        return result;
    }
}
