package ch.swissbytes.fundempresa.ventanilla.services;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.ventanilla.data.dto.VentaBDListadoDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.VentaBDResponseDto;
import java.io.OutputStream;

/**
 * Created by daniel on 24/02/2016.
 */
public interface VentaBDListadoService {

    VentaBDResponseDto ingresarSolicitud(VentaBDListadoDto requestDto, String servicio) throws AdabasCRequestException;

    void descargar(OutputStream os, VentaBDListadoDto requestDto);

    Long saveDraft(VentaBDListadoDto requestDto);

    VentaBDListadoDto getDraft(Long pktramite);
}
