package ch.swissbytes.fundempresa.ventanilla.services;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasDRequestException;
import ch.swissbytes.fundempresa.billing.data.model.Dosificacion;
import ch.swissbytes.fundempresa.billing.data.model.FundempresaInfo;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import ch.swissbytes.fundempresa.ventanilla.data.model.PerfilesAdabas;
import ch.swissbytes.fundempresa.ventanilla.data.model.PerfilesLocal;
import ch.swissbytes.fundempresa.ventanilla.exception.PerfilExisteException;

import java.util.List;

/**
 * Created by daniel on 24/02/2016.
 */
public interface ConfigService {

    FundempresaInfo fundempresaInfo();

    Boolean updateFundempresaInfo(FundempresaInfo info) throws Exception;

    List<Dosificacion> dosificaciones();

    Boolean addDosificacion(Dosificacion dosage) throws Exception;

    Boolean enableDosificacion(long id) throws Exception;

    Boolean disableDosificacion(long id) throws Exception;

    List<Departamento> departamentos() throws AdabasDRequestException;

    List<Municipio> municipios() throws AdabasDRequestException;

    List<Seccion> secciones() throws AdabasDRequestException;

    List<ClasesCIIU> clasesCIIU() throws AdabasDRequestException;

    List<PerfilesLocal> perfiles();

    List<PerfilesAdabas> asociaciones();

    List<PerfilAdabasDto> perfilesAdabas() throws AdabasDRequestException;

    Boolean update(PerfilesAdabas perfil);

    Boolean deletePerfilAdabas(int id) throws Exception;

    Boolean save(PerfilesAdabas perfil) throws PerfilExisteException;

}
