package ch.swissbytes.fundempresa.ventanilla.services.internal;



import ch.swissbytes.fundempresa.adabasc.exception.AdabasDRequestException;
import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.ventanilla.data.repository.TipoSociedadRepositoryImpl;
import ch.swissbytes.fundempresa.ventanilla.services.TipoSociedadService;
import ch.swissbytes.fundempresa.ventanilla.data.model.TipoSociedad;

import javax.inject.Inject;

import java.util.List;

/**
 * Created by jorgeburgos on 1/30/16.
 */
//@Stateless
public class TipoSociedadServiceImpl implements TipoSociedadService {

    @Inject
    TipoSociedadRepositoryImpl tipoSociedadRepositoryImpl;

    @Override
    public List<TipoSociedad> findAll() throws AdabasDRequestException {
        List<TipoSociedad> fotocopiasResponseDto = AdabasD.tiposSocietarios();
        return fotocopiasResponseDto;
    }

//    @Override
//    public List<TipoSociedad> findAll() {
//        List<TipoSociedad> result = this.tipoSociedadRepositoryImpl.findAll(TipoSociedad.class);
//        return result;
//    }
}
