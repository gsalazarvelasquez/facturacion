package ch.swissbytes.fundempresa.ventanilla.services;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.ventanilla.data.dto.VentaBDCruceVarDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.VentaBDResponseDto;

/**
 * Created by daniel on 24/02/2016.
 */
public interface VentaBDCruceVarService {

    VentaBDResponseDto crucevariables(VentaBDCruceVarDto requestDto, String servicio) throws AdabasCRequestException;

    Long saveDraft(VentaBDCruceVarDto requestDto);

    VentaBDCruceVarDto getDraft(Long pktramite);
}
