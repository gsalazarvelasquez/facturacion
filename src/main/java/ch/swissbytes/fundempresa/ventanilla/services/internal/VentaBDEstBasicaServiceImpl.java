package ch.swissbytes.fundempresa.ventanilla.services.internal;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasc.interfaces.VentaBDAdabasService;
import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import ch.swissbytes.fundempresa.ventanilla.data.model.EstadisticaBasica;
import ch.swissbytes.fundempresa.ventanilla.data.model.TipoEstadisticaBasica;
import ch.swissbytes.fundempresa.ventanilla.data.repository.VentaBDRepositoryImpl;
import ch.swissbytes.fundempresa.ventanilla.services.VentaBDEstBasicaService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Slf4j
public class VentaBDEstBasicaServiceImpl implements VentaBDEstBasicaService {

    @Inject
    private VentaBDAdabasService ventaBDAdabasService;

    @Inject
    private VentaBDRepositoryImpl ventaBDRepository;

    @Override
    public Long saveDraft(VentaBDEstBasicaDto requestDto) {
        Long result = null;
        try {
            if (requestDto.getPkTramite() == null){
                EstadisticaBasica entity = getNew(requestDto);
                ventaBDRepository.saveAndFlush(entity);
                result = entity.getId();
            }else{
                EstadisticaBasica entity = ventaBDRepository.getById(EstadisticaBasica.class, requestDto.getPkTramite()).get();
                updateDraft(requestDto, entity);
                ventaBDRepository.update(entity);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public VentaBDEstBasicaDto getDraft(Long pktramite) {
//        EstadisticaBasica entity = ventaBDRepository.getBy(EstadisticaBasica.class, "id", pktramite).get();
//        return new VentaBDEstBasicaDto(entity);
        return null;
    }

    private EstadisticaBasica getNew(VentaBDEstBasicaDto request) {
        EstadisticaBasica entity = new EstadisticaBasica();
        entity.setId(request.getPkTramite());
        entity.setInformacionUso(request.getUsoInformacion());
        entity.setDescripcionSolicitud(request.getDescripcion());
        entity.setSucursal(request.getSucursal().getIdSucursal());
        entity.setUserName(request.getUserName());
        entity.setPayCode(request.getPayCode());
        entity.setPayMode(request.getPayMode());
        entity.setPayAmount(request.getPayAmount());
        entity.setFechaCreacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        entity.setEstadoTramite(getEstadoTramite(request));
        return entity;
    }

    private void updateDraft(VentaBDEstBasicaDto request, EstadisticaBasica entity) {
        entity.setId(request.getPkTramite());
        entity.setInformacionUso(request.getUsoInformacion());
        entity.setDescripcionSolicitud(request.getDescripcion());
        entity.setSucursal(request.getSucursal().getIdSucursal());
        entity.setUserName(request.getUserName());
        entity.setPayCode(request.getPayCode());
        entity.setPayMode(request.getPayMode());
        entity.setPayAmount(request.getPayAmount());
        entity.setFechaCreacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        entity.setEstadoTramite(getEstadoTramite(request));
    }

    private String getEstadoTramite(VentaBDEstBasicaDto request) {
        String estadoTramite;
        if (!request.getPayCode().isEmpty()){
            if (request.getPkTramite() == null){
                estadoTramite = TramiteResponseDto.EstadoTramite.PAGADO.name();
            } else {
                estadoTramite = TramiteResponseDto.EstadoTramite.PROCESADO.name();
            }
        } else {
            estadoTramite = TramiteResponseDto.EstadoTramite.PENDIENTE_PAGO.name();
        }
        return estadoTramite;
    }

    @Override
    public VentaBDResponseDto estbasica(VentaBDEstBasicaDto requestDto, String servicio) throws AdabasCRequestException {
        VentaBDRequestAdabasDto ventaBdRequestDto = new VentaBDRequestAdabasDto();

        UserProfileDto userProfileDto = AdabasD.profile(requestDto.getUserName());
        String codigoServicio = AdabasD.codigoServicio(servicio);
        ventaBdRequestDto.setCodServicio(codigoServicio);
        ventaBdRequestDto.setSolicitante(userProfileDto.getFullName());
        ventaBdRequestDto.setCi(userProfileDto.getCi());
        ventaBdRequestDto.setExpci(userProfileDto.getExpci());
        ventaBdRequestDto.setTelefono(userProfileDto.getPhone());
        ventaBdRequestDto.setEmail(userProfileDto.getEmail());
        ventaBdRequestDto.setUsoInformacion(requestDto.getUsoInformacion());
        ventaBdRequestDto.setPayAmount(requestDto.getPayAmount());
        ventaBdRequestDto.setDescripcion(requestDto.getUsoInformacion());
        ventaBdRequestDto.setSucursal(requestDto.getSucursal().getIdSucursal());

        ventaBdRequestDto.setUserName(requestDto.getUserName());
        ventaBdRequestDto.setPayCode(requestDto.getPayCode());
        ventaBdRequestDto.setPayMode(requestDto.getPayMode());

        List<VentaBDVariablesAdabasDto> variables = getVariablesEstBasica(requestDto);
        ventaBdRequestDto.setVariables(variables);

        VentaBDResponseDto ventaBdResponseDto = ventaBDAdabasService.solicitar(ventaBdRequestDto);

        //FROM HERE SAVE CURRENT TRANSACTION IN DB
        ventaBdResponseDto.setEstadoTramite(TramiteResponseDto.EstadoTramite.PROCESADO.name());

        if (requestDto.getPkTramite() == null){
            EstadisticaBasica estadisticaBasica = new EstadisticaBasica();
            setEstadisticaBasicaSubmit(estadisticaBasica, ventaBdRequestDto, ventaBdResponseDto);
            estadisticaBasica.setEstadoTramite(ventaBdResponseDto.getEstadoTramite());
            estadisticaBasica.setFechaCreacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));
            
            ventaBDRepository.save(estadisticaBasica);
            log.info("Nueva transaccion registrada en la base de datos.");
        } else {
            EstadisticaBasica previous = ventaBDRepository.getById(EstadisticaBasica.class, requestDto.getPkTramite()).get();
            previous.setEstadoTramite(ventaBdResponseDto.getEstadoTramite());
            setEstadisticaBasicaSubmit(previous, ventaBdRequestDto, ventaBdResponseDto);
            ventaBDRepository.update(previous);
            log.info("Transaccion actualizada en la base de datos.");
        }

        return ventaBdResponseDto;
    }

    private void setEstadisticaBasicaSubmit(EstadisticaBasica estadisticaBasica, VentaBDRequestAdabasDto request, VentaBDResponseDto response) {

        estadisticaBasica.setId(request.getPkTramite());
        estadisticaBasica.setInformacionUso(request.getUsoInformacion());
        estadisticaBasica.setDescripcionSolicitud(request.getDescripcion());
        estadisticaBasica.setSucursal(request.getSucursal());
        estadisticaBasica.setVariables(formatDocumentosToJson(request.getVariables()));

        estadisticaBasica.setUserName(request.getUserName());
        estadisticaBasica.setPayCode(request.getPayCode());
        estadisticaBasica.setPayMode(request.getPayMode());
        estadisticaBasica.setPayAmount(request.getPayAmount());

        estadisticaBasica.setCodigoTramite(response.getCodigoTramite());
        estadisticaBasica.setCodigoError(response.getCodigoError());
        estadisticaBasica.setMensajeError(response.getMensajeError());
//        estadisticaBasica.setEstadoTramite(response.getEstadoTramite());
//        estadisticaBasica.setFechaCreacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        estadisticaBasica.setRequestXml(response.getRequestXml());
        estadisticaBasica.setResponseXml(response.getResponseXml());
    }

    private String formatDocumentosToJson(List<VentaBDVariablesAdabasDto> documentos) {
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(documentos);
        return json;
    }

    @Override
    public List<TipoEstadisticaBasica> getAllTipoEstadisticaBasica(){
        List<TipoEstadisticaBasica> result = ventaBDRepository.findAll(TipoEstadisticaBasica.class);
        return result;
    }

    private List<VentaBDVariablesAdabasDto> getVariablesEstBasica(VentaBDEstBasicaDto requestDto) {
        List<VentaBDVariablesAdabasDto> response = new ArrayList<VentaBDVariablesAdabasDto>();

        //ADD ESTADISTICAS_MENSUALES
        String mes = String.valueOf(requestDto.isMensuales());
        VentaBDVariablesAdabasDto mensual = new VentaBDVariablesAdabasDto();
        mensual.setVariable("ESTADISTICAS_MENSUALES");
        mensual.setValor(mes);
        response.add(mensual);

        //ADD ESTADISTICAS_DEPARTAMENTALES
        String dpto = String.valueOf(requestDto.isDepartamentales());
        VentaBDVariablesAdabasDto dptos = new VentaBDVariablesAdabasDto();
        dptos.setVariable("ESTADISTICAS_DEPARTAMENTALES");
        dptos.setValor(dpto);
        response.add(dptos);

        //ADD ESTADISTICAS_ACTIVIDAD_ECONOMICA
        String act = String.valueOf(requestDto.isActEconomica());
        VentaBDVariablesAdabasDto actEco = new VentaBDVariablesAdabasDto();
        actEco.setVariable("ESTADISTICAS_ACTIVIDAD_ECONOMICA");
        actEco.setValor(act);
        response.add(actEco);

        //ADD ANUARIO_ESTADISTICO
        String anuario = String.valueOf(requestDto.isAnuarioEstadistico());
        VentaBDVariablesAdabasDto anEst = new VentaBDVariablesAdabasDto();
        anEst.setVariable("ANUARIO_ESTADISTICO");
        anEst.setValor(anuario);
        response.add(anEst);

        //ADD ESTADISTICAS_DATOS_FINANCIEROS
        String dato = String.valueOf(requestDto.isDatosFinancieros());
        VentaBDVariablesAdabasDto datosFin = new VentaBDVariablesAdabasDto();
        datosFin.setVariable("ESTADISTICAS_DATOS_FINANCIEROS");
        datosFin.setValor(dato);
        response.add(datosFin);

        return response;
    }
}
