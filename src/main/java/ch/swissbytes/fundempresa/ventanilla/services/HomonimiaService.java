package ch.swissbytes.fundempresa.ventanilla.services;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasc.exception.AdabasDRequestException;
import ch.swissbytes.fundempresa.ventanilla.data.dto.HomonimiaRequestDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.HomonimiaResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteDraftResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.model.Homonimia;
import ch.swissbytes.fundempresa.ventanilla.data.model.TipoSociedad;

import java.math.BigDecimal;
import java.util.List;

public interface HomonimiaService {

    HomonimiaResponseDto ingresarHomonimia(HomonimiaRequestDto homonimiaRequestDto) throws AdabasCRequestException;

    HomonimiaResponseDto reingresarHomonimia(HomonimiaRequestDto homonimiaRequestDto) throws AdabasCRequestException;

    HomonimiaResponseDto getEstadoHomonimia(String codigoTramite) throws AdabasCRequestException;

    List<TramiteResponseDto> getListaPendientes(String userName) throws AdabasCRequestException;

    List<TramiteDraftResponseDto> getAllTramiteList(String userName) throws AdabasCRequestException;

    BigDecimal getPayAmountHomonimia(Integer tipoSocietarioId) throws AdabasCRequestException;

    Long saveDraft(HomonimiaRequestDto homonimiaRequestDto);

    HomonimiaRequestDto getDraft(Long pkHomonimia);

    Homonimia get(String pkTramite);

    void update(Homonimia tramite);
}
