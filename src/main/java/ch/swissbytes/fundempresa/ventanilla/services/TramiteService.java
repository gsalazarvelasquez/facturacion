package ch.swissbytes.fundempresa.ventanilla.services;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasc.exception.AdabasDRequestException;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteDraftResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteRequestDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteResponseDto;

import java.util.List;

/**
 * Created by daniel on 28/03/2016.
 */
public interface TramiteService {

//    TramiteResponseDto submitTramite(TramiteRequestDto dto);
//    Object saveDraft();

    List<TramiteDraftResponseDto> getAllTramiteList(String userName) throws AdabasCRequestException;

    UserProfileDto getUserProfile(String userName);
}
