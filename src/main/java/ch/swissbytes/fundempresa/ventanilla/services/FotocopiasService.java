package ch.swissbytes.fundempresa.ventanilla.services;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasc.exception.AdabasDRequestException;
import ch.swissbytes.fundempresa.adabasd.dto.SucursalDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import ch.swissbytes.fundempresa.ventanilla.data.model.Fotocopia;

import java.util.List;

/**
 * Created by daniel on 24/02/2016.
 */
public interface FotocopiasService {

    TramiteResponseDto ingresarFotocopia(FotocopiasRequestDto fotocopiasRequestDto) throws AdabasCRequestException;

//    FotocopiasResponseDto consultaEntidades(String razonsocial, String ultimoId) throws AdabasCRequestException;
    EntidadListDto consultaEntidades(String razonsocial, String ultimoId) throws AdabasCRequestException;

//    FotocopiasResponseDto consultaEntidad(String matricula) throws AdabasCRequestException;
    EntidadDto consultaEntidad(String matricula) throws AdabasCRequestException;

    FotocopiaListDto consultaFotocopias(String matricula, String ultimoId) throws AdabasCRequestException;

    Long saveDraft(FotocopiasRequestDto fotocopiasRequestDto);

    Fotocopia get(String pkTramite);

    void update(Fotocopia tramite);

    List<SucursalDto> sucursales() throws AdabasDRequestException;

    List<TramiteDraftResponseDto> getAllTramiteList(String userName);

    FotocopiasRequestDto getDraft(Long pktramite);
}
