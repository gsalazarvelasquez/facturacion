package ch.swissbytes.fundempresa.ventanilla.services.internal;

import ch.swissbytes.fundempresa.ventanilla.services.BasicService;
import ch.swissbytes.shared.persistence.Repository;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by daniel on 24/02/2016.
 */
public abstract class BasicServiceImpl implements BasicService{

    @Inject
    private Repository repository;

    protected abstract Class getPersistenceClass();

    @Override
    public <T> List<T> getAll() {
        List<T> result = this.repository.findAll(getPersistenceClass());
        return result;
    }

    @Override
    public <T> T getById() {
        return null;
    }

    @Override
    public <T> void save(T entity) {

    }

    @Override
    public <T> void delete(T entity) {

    }
}
