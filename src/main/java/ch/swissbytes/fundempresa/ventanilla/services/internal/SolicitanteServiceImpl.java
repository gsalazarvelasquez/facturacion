package ch.swissbytes.fundempresa.ventanilla.services.internal;

import ch.swissbytes.fundempresa.ventanilla.data.model.Solicitante;
import ch.swissbytes.fundempresa.ventanilla.data.repository.SolicitanteRepositoryImpl;
import ch.swissbytes.fundempresa.ventanilla.services.SolicitanteService;

import javax.inject.Inject;
import java.util.Optional;

/**
 * Created by daniel on 16/02/2016.
 */
public class SolicitanteServiceImpl implements SolicitanteService {

    @Inject
    private SolicitanteRepositoryImpl solicitanteRepository;

    @Override
    public Solicitante getSolicitanteById(Long id) {
        Optional<Solicitante> result = solicitanteRepository.findById(Solicitante.class, id);
        return result.get();
    }
}
