package ch.swissbytes.fundempresa.ventanilla.services;

import ch.swissbytes.fundempresa.ventanilla.exception.UserNotFoundException;
import ch.swissbytes.shared.security.dto.AccountDto;
import ch.swissbytes.shared.security.dto.UsernamePasswordCredential;

/**
 * Created by daniel on 17/05/2016.
 */
public interface AuthenticationService {

    AccountDto authenticate(UsernamePasswordCredential usernamePasswordCredential) throws UserNotFoundException;
}
