package ch.swissbytes.fundempresa.ventanilla.services.internal;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasc.interfaces.VentaBDAdabasService;
import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.adabasd.dto.EmpresasDto;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import ch.swissbytes.fundempresa.ventanilla.data.model.EstadisticaBasica;
import ch.swissbytes.fundempresa.ventanilla.data.model.TipoEstadisticaBasica;
import ch.swissbytes.fundempresa.ventanilla.data.repository.VentaBDRepositoryImpl;
import ch.swissbytes.fundempresa.ventanilla.services.VentaBDListadoService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;

import javax.inject.Inject;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Slf4j
public class VentaBDListadoServiceImpl implements VentaBDListadoService {

    @Inject
    private VentaBDAdabasService ventaBDAdabasService;

    @Inject
    private VentaBDRepositoryImpl ventaBDRepository;

    @Override
    public Long saveDraft(VentaBDListadoDto requestDto) {
        return null;
    }

    @Override
    public VentaBDListadoDto getDraft(Long pktramite) {
        return null;
    }

    @Override
    public VentaBDResponseDto ingresarSolicitud(VentaBDListadoDto requestDto, String servicio) throws AdabasCRequestException {
        VentaBDRequestAdabasDto ventaBdRequestDto = new VentaBDRequestAdabasDto();

        UserProfileDto userProfileDto = AdabasD.profile(requestDto.getUserName());
        String codigoServicio = AdabasD.codigoServicio(servicio);
        ventaBdRequestDto.setCodServicio(codigoServicio);
        ventaBdRequestDto.setSolicitante(userProfileDto.getFullName());
        ventaBdRequestDto.setCi(userProfileDto.getCi());
        ventaBdRequestDto.setExpci(userProfileDto.getExpci());
        ventaBdRequestDto.setTelefono(userProfileDto.getPhone());
        ventaBdRequestDto.setEmail(userProfileDto.getEmail());
        ventaBdRequestDto.setUsoInformacion(requestDto.getUsoInformacion());
        ventaBdRequestDto.setPayAmount(requestDto.getPayAmount());
        ventaBdRequestDto.setDescripcion(requestDto.getUsoInformacion());
        ventaBdRequestDto.setSucursal(requestDto.getSucursal());

        List<VentaBDVariablesAdabasDto> variables = getVariablesListado(requestDto);
        ventaBdRequestDto.setVariables(variables);

        VentaBDResponseDto ventaBdResponseDto = ventaBDAdabasService.solicitar(ventaBdRequestDto);
        return ventaBdResponseDto;
    }

    @Override
    public void descargar(OutputStream os, VentaBDListadoDto requestDto){
        List<EmpresasDto> listado = AdabasD.download(requestDto);
        try {
            //CREATE SHEET
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("Empresas");

            //CREATE ROW
            HSSFRow rowhead = sheet.createRow((short) 0);

            //CREATE CELLS FOR FIRST ROW (HEAD)
            HSSFCell cell00 = rowhead.createCell(0);
            HSSFCell cell01 = rowhead.createCell(1);
            HSSFCell cell02 = rowhead.createCell(2);
            HSSFCell cell03 = rowhead.createCell(3);
            HSSFCell cell04 = rowhead.createCell(4);
            HSSFCell cell05 = rowhead.createCell(5);
            HSSFCell cell06 = rowhead.createCell(6);
            HSSFCell cell07 = rowhead.createCell(7);
            HSSFCell cell08 = rowhead.createCell(8);
            HSSFCell cell09 = rowhead.createCell(9);

            //PUT VALUES TO HEAD ROW
            cell00.setCellValue("MATRICULA");
            cell01.setCellValue("RAZON SOCIAL");
            cell02.setCellValue("TIPO SOCIETARIO");
            cell03.setCellValue("DEPARTAMENTO");
            cell04.setCellValue("MUNICIPIO");
            cell05.setCellValue("ZONA");
            cell06.setCellValue("DIRECCION COMERCIAL");
            cell07.setCellValue("TELEFONO");
            cell08.setCellValue("FAX");
            cell09.setCellValue("ACTIVIDAD");

            //CREATE STYLE FOR FIRST ROW
            Font font = workbook.createFont();
            font.setColor(HSSFColor.WHITE.index);
            font.setBold(true);
            CellStyle cellStyle = workbook.createCellStyle();
            cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
            cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
            cellStyle.setBorderBottom(CellStyle.BORDER_THICK);
            cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            cellStyle.setFillBackgroundColor(IndexedColors.BLUE_GREY.getIndex());
            cellStyle.setFillForegroundColor(IndexedColors.BLUE_GREY.getIndex());
            cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            cellStyle.setFont(font);

            //ASSIGN STYLE FOR FIRST-ROW CELLS
            cell00.setCellStyle(cellStyle);
            cell01.setCellStyle(cellStyle);
            cell02.setCellStyle(cellStyle);
            cell03.setCellStyle(cellStyle);
            cell04.setCellStyle(cellStyle);
            cell05.setCellStyle(cellStyle);
            cell06.setCellStyle(cellStyle);
            cell07.setCellStyle(cellStyle);
            cell08.setCellStyle(cellStyle);
            cell09.setCellStyle(cellStyle);

            //FILL ROWS WITH RECEIVED DATA
            for (int i = 0; i < listado.size(); i++) {
                EmpresasDto item = listado.get(i);
                HSSFRow row = sheet.createRow((short) i+1);
                row.createCell(0).setCellValue(item.getMatricula());
                row.createCell(1).setCellValue(item.getRazonSocial());
                row.createCell(2).setCellValue(item.getTipoSocietario());
                row.createCell(3).setCellValue(item.getDepartamento());
                row.createCell(4).setCellValue(item.getMunicipio());
                row.createCell(5).setCellValue(item.getZona());
                row.createCell(6).setCellValue(item.getDireccionComercial());
                row.createCell(7).setCellValue(item.getTelefono());
                row.createCell(8).setCellValue(item.getFax());
                row.createCell(9).setCellValue(item.getActividad());
            }

            //RESIZE COLUMNS WIDTH TO AUTOMATIC LONGEST
            sheet.autoSizeColumn(0);
            sheet.autoSizeColumn(1);
            sheet.autoSizeColumn(2);
            sheet.autoSizeColumn(3);
            sheet.autoSizeColumn(4);
            sheet.autoSizeColumn(5);
            sheet.autoSizeColumn(6);
            sheet.autoSizeColumn(7);
            sheet.autoSizeColumn(8);
            sheet.autoSizeColumn(9);

            //FIX FIRST ROW HEADER
            sheet.createFreezePane(0, 1);

            //WRITE CHANGES AND CLOSE CONNECTIONS
            workbook.write(os);
            os.flush();
            os.close();
            System.out.println("Excel generated!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<VentaBDVariablesAdabasDto> getVariablesListado(VentaBDListadoDto requestDto) {
        List<VentaBDVariablesAdabasDto> response = new ArrayList<VentaBDVariablesAdabasDto>();

        //ADD DEPARTAMENTOS
        VentaBDVariablesAdabasDto dptos = new VentaBDVariablesAdabasDto();
        dptos.setVariable("TODOS_DEPARTAMENTOS");
        dptos.setValor(String.valueOf(requestDto.isTodosDepartamentos()));
        response.add(dptos);
        if(!requestDto.isTodosDepartamentos()) {
            List<String> listado = requestDto.getDepartamentos();
            if(listado != null && !listado.isEmpty()){
                VentaBDVariablesAdabasDto items = str(listado, "DEPARTAMENTOS");
                response.add(items);
            }
            listado = requestDto.getMunicipios();
            if(listado != null && !listado.isEmpty()){
                VentaBDVariablesAdabasDto items = str(listado, "MUNICIPIOS");
                response.add(items);
            }
        }

        //ADD TIPOS_SOCIEDAD
        VentaBDVariablesAdabasDto tSoc = new VentaBDVariablesAdabasDto();
        tSoc.setVariable("TODOS_TIPOS_SOCIEDAD");
        tSoc.setValor(String.valueOf(requestDto.isTodosTiposSociedad()));
        response.add(tSoc);
        if(!requestDto.isTodosTiposSociedad()) {
            List<String> listado = requestDto.getTiposSociedad();
            if(listado != null && !listado.isEmpty()){
                VentaBDVariablesAdabasDto items = str(listado, "TIPOS_SOCIEDAD");
                response.add(items);
            }
        }

        //ADD ESTADOS
        VentaBDVariablesAdabasDto states = new VentaBDVariablesAdabasDto();
        states.setVariable("TODOS_ESTADO_MATRICULA");
        states.setValor(String.valueOf(requestDto.isTodosEstados()));
        response.add(states);
        if(!requestDto.isTodosEstados()) {
            List<String> listado = requestDto.getEstados();
            if(listado != null && !listado.isEmpty()){
                VentaBDVariablesAdabasDto items = str(listado, "ESTADO_MATRICULA");
                response.add(items);
            }
        }

        //ADD ANHOS_INSCRITAS
        String inscritas = requestDto.getAnhosInscritas();
        inscritas = inscritas == null ? "" : inscritas;
        if(!inscritas.isEmpty()){
            VentaBDVariablesAdabasDto item = new VentaBDVariablesAdabasDto();
            item.setVariable("ANHOS_INSCRITAS");
            item.setValor(inscritas);
            response.add(item);
        }

        //ADD ANHOS_CANCELADAS
        String canceladas = requestDto.getAnhosCanceladas();
        canceladas = canceladas == null ? "" : canceladas;
        if(!canceladas.isEmpty()){
            VentaBDVariablesAdabasDto item = new VentaBDVariablesAdabasDto();
            item.setVariable("ANHOS_CANCELADAS");
            item.setValor(canceladas);
            response.add(item);
        }

        //ADD SECCIONES
        VentaBDVariablesAdabasDto secciones = new VentaBDVariablesAdabasDto();
        secciones.setVariable("TODOS_SECCIONES");
        secciones.setValor(String.valueOf(requestDto.isTodasSecciones()));
        response.add(secciones);
        if(!requestDto.isTodasSecciones()) {
            List<String> listado = requestDto.getSecciones();
            if(listado != null && !listado.isEmpty()){
                VentaBDVariablesAdabasDto items = str(listado, "SECCIONES");
                response.add(items);
            }
            listado = requestDto.getClasesCIIU();
            if(listado != null && !listado.isEmpty()){
                VentaBDVariablesAdabasDto items = str(listado, "CLASES_CIIU");
                response.add(items);
            }
        }

        return response;
    }

    private VentaBDVariablesAdabasDto str(List<String> listado, String variable) {
        VentaBDVariablesAdabasDto response = new VentaBDVariablesAdabasDto();
        String valores = "";
        int max = listado.size();
        for (int i = 0; i < max; i++) {
            String item = listado.get(i);
            valores += item;
            valores += (i + 1 < max) ? "," : "";
        }
        response.setVariable(variable);
        response.setValor(valores);
        return response;
    }
}
