package ch.swissbytes.fundempresa.ventanilla.services.internal;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasc.interfaces.CertificadosAdabasService;
import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.billing.services.internal.ParametrosPago;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import ch.swissbytes.fundempresa.ventanilla.data.model.CertificadoEspecial;
import ch.swissbytes.fundempresa.ventanilla.data.model.CodigosSeguridad;
import ch.swissbytes.fundempresa.ventanilla.data.model.TipoCertificado;
import ch.swissbytes.fundempresa.ventanilla.data.repository.CertificadoRepositoryImpl;
import ch.swissbytes.fundempresa.ventanilla.data.repository.CodigosSeguridadRepositoryImpl;
import ch.swissbytes.fundempresa.ventanilla.services.CertificadosService;
import lombok.extern.slf4j.Slf4j;
import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

@Slf4j
public class CertificadosServiceImpl implements CertificadosService {

    @Inject
    private CodigosSeguridadRepositoryImpl codigosSeguridadRepositoryImpl;

    @Inject
    private CertificadosAdabasService certificadosAdabasService;

    @Inject
    private CertificadoRepositoryImpl certificadoRepository;

    private final String ADABASC_FIELD_ESPECIALES = "VVSERVCE";
    private final String ADABASC_FIELD_ENLINEA = "SLCertificados";
    private final String ADABASC_FIELD_TRADICION = "VVSERVCT";
    private final String ADABASC_FIELD_NOINSCRIPCION = "VVSERVCN";

    @Override
    public CertificadosResponseDto especial(CertificadosRequestDto certificadosRequestDto) throws AdabasCRequestException {
        String matricula = certificadosRequestDto.getMatricula();
        String username = certificadosRequestDto.getUserName();
        ResponseDto resp = new ResponseDto();
//        resp.setArancel(certificadosRequestDto.getMontoPagado());
        resp.setArancel(certificadosRequestDto.getPayAmount());
        resp.setDescTramite(getTipo(certificadosRequestDto.getCertificado()));
        AdabasD.transaction(resp, username, matricula, "", "");

        UserProfileDto userProfileDto = AdabasD.profile(certificadosRequestDto.getUserName());
        CertificadosResponseDto certificadosResponseDto = certificadosAdabasService.especial(certificadosRequestDto, userProfileDto);

        //persist to local db
        certificadosResponseDto.setEstadoTramite(TramiteResponseDto.EstadoTramite.PROCESADO.name()); //TODO: ask what state corresponds

        if (certificadosRequestDto.getPkTramite() == null){
            CertificadoEspecial certificadoEspecial = new CertificadoEspecial();
            setObjectSubmit(certificadoEspecial, certificadosRequestDto, certificadosResponseDto);
            certificadoEspecial.setEstadoTramite(certificadosResponseDto.getEstadoTramite());
            certificadoEspecial.setFechaCreacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));
            certificadoEspecial.setFechaIngreso(new Timestamp(Calendar.getInstance().getTimeInMillis())); //TODO: En saveDraft poner fecha de creacion

            certificadoRepository.save(certificadoEspecial);
            log.info("Nueva transaccion registrada en la base de datos.");
        } else {
            CertificadoEspecial previous = certificadoRepository.getById(CertificadoEspecial.class, certificadosRequestDto.getPkTramite()).get();
            previous.setEstadoTramite(certificadosResponseDto.getEstadoTramite());
            setObjectSubmit(previous, certificadosRequestDto, certificadosResponseDto);
            previous.setFechaIngreso(new Timestamp(Calendar.getInstance().getTimeInMillis()));
            certificadoRepository.update(previous);
            log.info("Transaccion actualizada en la base de datos.");
        }

        return certificadosResponseDto;
    }

    private void setObjectSubmit(CertificadoEspecial certificadoEspecial, CertificadosRequestDto request, CertificadosResponseDto response) {

        certificadoEspecial.setId(request.getPkTramite());

        certificadoEspecial.setMatricula(request.getMatricula());
        certificadoEspecial.setCodigoCertificado(request.getCertificado());
        certificadoEspecial.setCantidadEjemplares(Long.parseLong(request.getEjemplares()));
        certificadoEspecial.setDescripcion(request.getDescripcion());
        certificadoEspecial.setSucursal(request.getSucursal());

        certificadoEspecial.setUserName(request.getUserName());
        certificadoEspecial.setPayCode(request.getPayCode());
        certificadoEspecial.setPayMode(request.getPayMode());
        certificadoEspecial.setPayAmount(request.getPayAmount());

        certificadoEspecial.setCodigoTramite(response.getCodigoTramite());
        certificadoEspecial.setCodigoError(response.getCodigoError());
        certificadoEspecial.setMensajeError(response.getMensajeError());
//        certificadoEspecial.setEstadoTramite(response.getEstadoTramite());
//        certificadoEspecial.setFechaCreacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        certificadoEspecial.setRequestXml(response.getRequestXml());
        certificadoEspecial.setResponseXml(response.getResponseXml());
    }

    @Override
    public CertificadosResponseDto enlinea(CertificadosRequestDto certificadosRequestDto) throws AdabasCRequestException {
        String matricula = certificadosRequestDto.getMatricula();
        String username = certificadosRequestDto.getUserName();
        ResponseDto resp = new ResponseDto();
        resp.setArancel(certificadosRequestDto.getPayAmount());
        resp.setDescTramite(ADABASC_FIELD_ENLINEA);
        AdabasD.transaction(resp, username, matricula, "", "");

        UserProfileDto userProfileDto = AdabasD.profile(certificadosRequestDto.getUserName());
        CertificadosResponseDto certificadosResponseDto = certificadosAdabasService.enlinea(certificadosRequestDto, userProfileDto);



        return certificadosResponseDto;
    }

    @Override
    public Long saveDraftEspeciales(CertificadosRequestDto request){
        Long result = null;
        try {
            if (request.getPkTramite() == null){
                CertificadoEspecial entity = getNewEspecialesBasic(request);
                certificadoRepository.saveAndFlush(entity);
                result = entity.getId();
            }else{
                CertificadoEspecial entity = certificadoRepository.getById(CertificadoEspecial.class, request.getPkTramite()).get();
                updateEspecialesDraft(request, entity);
                certificadoRepository.update(entity);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Long saveDraftEnlinea(CertificadosRequestDto request){
        Long result = null;
        try {
            if (request.getPkTramite() == null){
//                Certificado entity = getNewEnlineaBasic(request);
//                certificadoRepository.saveAndFlush(entity);
//                result = entity.getId();
            }else{
                CertificadoEspecial entity = certificadoRepository.getById(CertificadoEspecial.class, request.getPkTramite()).get();
                updateEspecialesDraft(request, entity);
                certificadoRepository.update(entity);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    private void updateEspecialesDraft(CertificadosRequestDto request, CertificadoEspecial especial) {
        especial.setId(request.getPkTramite());
        especial.setMatricula(request.getMatricula());
        especial.setCodigoCertificado(request.getCertificado());
        especial.setCantidadEjemplares(Long.parseLong(request.getEjemplares()));
        especial.setDescripcion(request.getDescripcion());
        especial.setSucursal(request.getSucursal());
        especial.setUserName(request.getUserName());
        especial.setPayCode(request.getPayCode());
        especial.setPayMode(request.getPayMode());
        especial.setPayAmount(request.getPayAmount());
        especial.setEstadoTramite(getEstadoTramite(request));
        especial.setFechaCreacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));
    }

    private void updateEnlineaDraft(CertificadosRequestDto request, CertificadoEspecial especial) {
        especial.setId(request.getPkTramite());
        especial.setMatricula(request.getMatricula());
        especial.setCodigoCertificado(request.getCertificado());
        especial.setCantidadEjemplares(Long.parseLong(request.getEjemplares()));
        especial.setDescripcion(request.getDescripcion());
        especial.setSucursal(request.getSucursal());
        especial.setUserName(request.getUserName());
        especial.setPayCode(request.getPayCode());
        especial.setPayMode(request.getPayMode());
        especial.setPayAmount(request.getPayAmount());
        especial.setEstadoTramite(getEstadoTramite(request));
        especial.setFechaCreacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));
    }

    private CertificadoEspecial getNewEspecialesBasic(CertificadosRequestDto request) {
        CertificadoEspecial especial = new CertificadoEspecial();
        especial.setId(request.getPkTramite());
        especial.setMatricula(request.getMatricula());
        especial.setCodigoCertificado(request.getCertificado());
        especial.setCantidadEjemplares(Long.parseLong(request.getEjemplares()));
        especial.setDescripcion(request.getDescripcion());
        especial.setSucursal(request.getSucursal());
        especial.setUserName(request.getUserName());
        especial.setPayCode(request.getPayCode());
        especial.setPayMode(request.getPayMode());
        especial.setPayAmount(request.getPayAmount());
        especial.setEstadoTramite(getEstadoTramite(request));
        especial.setFechaCreacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        return especial;
    }

    private CertificadoEspecial getNewEnlineaBasic(CertificadosRequestDto request) {
        CertificadoEspecial especial = new CertificadoEspecial();
        especial.setId(request.getPkTramite());
        especial.setMatricula(request.getMatricula());
        especial.setCodigoCertificado(request.getCertificado());
        especial.setCantidadEjemplares(Long.parseLong(request.getEjemplares()));
        especial.setDescripcion(request.getDescripcion());
        especial.setSucursal(request.getSucursal());
        especial.setUserName(request.getUserName());
        especial.setPayCode(request.getPayCode());
        especial.setPayMode(request.getPayMode());
        especial.setPayAmount(request.getPayAmount());
        especial.setEstadoTramite(getEstadoTramite(request));
        especial.setFechaCreacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        return especial;
    }

    private String getEstadoTramite(CertificadosRequestDto request) {
        String estadoTramite;
        if (!request.getPayCode().isEmpty()){
            if (request.getPkTramite() == null){
                estadoTramite = TramiteResponseDto.EstadoTramite.PAGADO.name();
            } else {
                estadoTramite = TramiteResponseDto.EstadoTramite.PROCESADO.name();
            }
        } else {
            estadoTramite = TramiteResponseDto.EstadoTramite.PENDIENTE_PAGO.name();
        }
        return estadoTramite;
    }

    @Override
    public CertificadosResponseDto validar(CertificadosRequestDto certificadosRequestDto) throws AdabasCRequestException {
        CertificadosResponseDto certificadosResponseDto = certificadosAdabasService.validar(certificadosRequestDto);
        return certificadosResponseDto;
    }

    @Override
    public List<TipoCertificado> getTipoCertificadoList(String tipo) {
        List<TipoCertificado> result = certificadoRepository.getTipoCertificadoListByTipo(tipo);

        return result;
    }

    private CodigosSeguridad getNewCodigoSeguridad(String username, long codigo) {
        CodigosSeguridad codigoSeguridad = new CodigosSeguridad();
        codigoSeguridad.setFecha(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        codigoSeguridad.setTramite(1);
        codigoSeguridad.setUsername(username);
        codigoSeguridad.setCodigo(codigo);
        return codigoSeguridad;
    }

    private long getCodigo() {
        long codigo = 0L;
        try {
            List<Long> codigosSeguridad = codigosSeguridadRepositoryImpl.getLast();
            codigo = (codigosSeguridad.get(0) ^ Long.parseLong(ParametrosPago.securityCode)) + 1;
        } catch (Exception e) {
            codigo = 0L;
        }
        return codigo ^ (Long.parseLong(ParametrosPago.securityCode));
    }

    private String getTipo(String tipo) {
        if(tipo.equals("01010401"))
            return ADABASC_FIELD_NOINSCRIPCION;
        if(tipo.equals("01010308"))
            return ADABASC_FIELD_TRADICION;
//        if(tipo.equals("01010307"))
        return ADABASC_FIELD_ESPECIALES;
    }
}
