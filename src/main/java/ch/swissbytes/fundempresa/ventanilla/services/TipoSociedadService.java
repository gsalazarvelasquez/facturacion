package ch.swissbytes.fundempresa.ventanilla.services;




import ch.swissbytes.fundempresa.adabasc.exception.AdabasDRequestException;
import ch.swissbytes.fundempresa.ventanilla.data.model.TipoSociedad;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by miguel on 1/26/16.
 */
@Local
public interface TipoSociedadService {

      List<TipoSociedad> findAll() throws AdabasDRequestException;
}
