package ch.swissbytes.fundempresa.ventanilla.services;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;

import javax.ejb.Local;
import java.math.BigDecimal;

/**
 * Created by miguel on 1/26/16.
 */
@Local
public interface PreciosService {

    PreciosResponseDto tramite(PreciosRequestDto preciosRequestDto) throws AdabasCRequestException;

    BigDecimal listadoEmpresas(VentaBDListadoDto requestDto);

    BigDecimal estadisticaBasica(VentaBDEstBasicaDto requestDto);

    BigDecimal cruceVariables(VentaBDCruceVarDto requestDto);

    TramiteResponseDto registrarPago(String username, String matricula, String pago,
                                     String orderId, String monto, String servicio, String descripcion);
}