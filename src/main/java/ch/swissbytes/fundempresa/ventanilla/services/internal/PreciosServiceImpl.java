package ch.swissbytes.fundempresa.ventanilla.services.internal;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasc.interfaces.PreciosAdabasService;
import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import ch.swissbytes.fundempresa.ventanilla.data.model.VentabdParametros;
import ch.swissbytes.fundempresa.ventanilla.services.PreciosService;
import ch.swissbytes.fundempresa.ventanilla.services.VentaBDParametrosService;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by jorgeburgos on 1/30/16.
 */
public class PreciosServiceImpl implements PreciosService {

    @Inject
    private PreciosAdabasService preciosAdabasService;

    private static final int TRAMITE_HOMONIMIA = 1;
    private static final int TRAMITE_FOTOCOPIAS = 2;
    private static final int TRAMITE_CERTIFICADOS = 3;

    private static String ADABASC_FIELD_HOMONIMIA = "VVSERVHOM";
    private static String ADABASC_FIELD_FOTOCOPIAS = "VVSERVSLF";
    private static String ADABASC_FIELD_CERTIFICADOS = "VVSERVCER";

    @Inject
    public VentaBDParametrosService ventaBDParametrosService;

    @Override
    public PreciosResponseDto tramite(PreciosRequestDto preciosRequestDto) throws AdabasCRequestException {
        int tipoTramite = preciosRequestDto.getTipoTramite();
        String tipoSociedad = preciosRequestDto.getTipoSociedad();
        String matricula = preciosRequestDto.getMatricula();

        //Por defecto: tramite = CONTROL DE HOMONIMIA
        String tramite = ADABASC_FIELD_HOMONIMIA;
        int factor = 1;

        switch (tipoTramite){
            case TRAMITE_HOMONIMIA:{
                // tramite = CONTROL DE HOMONIMIA
                tramite = ADABASC_FIELD_HOMONIMIA;
                break;
            }
            case TRAMITE_FOTOCOPIAS:{
                // tramite = SOLICITUD DE FOTOCOPIAS
                tramite = ADABASC_FIELD_FOTOCOPIAS;
                factor = preciosRequestDto.getDocsSeleccionados();
                factor = factor == 0 ? 1 : factor;
                break;
            }
            case TRAMITE_CERTIFICADOS:{
                // tramite = SOLICITUD DE CERTIFICADOS EN LINEA
                tramite = ADABASC_FIELD_CERTIFICADOS;
                factor = preciosRequestDto.getDocsSeleccionados();
                factor = factor == 0 ? 1 : factor;
                break;
            }
            //AUMENTAR LOS CODIGOS DE LOS DEMAS TRAMITES
        }

        String servicio = AdabasD.codigoServicio(tramite);
        PreciosResponseDto arancel = new PreciosResponseDto();
        if(servicio.length() > 7){
            matricula = (matricula == null || matricula.length() < 2) ? ("VV00000" + tipoSociedad) : matricula;
            arancel = preciosAdabasService.tramite(matricula, servicio);
            BigDecimal price = new BigDecimal(arancel.getArancel());
            arancel.setArancel(price.multiply(new BigDecimal(factor)).toPlainString());
        }else{
            arancel.setCodigoError("101");
            arancel.setMensajeError("No se encontro el codigo de servicio para el tramite.");
            arancel.setArancel("0.00");
        }
        return arancel;
    }

    @Override
    public TramiteResponseDto registrarPago(String username, String matricula, String pago, String orderId, String monto, String servicio, String descripcion) {
        return preciosAdabasService.registrarPago(username, matricula, pago, orderId, monto, servicio, descripcion);
    }

    @Override
    public BigDecimal listadoEmpresas(VentaBDListadoDto requestDto) {
        /**************************    VALORES CONSTANTES     *************************/
        Optional<VentabdParametros> param = ventaBDParametrosService.getConfig();
        VentabdParametros parametros = param.get();

        BigDecimal IVA = new BigDecimal(parametros.getIva());
        BigDecimal basePrice = new BigDecimal(parametros.getListadoPrecioBase());
        BigDecimal unitPrice = new BigDecimal(parametros.getListadoPrecioRegistro());

        /**************************    CALCULO DE FORMULA     *************************/
        BigDecimal arancel = filtrarEmpresas(requestDto, unitPrice);
        arancel = arancel.add(basePrice).multiply(BigDecimal.ONE.add(IVA));
        arancel = arancel.setScale(2, RoundingMode.HALF_UP);
        return arancel;
    }

    @Override
    public BigDecimal estadisticaBasica(VentaBDEstBasicaDto requestDto) {
        /**************************    VALORES CONSTANTES     *************************/
        Optional<VentabdParametros> param = ventaBDParametrosService.getConfig();
        VentabdParametros parametros = param.get();

        BigDecimal IVA = new BigDecimal(parametros.getIva());
        BigDecimal mensual = new BigDecimal(parametros.getEstbasicaMensuales());
        BigDecimal departamentales = new BigDecimal(parametros.getEstbasicaDepartamentales());
        BigDecimal actEconomica = new BigDecimal(parametros.getEstbasicaActivEconomica());
        BigDecimal anuario = new BigDecimal(parametros.getEstbasicaAnuario());
        BigDecimal datosFinan = new BigDecimal(parametros.getEstbasicaDatosFinancieros());
        /**************************    CALCULO DE FORMULA     *************************/
        BigDecimal arancel = BigDecimal.ZERO;
        if(requestDto != null){
            if(requestDto.isMensuales())
                arancel = arancel.add(mensual);
            if(requestDto.isDepartamentales())
                arancel = arancel.add(departamentales);
            if(requestDto.isActEconomica())
                arancel = arancel.add(actEconomica);
            if(requestDto.isAnuarioEstadistico())
                arancel = arancel.add(anuario);
            if(requestDto.isDatosFinancieros())
                arancel = arancel.add(datosFinan);
            if(arancel.compareTo(BigDecimal.ZERO) > 0)
                arancel = arancel.multiply(BigDecimal.ONE.add(IVA));
        }
        arancel = arancel.setScale(2, RoundingMode.HALF_UP);
        return arancel;
    }

    @Override
    public BigDecimal cruceVariables(VentaBDCruceVarDto requestDto) {

        List<Integer> qvars = AdabasD.variablesCruceVars();
        int allDptos = qvars.get(0);
        int allMuncp = qvars.get(1);
        int allTipSoc = qvars.get(2);
        int allClases = qvars.get(3);
        int allFinan = qvars.get(4);
        BigDecimal variables = new BigDecimal(allFinan + allDptos + allMuncp + allTipSoc + allClases);

        //OBTENER DEL REQUEST
        int financ = variablesFinancieras(requestDto);
        int dptos = requestDto.isTodosDptos() ? allDptos : requestDto.getDepartamentos() != null ? requestDto.getDepartamentos().size() : 0;
        int municipios = requestDto.isTodosDptos() ? allMuncp : municipios(requestDto);
        int tipoSoc = requestDto.isTodosTipoSoc() ? allTipSoc : requestDto.getTiposSociedad() != null ? requestDto.getTiposSociedad().size() : 0;
        int clases = requestDto.isTodasSeccion() ? allClases : clases(requestDto);
        BigDecimal solicitadas = new BigDecimal(financ + dptos + municipios + tipoSoc + clases);

        /**************************    VALORES CONSTANTES     *************************/
        Optional<VentabdParametros> param = ventaBDParametrosService.getConfig();
        VentabdParametros parametros = param.get();

        BigDecimal IVA = new BigDecimal(parametros.getIva());
        BigDecimal costoFijo = new BigDecimal(parametros.getCrucevarCostoFijo());
        BigDecimal costoVar = new BigDecimal(parametros.getCrucevarCostoVar());
        BigDecimal costoTotal = costoFijo.add(costoVar);
        BigDecimal coeficiente = solicitadas.divide(variables, 2, RoundingMode.HALF_UP);
        BigDecimal costoUnitario = costoTotal.divide(variables, 2, RoundingMode.HALF_UP);
        BigDecimal costoSolicitud = costoUnitario.multiply(coeficiente);
        costoSolicitud = costoSolicitud.add(costoUnitario);
        BigDecimal precioUnitario = new BigDecimal(parametros.getCrucevarPrecioRegistro());

        /**************************    CALCULO DE FORMULA     *************************/
        BigDecimal arancel = costoSolicitud.multiply(precioUnitario);
        arancel = arancel.multiply(BigDecimal.ONE.add(IVA));
        arancel = arancel.setScale(2, RoundingMode.HALF_UP);
        return arancel;
    }

    private int variablesFinancieras(VentaBDCruceVarDto requestDto) {
        int response = 0;
        if(requestDto.getVFinancActivos() != null && !requestDto.getVFinancActivos().isEmpty())
            response ++;
        if(requestDto.getVFinancPatrimonio() != null && !requestDto.getVFinancPatrimonio().isEmpty())
            response ++;
        if(requestDto.getVFinancVentas() != null && !requestDto.getVFinancVentas().isEmpty())
            response ++;
        if(requestDto.getVFinancUtilidades() != null && !requestDto.getVFinancUtilidades().isEmpty())
            response ++;
        if (requestDto.getVFinancCapital() != null && !requestDto.getVFinancCapital().isEmpty())
            response ++;
        return response;
    }

    private int municipios(VentaBDCruceVarDto ventaBd) {
        //TODOS LOS MUNICIPIOS QUE ABARQUEN LOS DEPARTAMENTOS SELECCIONADOS
        int municipios = AdabasD.municipios(ventaBd.getDepartamentos());
        //TODOS LOS MUNICIPIOS SELECCIONADOS EXTRAS A LOS DEPARTAMENTOS
        municipios += ventaBd.getMunicipios() != null ? ventaBd.getMunicipios().size() : 0;
        return municipios;
    }

    private int clases(VentaBDCruceVarDto ventaBd) {
        //TODAS LAS CLASES_CIIU QUE ABARQUEN LAS SECCIONES SELECCIONADAS
        int clases = AdabasD.clases(ventaBd.getSecciones());
        //TODAS LAS CLASES_CIIU SELECCIONADAS EXTRAS A LAS SECCIONES
        clases += ventaBd.getClasesCIIU() != null ? ventaBd.getClasesCIIU().size() : 0;
        return clases;
    }

    private BigDecimal filtrarEmpresas(VentaBDListadoDto requestDto, BigDecimal unitPrice) {
        List<ListadoCalcDto> listado = AdabasD.listadoEmpresas(requestDto);
        BigDecimal response = BigDecimal.ZERO;
        for (ListadoCalcDto item : listado) {
            BigDecimal quantity = new BigDecimal(item.getCantidad());
            BigDecimal factor = new BigDecimal(item.getFactor());
            response = response.add(quantity.multiply(factor).multiply(unitPrice));
        }
        return response;
    }
}
