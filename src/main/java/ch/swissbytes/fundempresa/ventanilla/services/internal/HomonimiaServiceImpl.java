package ch.swissbytes.fundempresa.ventanilla.services.internal;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasc.interfaces.HomonimiaAdabasService;
import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import ch.swissbytes.fundempresa.ventanilla.data.model.Homonimia;
import ch.swissbytes.fundempresa.ventanilla.services.HomonimiaService;
import ch.swissbytes.fundempresa.ventanilla.data.repository.HomonimiaRepositoryImpl;
import ch.swissbytes.fundempresa.ventanilla.services.PreciosService;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

//@Slf4j
public class HomonimiaServiceImpl implements HomonimiaService {

    @Inject
    private HomonimiaRepositoryImpl homonimiaRepository;

    @Inject
    private HomonimiaAdabasService homonimiaAdabasService;

    @Inject
    private PreciosService preciosService;

    @Override
    public HomonimiaResponseDto ingresarHomonimia(HomonimiaRequestDto homonimiaRequestDto) throws AdabasCRequestException {

        /**
         * 1. Ingresar Tramite a ADABAS -> XMLResponse
         * 2. Set XMLResponse en HomonimiaRequestDto
         * 3. Convertir XMLResponse a HomonimiaResponseDto(ResponseDto)
         * 4. Convertir HomonimiaResponseDto en Homonimia de modelo
         * 5. Persistir new Homonimia
         * 4. Retornar HomonimiaResponseDto
         */

        UserProfileDto userProfileDto = AdabasD.profile(homonimiaRequestDto.getUserName());
        HomonimiaResponseDto homonimiaResponseDto = (HomonimiaResponseDto) homonimiaAdabasService.ingresarHomonimia(homonimiaRequestDto, userProfileDto);
        homonimiaResponseDto.setEstadoTramite(TramiteResponseDto.EstadoTramite.EN_PROCESO.name());

        Homonimia homonimia = getNewHomonimia(homonimiaRequestDto, homonimiaResponseDto);
        homonimia.setCodigoTramite(homonimiaResponseDto.getCodigoTramite());
        homonimia.setTipoIngreso(HomonimiaRequestDto.TIPO_INGRESO.NUEVO.name());

        if (homonimia.getPkHomonimia() == null){
            homonimiaRepository.save(homonimia);
        } else {
            Homonimia previous = homonimiaRepository.getByPk(homonimia.getPkHomonimia());
            if (previous.getFechaCreacion() != null){
                homonimia.setFechaCreacion(previous.getFechaCreacion());
            }
            homonimiaRepository.update(homonimia);
        }

        return homonimiaResponseDto;
    }

    @Override
    public HomonimiaResponseDto reingresarHomonimia(HomonimiaRequestDto homonimiaRequestDto) throws AdabasCRequestException {
        HomonimiaResponseDto homonimiaResponseDto = (HomonimiaResponseDto) homonimiaAdabasService.reingresarHomonimia(homonimiaRequestDto);

        Homonimia previousHomonimia = homonimiaRepository.getBy(Homonimia.class, "pkHomonimia", homonimiaRequestDto.getPkTramite()).get();
        previousHomonimia.setEstadoTramite(TramiteResponseDto.EstadoTramite.PROCESADO.name());
        previousHomonimia.setFechaActualizacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        homonimiaRepository.update(previousHomonimia);

        homonimiaResponseDto.setEstadoTramite(TramiteResponseDto.EstadoTramite.EN_PROCESO.name());
        homonimiaResponseDto.setCodigoTramite(previousHomonimia.getCodigoTramite());
        homonimiaResponseDto.setPinQuiosco(previousHomonimia.getPinQuiosco());

        Homonimia newHomonimia = getNewHomonimia(homonimiaRequestDto, homonimiaResponseDto);
        newHomonimia.setPkHomonimia(null);
        newHomonimia.setCodigoTramite(homonimiaRequestDto.getCodigoTramite());
        newHomonimia.setTipoIngreso(HomonimiaRequestDto.TIPO_INGRESO.REINGRESO.name());
        homonimiaRepository.save(newHomonimia);

        return homonimiaResponseDto;
    }

    @Override
    public HomonimiaResponseDto getEstadoHomonimia(String codigoTramite) throws AdabasCRequestException {

        HomonimiaResponseDto result = (HomonimiaResponseDto) homonimiaAdabasService.getEstadoHomonimia(codigoTramite);
        return result;
    }

    @Override
    public List<TramiteResponseDto> getListaPendientes(String userName) throws AdabasCRequestException {

        actualizarEstadosAdabas(userName);

        List<Homonimia> homonimiaList = homonimiaRepository.getAllListaPendientesByUser(userName);
        List<TramiteResponseDto> pendientesList = new LinkedList<TramiteResponseDto>();
        for (Homonimia homonimia: homonimiaList){
            TramiteResponseDto tramiteResponseDto = new HomonimiaResponseDto(homonimia);
            pendientesList.add(tramiteResponseDto);
        }

        return pendientesList;
    }

    @Override
    public List<TramiteDraftResponseDto> getAllTramiteList(String userName) throws AdabasCRequestException {
        actualizarEstadosAdabas(userName);

        List<Homonimia> homonimiaList = homonimiaRepository.getAllOrderByCreationDate();
        List<TramiteDraftResponseDto> responseResult = new LinkedList<TramiteDraftResponseDto>();
        for (Homonimia homonimia: homonimiaList){
            TramiteDraftResponseDto tramiteDraftResponseDto = new TramiteDraftResponseDto();
            tramiteDraftResponseDto.setTipoTramite(TramiteResponseDto.TipoTramite.HOMONIMIA.name());

            Date fechaCreacion = homonimia.getFechaCreacion() != null
                    ? new Date(homonimia.getFechaCreacion().getTime())
                    : null;
            tramiteDraftResponseDto.setFechaCreacion(fechaCreacion);

//            tramiteDraftResponseDto.setTramiteRequestDto(new HomonimiaRequestDto(homonimia));
            tramiteDraftResponseDto.setTramiteResponseDto(new HomonimiaResponseDto(homonimia));
            responseResult.add(tramiteDraftResponseDto);
        }

        return responseResult;
    }

    @Override
    public BigDecimal getPayAmountHomonimia(Integer tipoSocietarioId) throws AdabasCRequestException {
        PreciosRequestDto preciosRequestDto = new PreciosRequestDto();
        preciosRequestDto.setTipoTramite(1);
        preciosRequestDto.setTipoSociedad(String.valueOf(tipoSocietarioId));
        preciosRequestDto.setMatricula("0");

        PreciosResponseDto responseDto = preciosService.tramite(preciosRequestDto);

        return new BigDecimal(responseDto.getArancel());
    }

    @Override
    public Long saveDraft(HomonimiaRequestDto homonimiaRequestDto){
        Long result = null;
        try {
            if (homonimiaRequestDto.getPkTramite() == null){
                Homonimia homonimia = getNewHomonimiaDraft(homonimiaRequestDto);
                homonimiaRepository.saveAndFlush(homonimia);
                result = homonimia.getPkHomonimia();
            }else{
                Homonimia homonimia = homonimiaRepository.getByPk(homonimiaRequestDto.getPkTramite());
                updateHomonimiaDraft(homonimiaRequestDto, homonimia);
                homonimiaRepository.update(homonimia);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Homonimia get(String pkTramite) {
        return homonimiaRepository.getBy(Homonimia.class, "pkHomonimia", Long.parseLong(pkTramite)).get();
    }

    @Override
    public void update(Homonimia tramite) {
        homonimiaRepository.update(tramite);
    }

    @Override
    public HomonimiaRequestDto getDraft(Long pkHomonimia) {

        Homonimia homonimia = homonimiaRepository.getBy(Homonimia.class, "pkHomonimia", pkHomonimia).get();
        HomonimiaRequestDto result = new HomonimiaRequestDto(homonimia);

        return result;
    }

    private Homonimia getNewHomonimia(HomonimiaRequestDto request, HomonimiaResponseDto response) {

        Homonimia homonimia = getNewHomonimiaBasic(request);

        homonimia.setPayCode(request.getPayCode());
        homonimia.setPayMode(request.getPayMode());
        homonimia.setPrecio(request.getPayAmount());

        homonimia.setPinQuiosco(response.getPinQuiosco());
        homonimia.setCodigoError(response.getCodigoError());
        homonimia.setMensajeError(response.getMensajeError());
        homonimia.setEstadoTramite(response.getEstadoTramite());

        homonimia.setRequestXml(response.getRequestXml());
        homonimia.setResponseXml(response.getResponseXml());

        homonimia.setFechaCreacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        homonimia.setFechaIngreso(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        homonimia.setFechaActualizacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));

        return homonimia;
    }

    private Homonimia getNewHomonimiaBasic(HomonimiaRequestDto request) {

        Homonimia homonimia = new Homonimia();
        homonimia.setPkHomonimia(request.getPkTramite());
        homonimia.setUserName(request.getUserName());
        homonimia.setTipoSocietario(request.getTipoSocietario().getId());
        homonimia.setOpcion1(request.getOpcion1());
        homonimia.setOpcion2(request.getOpcion2());
        homonimia.setOpcion3(request.getOpcion3());
        homonimia.setActividadEconomica(request.getActividadEconomica());

        return homonimia;
    }

    private void updateHomonimiaDraft(HomonimiaRequestDto request, Homonimia homonimia) {

        homonimia.setOpcion1(request.getOpcion1());
        homonimia.setOpcion2(request.getOpcion2());
        homonimia.setOpcion3(request.getOpcion3());
        homonimia.setActividadEconomica(request.getActividadEconomica());
        homonimia.setPayCode(request.getPayCode());
        homonimia.setPayMode(request.getPayMode());
        homonimia.setPrecio(request.getPayAmount());
        homonimia.setEstadoTramite(getEstadoTramite(request));
        homonimia.setFechaCreacion(homonimia.getFechaCreacion());
    }

    private Homonimia getNewHomonimiaDraft(HomonimiaRequestDto request) {

        Homonimia homonimia = getNewHomonimiaBasic(request);
        homonimia.setFechaCreacion(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        homonimia.setPayCode(request.getPayCode());
        homonimia.setPayMode(request.getPayMode());
        homonimia.setPrecio(request.getPayAmount());
        homonimia.setEstadoTramite(getEstadoTramite(request));

        return homonimia;
    }

    private String getEstadoTramite(HomonimiaRequestDto request) {
        String estadoTramite;
        if (!request.getPayCode().equals("")){
            if (request.getCodigoTramite() == null){
                estadoTramite = TramiteResponseDto.EstadoTramite.PAGADO.name();
            } else {
                estadoTramite = TramiteResponseDto.EstadoTramite.OBSERVADO.name();
            }
        } else {
            estadoTramite = TramiteResponseDto.EstadoTramite.PENDIENTE_PAGO.name();
        }
        return estadoTramite;
    }

    private void actualizarEstadosAdabas(String userName) throws AdabasCRequestException {
        List<Homonimia> homonimiaList = homonimiaRepository.getListaEnProcesoByUser(userName);
        for (Homonimia homonimia: homonimiaList){
            HomonimiaResponseDto homonimiaResponseDto = (HomonimiaResponseDto)homonimiaAdabasService.getEstadoHomonimia(homonimia.getCodigoTramite());
            long ultimaFechaActualizacion = parseFechaActualizacion(homonimiaResponseDto.getFechaActualizacion());

            if (homonimiaResponseDto != null
                    && homonimiaResponseDto.getFechaActualizacion() != null){

                homonimia.setEstadoTramite(homonimiaResponseDto.getEstadoTramite());
                homonimia.setFechaActualizacion(new Timestamp(ultimaFechaActualizacion));
                homonimiaRepository.update(homonimia);
            }
        }
    }

    private long parseFechaActualizacion(String fechaActualizacion) {
        long result = 0L;
        if (!fechaActualizacion.equals("")) {
            int yy = Integer.parseInt(fechaActualizacion.substring(0, 4));
            int mm = Integer.parseInt(fechaActualizacion.substring(4, 6));
            int dd = Integer.parseInt(fechaActualizacion.substring(6, 8));
            Calendar lastDate = Calendar.getInstance();
            lastDate.set(yy, mm, dd);
            result = lastDate.getTime().getTime();
        }
        return result;
    }
}
