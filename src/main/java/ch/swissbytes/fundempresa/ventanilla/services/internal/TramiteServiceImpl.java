package ch.swissbytes.fundempresa.ventanilla.services.internal;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasc.exception.AdabasDRequestException;
import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteDraftResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.model.Homonimia;
import ch.swissbytes.fundempresa.ventanilla.services.FotocopiasService;
import ch.swissbytes.fundempresa.ventanilla.services.HomonimiaService;
import ch.swissbytes.fundempresa.ventanilla.services.TramiteService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by daniel on 04/04/2016.
 */
public class TramiteServiceImpl implements TramiteService{

    @Inject
    private HomonimiaService homonimiaService;

    @Inject
    private FotocopiasService fotocopiasService;

    @Override
    public List<TramiteDraftResponseDto> getAllTramiteList(String userName) throws AdabasCRequestException {
        List<TramiteDraftResponseDto> response = new ArrayList<TramiteDraftResponseDto>();

        List<TramiteDraftResponseDto> homonimiaList = homonimiaService.getAllTramiteList(userName);
        List<TramiteDraftResponseDto> fotocopiasList = fotocopiasService.getAllTramiteList(userName);

        response.addAll(homonimiaList);
        response.addAll(fotocopiasList);

        response.sort(new Comparator<TramiteDraftResponseDto>() {
            @Override
            public int compare(TramiteDraftResponseDto o1, TramiteDraftResponseDto o2) {
                return o2.getFechaCreacion() != null && o1.getFechaCreacion() != null
                        ? o2.getFechaCreacion().compareTo(o1.getFechaCreacion())
                        : o2.getTramiteResponseDto().getPkTramite().compareTo(o1.getTramiteResponseDto().getPkTramite());
            }
        });
        return response;
    }

    @Override
    public UserProfileDto getUserProfile(String userName) {
        UserProfileDto userProfileDto = AdabasD.profile(userName);
        return userProfileDto;
    }
}
