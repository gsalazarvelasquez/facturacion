package ch.swissbytes.fundempresa.ventanilla.services;

import ch.swissbytes.fundempresa.ventanilla.data.model.VentabdParametros;

import javax.ejb.Local;
import java.util.Optional;

/**
 * Created by miguel on 1/26/16.
 */
@Local
public interface VentaBDParametrosService {

      Optional<VentabdParametros> getConfig();

      Boolean update(VentabdParametros ventaBDParametros);
}
