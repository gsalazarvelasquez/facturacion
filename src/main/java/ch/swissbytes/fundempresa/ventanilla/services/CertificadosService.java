package ch.swissbytes.fundempresa.ventanilla.services;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.ventanilla.data.dto.CertificadosRequestDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.CertificadosResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.model.TipoCertificado;

import java.util.List;

/**
 * Created by daniel on 24/02/2016.
 */
public interface CertificadosService {

    Long saveDraftEspeciales(CertificadosRequestDto certificadosRequestDto);

    Long saveDraftEnlinea(CertificadosRequestDto certificadosRequestDto);

    CertificadosResponseDto especial(CertificadosRequestDto certificadosRequestDto) throws AdabasCRequestException;

    CertificadosResponseDto enlinea(CertificadosRequestDto certificadosRequestDto) throws AdabasCRequestException;

    CertificadosResponseDto validar(CertificadosRequestDto certificadosRequestDto) throws AdabasCRequestException;

    List<TipoCertificado> getTipoCertificadoList(String tipo);

}
