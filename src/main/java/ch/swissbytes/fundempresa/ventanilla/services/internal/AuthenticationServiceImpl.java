package ch.swissbytes.fundempresa.ventanilla.services.internal;

import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.ventanilla.data.repository.PerfilesRepositoryImpl;
import ch.swissbytes.fundempresa.ventanilla.exception.UserNotFoundException;
import ch.swissbytes.fundempresa.ventanilla.services.AuthenticationService;
import ch.swissbytes.shared.security.dto.AccountDto;
import ch.swissbytes.shared.security.dto.UsernamePasswordCredential;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by daniel on 17/05/2016.
 */
public class AuthenticationServiceImpl implements AuthenticationService{

    @Inject
    private PerfilesRepositoryImpl perfilesRepository;

    @Override
    public AccountDto authenticate(UsernamePasswordCredential credential) throws UserNotFoundException {
        UserProfileDto userProfileDto = AdabasD.autenticar(credential.getUserId(), credential.getPassword());

        if (userProfileDto == null){
            throw new UserNotFoundException();
        }

        String perfilAdabas = userProfileDto.getProfile();
        AccountDto accountDto = new AccountDto();
        accountDto.setUserName(credential.getUserId());
        accountDto.setRole(local(perfilAdabas));
        accountDto.setToken("1234");

        return accountDto;
    }

    private String local(String perfilAdabas) {
        String perfiles = "";
        List<Integer> list = perfilesRepository.local(perfilAdabas);
        for (Integer local : list) {
            perfiles += ("-" + local);
        }
        perfiles += "-";
        return perfiles;
    }
}
