package ch.swissbytes.fundempresa.impuestos.data;

public class Base64 {

    public String encode(long value) {
        long ratio = 1;
        long residue = 0;
        String result = "";
        while (ratio > 0) {
            ratio = value / 64;
            residue = value % 64;
            Dictionary dic = new Dictionary();
            result = dic.getValue((int) residue) + result;
            value = ratio;
        }
        return result;
    }

    class Dictionary {

        public String values[];

        public Dictionary() {
            values = new String[]{ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
                    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "+", "/" };
        }

        public String getValue(int position) {
            return values[position];
        }
    }
}

