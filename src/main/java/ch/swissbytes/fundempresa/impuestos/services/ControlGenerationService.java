package ch.swissbytes.fundempresa.impuestos.services;

import ch.swissbytes.fundempresa.impuestos.data.dto.ControlEntityDto;

import java.text.ParseException;

/**
 * Created by jose on 22/04/2016.
 */
public interface ControlGenerationService {

    public String generate(ControlEntityDto control) throws ParseException;

}
