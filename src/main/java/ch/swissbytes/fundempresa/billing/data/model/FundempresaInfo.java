package ch.swissbytes.fundempresa.billing.data.model;

import javax.persistence.*;

/**
 * Created by jose on 25/04/2016.
 */
@Entity
@Table(name = "fundempresa_info", schema = "", catalog = "fundempresa_local")
public class FundempresaInfo {
    private Integer id;
    private String emisorNombre;
    private String emisorNit;
    private String emisorActividadComercial;
    private String emisorDireccion;
    private String emisorTelefonos;
    private String emisorCiudad;

    @Id
    @Column(name= "id", nullable= false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "emisor_nombre", nullable = true, insertable = true, updatable = true, length = 200)
    public String getEmisorNombre() {
        return emisorNombre;
    }

    public void setEmisorNombre(String emisorNombre) {
        this.emisorNombre = emisorNombre;
    }

    @Basic
    @Column(name = "emisor_nit", nullable = true, insertable = true, updatable = true, length = 50)
    public String getEmisorNit() {
        return emisorNit;
    }

    public void setEmisorNit(String emisorNit) {
        this.emisorNit = emisorNit;
    }

    @Basic
    @Column(name = "emisor_actividad_comercial", nullable = true, insertable = true, updatable = true, length = 200)
    public String getEmisorActividadComercial() {
        return emisorActividadComercial;
    }

    public void setEmisorActividadComercial(String emisorActividadComercial) {
        this.emisorActividadComercial = emisorActividadComercial;
    }

    @Basic
    @Column(name = "emisor_direccion", nullable = true, insertable = true, updatable = true, length = 200)
    public String getEmisorDireccion() {
        return emisorDireccion;
    }

    public void setEmisorDireccion(String emisorDireccion) {
        this.emisorDireccion = emisorDireccion;
    }

    @Basic
    @Column(name = "emisor_telefonos", nullable = true, insertable = true, updatable = true, length = 200)
    public String getEmisorTelefonos() {
        return emisorTelefonos;
    }

    public void setEmisorTelefonos(String emisorTelefonos) {
        this.emisorTelefonos = emisorTelefonos;
    }

    @Basic
    @Column(name = "emisor_ciudad", nullable = true, insertable = true, updatable = true, length = 200)
    public String getEmisorCiudad() {
        return emisorCiudad;
    }

    public void setEmisorCiudad(String emisorCiudad) {
        this.emisorCiudad = emisorCiudad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FundempresaInfo that = (FundempresaInfo) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (emisorNombre != null ? !emisorNombre.equals(that.emisorNombre) : that.emisorNombre != null) return false;
        if (emisorNit != null ? !emisorNit.equals(that.emisorNit) : that.emisorNit != null) return false;
        if (emisorActividadComercial != null ? !emisorActividadComercial.equals(that.emisorActividadComercial) : that.emisorActividadComercial != null)
            return false;
        if (emisorDireccion != null ? !emisorDireccion.equals(that.emisorDireccion) : that.emisorDireccion != null)
            return false;
        if (emisorTelefonos != null ? !emisorTelefonos.equals(that.emisorTelefonos) : that.emisorTelefonos != null)
            return false;
        if (emisorCiudad != null ? !emisorCiudad.equals(that.emisorCiudad) : that.emisorCiudad != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (emisorNombre != null ? emisorNombre.hashCode() : 0);
        result = 31 * result + (emisorNit != null ? emisorNit.hashCode() : 0);
        result = 31 * result + (emisorActividadComercial != null ? emisorActividadComercial.hashCode() : 0);
        result = 31 * result + (emisorDireccion != null ? emisorDireccion.hashCode() : 0);
        result = 31 * result + (emisorTelefonos != null ? emisorTelefonos.hashCode() : 0);
        result = 31 * result + (emisorCiudad != null ? emisorCiudad.hashCode() : 0);
        return result;
    }
}
