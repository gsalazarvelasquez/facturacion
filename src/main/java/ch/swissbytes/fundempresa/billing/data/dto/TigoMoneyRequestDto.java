package ch.swissbytes.fundempresa.billing.data.dto;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

/**
 * Created by jose on 22/03/2016.
 */
@Getter
@Setter
public class TigoMoneyRequestDto extends PaymentRequestDto implements Serializable {

    private String solicitante;
    private String mensaje;
    private String pregunta;
    private long orderId;
    private int numeroTigoMoney;
    private String ci;
}
