package ch.swissbytes.fundempresa.billing.data.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by jose on 12/04/2016.
 */
@Entity
@Table(name = "tigo_money_transacciones", schema = "", catalog = "fundempresa_local")
public class TigoMoneyTransacciones {
    private Long id;
    private String requestNombre;
    private String requestNumerodocumento;
    private String requestRazonsocial;
    private String requestNit;
    private String requestNumerolinea;
    private String requestMonto;
    private Timestamp requestFechahora;
    private Long responseCodigoerror;
    private String responseMensaje;
    private String responseOrderid;
    private String responseDescripcion;
    private String requestUsername;

    @Id
    @Column(name= "id", nullable= false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "request_nombre", nullable = true, insertable = true, updatable = true, length = 2048)
    public String getRequestNombre() {
        return requestNombre;
    }

    public void setRequestNombre(String requestNombre) {
        this.requestNombre = requestNombre;
    }

    @Basic
    @Column(name = "request_numerodocumento", nullable = true, insertable = true, updatable = true, length = 50)
    public String getRequestNumerodocumento() {
        return requestNumerodocumento;
    }

    public void setRequestNumerodocumento(String requestNumerodocumento) {
        this.requestNumerodocumento = requestNumerodocumento;
    }

    @Basic
    @Column(name = "request_razonsocial", nullable = true, insertable = true, updatable = true, length = 2048)
    public String getRequestRazonsocial() {
        return requestRazonsocial;
    }

    public void setRequestRazonsocial(String requestRazonsocial) {
        this.requestRazonsocial = requestRazonsocial;
    }

    @Basic
    @Column(name = "request_nit", nullable = true, insertable = true, updatable = true, length = 50)
    public String getRequestNit() {
        return requestNit;
    }

    public void setRequestNit(String requestNit) {
        this.requestNit = requestNit;
    }

    @Basic
    @Column(name = "request_numerolinea", nullable = true, insertable = true, updatable = true, length = 50)
    public String getRequestNumerolinea() {
        return requestNumerolinea;
    }

    public void setRequestNumerolinea(String requestNumerolinea) {
        this.requestNumerolinea = requestNumerolinea;
    }

    @Basic
    @Column(name = "request_monto", nullable = true, insertable = true, updatable = true, length = 50)
    public String getRequestMonto() {
        return requestMonto;
    }

    public void setRequestMonto(String requestMonto) {
        this.requestMonto = requestMonto;
    }

    @Basic
    @Column(name = "request_fechahora", nullable = true, insertable = true, updatable = true)
    public Timestamp getRequestFechahora() {
        return requestFechahora;
    }

    public void setRequestFechahora(Timestamp requestFechahora) {
        this.requestFechahora = requestFechahora;
    }

    @Basic
    @Column(name = "response_codigoerror", nullable = true, insertable = true, updatable = true)
    public Long getResponseCodigoerror() {
        return responseCodigoerror;
    }

    public void setResponseCodigoerror(Long responseCodigoerror) {
        this.responseCodigoerror = responseCodigoerror;
    }

    @Basic
    @Column(name = "response_mensaje", nullable = true, insertable = true, updatable = true, length = 2048)
    public String getResponseMensaje() {
        return responseMensaje;
    }

    public void setResponseMensaje(String responseMensaje) {
        this.responseMensaje = responseMensaje;
    }

    @Basic
    @Column(name = "response_orderid", nullable = true, insertable = true, updatable = true, length = 2048)
    public String getResponseOrderid() {
        return responseOrderid;
    }

    public void setResponseOrderid(String responseOrderid) {
        this.responseOrderid = responseOrderid;
    }

    @Basic
    @Column(name = "response_descripcion", nullable = true, insertable = true, updatable = true, length = 2048)
    public String getResponseDescripcion() {
        return responseDescripcion;
    }

    public void setResponseDescripcion(String responseDescripcion) {
        this.responseDescripcion = responseDescripcion;
    }

    @Basic
    @Column(name = "request_username", nullable = true, insertable = true, updatable = true, length = 2048)
    public String getRequestUsername() {
        return requestUsername;
    }

    public void setRequestUsername(String requestUsername) {
        this.requestUsername = requestUsername;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TigoMoneyTransacciones that = (TigoMoneyTransacciones) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (requestNombre != null ? !requestNombre.equals(that.requestNombre) : that.requestNombre != null)
            return false;
        if (requestNumerodocumento != null ? !requestNumerodocumento.equals(that.requestNumerodocumento) : that.requestNumerodocumento != null)
            return false;
        if (requestRazonsocial != null ? !requestRazonsocial.equals(that.requestRazonsocial) : that.requestRazonsocial != null)
            return false;
        if (requestNit != null ? !requestNit.equals(that.requestNit) : that.requestNit != null) return false;
        if (requestNumerolinea != null ? !requestNumerolinea.equals(that.requestNumerolinea) : that.requestNumerolinea != null)
            return false;
        if (requestMonto != null ? !requestMonto.equals(that.requestMonto) : that.requestMonto != null) return false;
        if (requestFechahora != null ? !requestFechahora.equals(that.requestFechahora) : that.requestFechahora != null)
            return false;
        if (responseCodigoerror != null ? !responseCodigoerror.equals(that.responseCodigoerror) : that.responseCodigoerror != null)
            return false;
        if (responseMensaje != null ? !responseMensaje.equals(that.responseMensaje) : that.responseMensaje != null)
            return false;
        if (responseOrderid != null ? !responseOrderid.equals(that.responseOrderid) : that.responseOrderid != null)
            return false;
        if (responseDescripcion != null ? !responseDescripcion.equals(that.responseDescripcion) : that.responseDescripcion != null)
            return false;
        if (requestUsername != null ? !requestUsername.equals(that.requestUsername) : that.requestUsername != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (requestNombre != null ? requestNombre.hashCode() : 0);
        result = 31 * result + (requestNumerodocumento != null ? requestNumerodocumento.hashCode() : 0);
        result = 31 * result + (requestRazonsocial != null ? requestRazonsocial.hashCode() : 0);
        result = 31 * result + (requestNit != null ? requestNit.hashCode() : 0);
        result = 31 * result + (requestNumerolinea != null ? requestNumerolinea.hashCode() : 0);
        result = 31 * result + (requestMonto != null ? requestMonto.hashCode() : 0);
        result = 31 * result + (requestFechahora != null ? requestFechahora.hashCode() : 0);
        result = 31 * result + (responseCodigoerror != null ? responseCodigoerror.hashCode() : 0);
        result = 31 * result + (responseMensaje != null ? responseMensaje.hashCode() : 0);
        result = 31 * result + (responseOrderid != null ? responseOrderid.hashCode() : 0);
        result = 31 * result + (responseDescripcion != null ? responseDescripcion.hashCode() : 0);
        result = 31 * result + (requestUsername != null ? requestUsername.hashCode() : 0);
        return result;
    }
}
