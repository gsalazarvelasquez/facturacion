package ch.swissbytes.fundempresa.billing.data.repository;

import ch.swissbytes.fundempresa.billing.data.model.PaypalTransacciones;
import ch.swissbytes.fundempresa.billing.data.model.TwocheckoutConfig;
import ch.swissbytes.fundempresa.billing.data.model.TwocheckoutTransacciones;
import ch.swissbytes.shared.persistence.Repository;

import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by daniel on 11/02/2016.
 */
@Stateless
public class TwocheckoutRepositoryImpl extends Repository {

    public void save(TwocheckoutTransacciones entity) throws Exception {
        saveAndFlush(entity);
    }

    public TwocheckoutConfig getConfig() throws Exception{
        Query query = em.createQuery("SELECT config FROM TwocheckoutConfig config ORDER BY config.id DESC");
        List l = query.getResultList();
        if(l == null || l.isEmpty()){
            return null;
        }
        return (TwocheckoutConfig) l.get(0);
    }

    @Override
    protected Class getPersistentClass() {
        return this.getClass();
    }
}
