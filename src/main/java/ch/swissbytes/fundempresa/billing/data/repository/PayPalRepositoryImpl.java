package ch.swissbytes.fundempresa.billing.data.repository;

import ch.swissbytes.fundempresa.billing.data.model.PaypalConfig;
import ch.swissbytes.fundempresa.billing.data.model.PaypalTransacciones;
import ch.swissbytes.fundempresa.billing.data.model.TwocheckoutConfig;
import ch.swissbytes.shared.persistence.Repository;
import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by daniel on 11/02/2016.
 */
@Stateless
public class PayPalRepositoryImpl extends Repository {

    public PaypalConfig getConfig() throws Exception{
        Query query = em.createQuery("SELECT config FROM PaypalConfig config ORDER BY config.id DESC");
        List l = query.getResultList();
        if(l == null || l.isEmpty()){
            return null;
        }
        return (PaypalConfig) l.get(0);
    }

    @Override
    protected Class getPersistentClass() {
        return this.getClass();
    }
}
