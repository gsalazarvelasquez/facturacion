package ch.swissbytes.fundempresa.billing.data.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by daniel on 19/05/2016.
 */
@Entity
@Table(name = "paypal_transacciones", schema = "fundempresa_local", catalog = "")
public class PaypalTransacciones {
    private Long id;
    private String requestNombre;
    private String requestNumerodocumento;
    private String requestRazonsocial;
    private String requestNit;
    private String requestMonto;
    private Timestamp requestFechahora;
    private String requestUsername;
    private String requestOrderid;
    private String requestTipotramite;
    private String requestPktramite;
    private String responsePaymentStatus;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "request_nombre", nullable = true, length = 2048)
    public String getRequestNombre() {
        return requestNombre;
    }

    public void setRequestNombre(String requestNombre) {
        this.requestNombre = requestNombre;
    }

    @Basic
    @Column(name = "request_numerodocumento", nullable = true, length = 50)
    public String getRequestNumerodocumento() {
        return requestNumerodocumento;
    }

    public void setRequestNumerodocumento(String requestNumerodocumento) {
        this.requestNumerodocumento = requestNumerodocumento;
    }

    @Basic
    @Column(name = "request_razonsocial", nullable = true, length = 2048)
    public String getRequestRazonsocial() {
        return requestRazonsocial;
    }

    public void setRequestRazonsocial(String requestRazonsocial) {
        this.requestRazonsocial = requestRazonsocial;
    }

    @Basic
    @Column(name = "request_nit", nullable = true, length = 50)
    public String getRequestNit() {
        return requestNit;
    }

    public void setRequestNit(String requestNit) {
        this.requestNit = requestNit;
    }

    @Basic
    @Column(name = "request_monto", nullable = true, length = 50)
    public String getRequestMonto() {
        return requestMonto;
    }

    public void setRequestMonto(String requestMonto) {
        this.requestMonto = requestMonto;
    }

    @Basic
    @Column(name = "request_fechahora", nullable = true)
    public Timestamp getRequestFechahora() {
        return requestFechahora;
    }

    public void setRequestFechahora(Timestamp requestFechahora) {
        this.requestFechahora = requestFechahora;
    }

    @Basic
    @Column(name = "request_username", nullable = true, length = 2048)
    public String getRequestUsername() {
        return requestUsername;
    }

    public void setRequestUsername(String requestUsername) {
        this.requestUsername = requestUsername;
    }

    @Basic
    @Column(name = "request_orderid", nullable = true, length = 100)
    public String getRequestOrderid() {
        return requestOrderid;
    }

    public void setRequestOrderid(String requestOrderid) {
        this.requestOrderid = requestOrderid;
    }

    @Basic
    @Column(name = "request_tipotramite", nullable = true, length = 50)
    public String getRequestTipotramite() {
        return requestTipotramite;
    }

    public void setRequestTipotramite(String requestTipotramite) {
        this.requestTipotramite = requestTipotramite;
    }

    @Basic
    @Column(name = "request_pktramite", nullable = true, length = 10)
    public String getRequestPktramite() {
        return requestPktramite;
    }

    public void setRequestPktramite(String requestPktramite) {
        this.requestPktramite = requestPktramite;
    }

    @Basic
    @Column(name = "response_payment_status", nullable = true, length = 2048)
    public String getResponsePaymentStatus() {
        return responsePaymentStatus;
    }

    public void setResponsePaymentStatus(String responsePaymentStatus) {
        this.responsePaymentStatus = responsePaymentStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaypalTransacciones that = (PaypalTransacciones) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (requestNombre != null ? !requestNombre.equals(that.requestNombre) : that.requestNombre != null)
            return false;
        if (requestNumerodocumento != null ? !requestNumerodocumento.equals(that.requestNumerodocumento) : that.requestNumerodocumento != null)
            return false;
        if (requestRazonsocial != null ? !requestRazonsocial.equals(that.requestRazonsocial) : that.requestRazonsocial != null)
            return false;
        if (requestNit != null ? !requestNit.equals(that.requestNit) : that.requestNit != null) return false;
        if (requestMonto != null ? !requestMonto.equals(that.requestMonto) : that.requestMonto != null) return false;
        if (requestFechahora != null ? !requestFechahora.equals(that.requestFechahora) : that.requestFechahora != null)
            return false;
        if (requestUsername != null ? !requestUsername.equals(that.requestUsername) : that.requestUsername != null)
            return false;
        if (requestOrderid != null ? !requestOrderid.equals(that.requestOrderid) : that.requestOrderid != null)
            return false;
        if (requestTipotramite != null ? !requestTipotramite.equals(that.requestTipotramite) : that.requestTipotramite != null)
            return false;
        if (requestPktramite != null ? !requestPktramite.equals(that.requestPktramite) : that.requestPktramite != null)
            return false;
        if (responsePaymentStatus != null ? !responsePaymentStatus.equals(that.responsePaymentStatus) : that.responsePaymentStatus != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (requestNombre != null ? requestNombre.hashCode() : 0);
        result = 31 * result + (requestNumerodocumento != null ? requestNumerodocumento.hashCode() : 0);
        result = 31 * result + (requestRazonsocial != null ? requestRazonsocial.hashCode() : 0);
        result = 31 * result + (requestNit != null ? requestNit.hashCode() : 0);
        result = 31 * result + (requestMonto != null ? requestMonto.hashCode() : 0);
        result = 31 * result + (requestFechahora != null ? requestFechahora.hashCode() : 0);
        result = 31 * result + (requestUsername != null ? requestUsername.hashCode() : 0);
        result = 31 * result + (requestOrderid != null ? requestOrderid.hashCode() : 0);
        result = 31 * result + (requestTipotramite != null ? requestTipotramite.hashCode() : 0);
        result = 31 * result + (requestPktramite != null ? requestPktramite.hashCode() : 0);
        result = 31 * result + (responsePaymentStatus != null ? responsePaymentStatus.hashCode() : 0);
        return result;
    }
}
