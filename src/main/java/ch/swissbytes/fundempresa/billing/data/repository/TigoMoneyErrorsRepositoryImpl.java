package ch.swissbytes.fundempresa.billing.data.repository;

import ch.swissbytes.shared.persistence.Repository;
import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by daniel on 11/02/2016.
 */
@Stateless
public class TigoMoneyErrorsRepositoryImpl extends Repository {

    @Override
    protected Class getPersistentClass() {
        return this.getClass();
    }

    public String getbyCode(int code) {
        String result = "";
        try {
            Query query = em.createQuery("SELECT description FROM TigoMoneyErrorCodes tigoErrors WHERE tigoErrors.code = :codError");
            query.setParameter("codError", code);
            List list = query.getResultList();
            if(list != null && !list.isEmpty())
                result = (String) list.get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
