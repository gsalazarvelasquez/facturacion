package ch.swissbytes.fundempresa.billing.data.repository;

import ch.swissbytes.fundempresa.billing.data.model.TigoMoneyConfig;
import ch.swissbytes.shared.persistence.Repository;
import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by daniel on 11/02/2016.
 */
@Stateless
public class TigoMoneyConfigRepositoryImpl extends Repository {

    @Override
    protected Class getPersistentClass() {
        return this.getClass();
    }

    public TigoMoneyConfig getConfig() {
        TigoMoneyConfig result = null;
        try {
            String sql = "SELECT configuration FROM TigoMoneyConfig configuration ORDER BY configuration.id DESC";
            Query query = em.createQuery(sql);
            List list = query.getResultList();
            if (list != null && !list.isEmpty())
                result = (TigoMoneyConfig) list.get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
