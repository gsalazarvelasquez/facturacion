package ch.swissbytes.fundempresa.billing.data.dto;

import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by jose on 22/03/2016.
 */
@Getter
@Setter
public class PayPalRequestDto extends PaymentRequestDto implements Serializable {

    private String solicitante;
    private String ci;
    private String paymentStatus;

    private HomonimiaRequestDto homonimiaRequestDto;
    private FotocopiasRequestDto fotocopiasRequestDto;
    private CertificadosRequestDto certificadosRequestDto;

}
