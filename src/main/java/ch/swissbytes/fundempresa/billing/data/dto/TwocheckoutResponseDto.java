package ch.swissbytes.fundempresa.billing.data.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by daniel on 31/03/2016.
 */
@Getter
@Setter
public class TwocheckoutResponseDto extends PaymentResponseDto {

    String payCode;
    long orderId;
    long invoiceId;

}
