package ch.swissbytes.fundempresa.billing.data.model;

import javax.persistence.*;

/**
 * Created by jose on 17/05/2016.
 */
@Entity
@Table(name = "twocheckout_config", schema = "", catalog = "fundempresa_local")
public class TwocheckoutConfig {
    private Integer id;
    private String sid;
    private String tipoCambio;
    private String motivo;
    private String secretWord;
    private String urlReturn;
    private String urlError;
    private String demo;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "sid", nullable = true, insertable = true, updatable = true, length = 150)
    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    @Basic
    @Column(name = "tipo_cambio", nullable = true, insertable = true, updatable = true, length = 150)
    public String getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(String tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    @Basic
    @Column(name = "motivo", nullable = true, insertable = true, updatable = true, length = 150)
    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    @Basic
    @Column(name = "secret_word", nullable = true, insertable = true, updatable = true, length = 250)
    public String getSecretWord() {
        return secretWord;
    }

    public void setSecretWord(String secretWord) {
        this.secretWord = secretWord;
    }

    @Basic
    @Column(name = "url_return", nullable = true, insertable = true, updatable = true, length = 250)
    public String getUrlReturn() {
        return urlReturn;
    }

    public void setUrlReturn(String urlReturn) {
        this.urlReturn = urlReturn;
    }

    @Basic
    @Column(name = "url_error", nullable = true, insertable = true, updatable = true, length = 250)
    public String getUrlError() {
        return urlError;
    }

    public void setUrlError(String urlError) {
        this.urlError = urlError;
    }

    @Basic
    @Column(name = "demo", nullable = true, insertable = true, updatable = true, length = 5)
    public String getDemo() {
        return demo;
    }

    public void setDemo(String demo) {
        this.demo = demo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TwocheckoutConfig that = (TwocheckoutConfig) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (sid != null ? !sid.equals(that.sid) : that.sid != null) return false;
        if (tipoCambio != null ? !tipoCambio.equals(that.tipoCambio) : that.tipoCambio != null) return false;
        if (motivo != null ? !motivo.equals(that.motivo) : that.motivo != null) return false;
        if (secretWord != null ? !secretWord.equals(that.secretWord) : that.secretWord != null) return false;
        if (urlReturn != null ? !urlReturn.equals(that.urlReturn) : that.urlReturn != null) return false;
        if (urlError != null ? !urlError.equals(that.urlError) : that.urlError != null) return false;
        if (demo != null ? !demo.equals(that.demo) : that.demo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (sid != null ? sid.hashCode() : 0);
        result = 31 * result + (tipoCambio != null ? tipoCambio.hashCode() : 0);
        result = 31 * result + (motivo != null ? motivo.hashCode() : 0);
        result = 31 * result + (secretWord != null ? secretWord.hashCode() : 0);
        result = 31 * result + (urlReturn != null ? urlReturn.hashCode() : 0);
        result = 31 * result + (urlError != null ? urlError.hashCode() : 0);
        result = 31 * result + (demo != null ? demo.hashCode() : 0);
        return result;
    }
}
