package ch.swissbytes.fundempresa.billing.data.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by daniel on 31/03/2016.
 */
@Getter
@Setter
public class DebitoResponseDto extends PaymentResponseDto {

    String payCode;

}
