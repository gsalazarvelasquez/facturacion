package ch.swissbytes.fundempresa.billing.data.model;

import javax.persistence.*;

/**
 * Created by jose on 12/04/2016.
 */
@Entity
@Table(name = "tigo_money_config", schema = "", catalog = "fundempresa_local")
public class TigoMoneyConfig {
    private Integer id;
    private String testUrl;
    private String testEncryptionKey;
    private String testIdKey;
    private String testUrlSuccess;
    private String testUrlError;
    private Integer testTimeout;
    private String prodUrl;
    private String prodEncryptionKey;
    private String prodIdKey;
    private String prodUrlSuccess;
    private String prodUrlError;
    private Integer prodTimeout;

    @Id
    @Column(name= "id", nullable= false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "test_url", nullable = false, insertable = true, updatable = true, length = 2048)
    public String getTestUrl() {
        return testUrl;
    }

    public void setTestUrl(String testUrl) {
        this.testUrl = testUrl;
    }

    @Basic
    @Column(name = "test_encryption_key", nullable = false, insertable = true, updatable = true, length = 2048)
    public String getTestEncryptionKey() {
        return testEncryptionKey;
    }

    public void setTestEncryptionKey(String testEncryptionKey) {
        this.testEncryptionKey = testEncryptionKey;
    }

    @Basic
    @Column(name = "test_id_key", nullable = false, insertable = true, updatable = true, length = 2048)
    public String getTestIdKey() {
        return testIdKey;
    }

    public void setTestIdKey(String testIdKey) {
        this.testIdKey = testIdKey;
    }

    @Basic
    @Column(name = "test_url_success", nullable = false, insertable = true, updatable = true, length = 2048)
    public String getTestUrlSuccess() {
        return testUrlSuccess;
    }

    public void setTestUrlSuccess(String testUrlSuccess) {
        this.testUrlSuccess = testUrlSuccess;
    }

    @Basic
    @Column(name = "test_url_error", nullable = false, insertable = true, updatable = true, length = 2048)
    public String getTestUrlError() {
        return testUrlError;
    }

    public void setTestUrlError(String testUrlError) {
        this.testUrlError = testUrlError;
    }

    @Basic
    @Column(name = "test_timeout", nullable = true, insertable = true, updatable = true)
    public Integer getTestTimeout() {
        return testTimeout;
    }

    public void setTestTimeout(Integer testTimeout) {
        this.testTimeout = testTimeout;
    }

    @Basic
    @Column(name = "prod_url", nullable = false, insertable = true, updatable = true, length = 2048)
    public String getProdUrl() {
        return prodUrl;
    }

    public void setProdUrl(String prodUrl) {
        this.prodUrl = prodUrl;
    }

    @Basic
    @Column(name = "prod_encryption_key", nullable = false, insertable = true, updatable = true, length = 2048)
    public String getProdEncryptionKey() {
        return prodEncryptionKey;
    }

    public void setProdEncryptionKey(String prodEncryptionKey) {
        this.prodEncryptionKey = prodEncryptionKey;
    }

    @Basic
    @Column(name = "prod_id_key", nullable = false, insertable = true, updatable = true, length = 2048)
    public String getProdIdKey() {
        return prodIdKey;
    }

    public void setProdIdKey(String prodIdKey) {
        this.prodIdKey = prodIdKey;
    }

    @Basic
    @Column(name = "prod_url_success", nullable = false, insertable = true, updatable = true, length = 2048)
    public String getProdUrlSuccess() {
        return prodUrlSuccess;
    }

    public void setProdUrlSuccess(String prodUrlSuccess) {
        this.prodUrlSuccess = prodUrlSuccess;
    }

    @Basic
    @Column(name = "prod_url_error", nullable = false, insertable = true, updatable = true, length = 2048)
    public String getProdUrlError() {
        return prodUrlError;
    }

    public void setProdUrlError(String prodUrlError) {
        this.prodUrlError = prodUrlError;
    }

    @Basic
    @Column(name = "prod_timeout", nullable = true, insertable = true, updatable = true)
    public Integer getProdTimeout() {
        return prodTimeout;
    }

    public void setProdTimeout(Integer prodTimeout) {
        this.prodTimeout = prodTimeout;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TigoMoneyConfig that = (TigoMoneyConfig) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (testUrl != null ? !testUrl.equals(that.testUrl) : that.testUrl != null) return false;
        if (testEncryptionKey != null ? !testEncryptionKey.equals(that.testEncryptionKey) : that.testEncryptionKey != null)
            return false;
        if (testIdKey != null ? !testIdKey.equals(that.testIdKey) : that.testIdKey != null) return false;
        if (testUrlSuccess != null ? !testUrlSuccess.equals(that.testUrlSuccess) : that.testUrlSuccess != null)
            return false;
        if (testUrlError != null ? !testUrlError.equals(that.testUrlError) : that.testUrlError != null) return false;
        if (testTimeout != null ? !testTimeout.equals(that.testTimeout) : that.testTimeout != null) return false;
        if (prodUrl != null ? !prodUrl.equals(that.prodUrl) : that.prodUrl != null) return false;
        if (prodEncryptionKey != null ? !prodEncryptionKey.equals(that.prodEncryptionKey) : that.prodEncryptionKey != null)
            return false;
        if (prodIdKey != null ? !prodIdKey.equals(that.prodIdKey) : that.prodIdKey != null) return false;
        if (prodUrlSuccess != null ? !prodUrlSuccess.equals(that.prodUrlSuccess) : that.prodUrlSuccess != null)
            return false;
        if (prodUrlError != null ? !prodUrlError.equals(that.prodUrlError) : that.prodUrlError != null) return false;
        if (prodTimeout != null ? !prodTimeout.equals(that.prodTimeout) : that.prodTimeout != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (testUrl != null ? testUrl.hashCode() : 0);
        result = 31 * result + (testEncryptionKey != null ? testEncryptionKey.hashCode() : 0);
        result = 31 * result + (testIdKey != null ? testIdKey.hashCode() : 0);
        result = 31 * result + (testUrlSuccess != null ? testUrlSuccess.hashCode() : 0);
        result = 31 * result + (testUrlError != null ? testUrlError.hashCode() : 0);
        result = 31 * result + (testTimeout != null ? testTimeout.hashCode() : 0);
        result = 31 * result + (prodUrl != null ? prodUrl.hashCode() : 0);
        result = 31 * result + (prodEncryptionKey != null ? prodEncryptionKey.hashCode() : 0);
        result = 31 * result + (prodIdKey != null ? prodIdKey.hashCode() : 0);
        result = 31 * result + (prodUrlSuccess != null ? prodUrlSuccess.hashCode() : 0);
        result = 31 * result + (prodUrlError != null ? prodUrlError.hashCode() : 0);
        result = 31 * result + (prodTimeout != null ? prodTimeout.hashCode() : 0);
        return result;
    }
}
