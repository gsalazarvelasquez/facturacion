package ch.swissbytes.fundempresa.billing.data.model;

import javax.persistence.*;

/**
 * Created by jose on 12/04/2016.
 */
@Entity
@Table(name = "tigo_money_error_codes", schema = "", catalog = "fundempresa_local")
public class TigoMoneyErrorCodes {
    private Integer code;
    private String description;

    @Id
    @Column(name = "code", nullable = false, insertable = true, updatable = true)
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Basic
    @Column(name = "description", nullable = false, insertable = true, updatable = true, length = 2048)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TigoMoneyErrorCodes that = (TigoMoneyErrorCodes) o;

        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
