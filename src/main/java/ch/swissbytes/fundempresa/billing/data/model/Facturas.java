package ch.swissbytes.fundempresa.billing.data.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by jose on 25/04/2016.
 */
@Entity
public class Facturas {

    public Facturas() {
    }

    private Long id;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @javax.persistence.Column(name = "id", nullable = false, insertable = true, updatable = true)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private String emisorNombre;

    @Basic
    @javax.persistence.Column(name = "emisor_nombre", nullable = true, insertable = true, updatable = true, length = 200)
    public String getEmisorNombre() {
        return emisorNombre;
    }

    public void setEmisorNombre(String emisorNombre) {
        this.emisorNombre = emisorNombre;
    }

    private String emisorNit;

    @Basic
    @javax.persistence.Column(name = "emisor_nit", nullable = true, insertable = true, updatable = true, length = 50)
    public String getEmisorNit() {
        return emisorNit;
    }

    public void setEmisorNit(String emisorNit) {
        this.emisorNit = emisorNit;
    }

    private String emisorActividadComercial;

    @Basic
    @javax.persistence.Column(name = "emisor_actividad_comercial", nullable = true, insertable = true, updatable = true, length = 200)
    public String getEmisorActividadComercial() {
        return emisorActividadComercial;
    }

    public void setEmisorActividadComercial(String emisorActividadComercial) {
        this.emisorActividadComercial = emisorActividadComercial;
    }

    private String emisorDireccion;

    @Basic
    @javax.persistence.Column(name = "emisor_direccion", nullable = true, insertable = true, updatable = true, length = 200)
    public String getEmisorDireccion() {
        return emisorDireccion;
    }

    public void setEmisorDireccion(String emisorDireccion) {
        this.emisorDireccion = emisorDireccion;
    }

    private String emisorTelefonos;

    @Basic
    @javax.persistence.Column(name = "emisor_telefonos", nullable = true, insertable = true, updatable = true, length = 200)
    public String getEmisorTelefonos() {
        return emisorTelefonos;
    }

    public void setEmisorTelefonos(String emisorTelefonos) {
        this.emisorTelefonos = emisorTelefonos;
    }

    private String emisorCiudad;

    @Basic
    @javax.persistence.Column(name = "emisor_ciudad", nullable = true, insertable = true, updatable = true, length = 200)
    public String getEmisorCiudad() {
        return emisorCiudad;
    }

    public void setEmisorCiudad(String emisorCiudad) {
        this.emisorCiudad = emisorCiudad;
    }

    private Long numeroFactura;

    @Basic
    @javax.persistence.Column(name = "numero_factura", nullable = false, insertable = true, updatable = true)
    public Long getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(Long numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    private String numeroAutorizacion;

    @Basic
    @javax.persistence.Column(name = "numero_autorizacion", nullable = false, insertable = true, updatable = true, length = 200)
    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    private Timestamp fecha;

    @Basic
    @javax.persistence.Column(name = "fecha", nullable = true, insertable = true, updatable = true)
    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    private Timestamp fechaLimiteEmision;

    @Basic
    @javax.persistence.Column(name = "fecha_limite_emision", nullable = true, insertable = true, updatable = true)
    public Timestamp getFechaLimiteEmision() {
        return fechaLimiteEmision;
    }

    public void setFechaLimiteEmision(Timestamp fechaLimiteEmision) {
        this.fechaLimiteEmision = fechaLimiteEmision;
    }

    private String montoFactura;

    @Basic
    @javax.persistence.Column(name = "monto_factura", nullable = true, insertable = true, updatable = true, length = 50)
    public String getMontoFactura() {
        return montoFactura;
    }

    public void setMontoFactura(String montoFactura) {
        this.montoFactura = montoFactura;
    }

    private String montoCreditoFiscal;

    @Basic
    @javax.persistence.Column(name = "monto_credito_fiscal", nullable = true, insertable = true, updatable = true, length = 50)
    public String getMontoCreditoFiscal() {
        return montoCreditoFiscal;
    }

    public void setMontoCreditoFiscal(String montoCreditoFiscal) {
        this.montoCreditoFiscal = montoCreditoFiscal;
    }

    private String codigoControl;

    @Basic
    @javax.persistence.Column(name = "codigo_control", nullable = true, insertable = true, updatable = true, length = 50)
    public String getCodigoControl() {
        return codigoControl;
    }

    public void setCodigoControl(String codigoControl) {
        this.codigoControl = codigoControl;
    }

    private String clienteRazonsocial;

    @Basic
    @javax.persistence.Column(name = "cliente_razonsocial", nullable = true, insertable = true, updatable = true, length = 500)
    public String getClienteRazonsocial() {
        return clienteRazonsocial;
    }

    public void setClienteRazonsocial(String clienteRazonsocial) {
        this.clienteRazonsocial = clienteRazonsocial;
    }

    private String clienteNit;

    @Basic
    @javax.persistence.Column(name = "cliente_nit", nullable = true, insertable = true, updatable = true, length = 200)
    public String getClienteNit() {
        return clienteNit;
    }

    public void setClienteNit(String clienteNit) {
        this.clienteNit = clienteNit;
    }

    private String descuento;

    @Basic
    @javax.persistence.Column(name = "descuento", nullable = true, insertable = true, updatable = true, length = 200)
    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    private Integer estadoImpresion;

    @Basic
    @javax.persistence.Column(name = "estado_impresion", nullable = true, insertable = true, updatable = true)
    public Integer getEstadoImpresion() {
        return estadoImpresion;
    }

    public void setEstadoImpresion(Integer estadoImpresion) {
        this.estadoImpresion = estadoImpresion;
    }

    private Integer estadoFactura;

    @Basic
    @javax.persistence.Column(name = "estado_factura", nullable = true, insertable = true, updatable = true)
    public Integer getEstadoFactura() {
        return estadoFactura;
    }

    public void setEstadoFactura(Integer estadoFactura) {
        this.estadoFactura = estadoFactura;
    }

    private String metodoPago;

    @Basic
    @javax.persistence.Column(name = "metodo_pago", nullable = true, insertable = true, updatable = true, length = 200)
    public String getMetodoPago() {
        return metodoPago;
    }

    public void setMetodoPago(String metodoPago) {
        this.metodoPago = metodoPago;
    }

    private String detalleItem;

    @Basic
    @javax.persistence.Column(name = "detalle_item", nullable = true, insertable = true, updatable = true, length = 1048)
    public String getDetalleItem() {
        return detalleItem;
    }

    public void setDetalleItem(String detalleItem) {
        this.detalleItem = detalleItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Facturas facturas = (Facturas) o;

        if (id != null ? !id.equals(facturas.id) : facturas.id != null) return false;
        if (emisorNombre != null ? !emisorNombre.equals(facturas.emisorNombre) : facturas.emisorNombre != null)
            return false;
        if (emisorNit != null ? !emisorNit.equals(facturas.emisorNit) : facturas.emisorNit != null) return false;
        if (emisorActividadComercial != null ? !emisorActividadComercial.equals(facturas.emisorActividadComercial) : facturas.emisorActividadComercial != null)
            return false;
        if (emisorDireccion != null ? !emisorDireccion.equals(facturas.emisorDireccion) : facturas.emisorDireccion != null)
            return false;
        if (emisorTelefonos != null ? !emisorTelefonos.equals(facturas.emisorTelefonos) : facturas.emisorTelefonos != null)
            return false;
        if (emisorCiudad != null ? !emisorCiudad.equals(facturas.emisorCiudad) : facturas.emisorCiudad != null)
            return false;
        if (numeroFactura != null ? !numeroFactura.equals(facturas.numeroFactura) : facturas.numeroFactura != null)
            return false;
        if (numeroAutorizacion != null ? !numeroAutorizacion.equals(facturas.numeroAutorizacion) : facturas.numeroAutorizacion != null)
            return false;
        if (fecha != null ? !fecha.equals(facturas.fecha) : facturas.fecha != null) return false;
        if (fechaLimiteEmision != null ? !fechaLimiteEmision.equals(facturas.fechaLimiteEmision) : facturas.fechaLimiteEmision != null)
            return false;
        if (montoFactura != null ? !montoFactura.equals(facturas.montoFactura) : facturas.montoFactura != null)
            return false;
        if (montoCreditoFiscal != null ? !montoCreditoFiscal.equals(facturas.montoCreditoFiscal) : facturas.montoCreditoFiscal != null)
            return false;
        if (codigoControl != null ? !codigoControl.equals(facturas.codigoControl) : facturas.codigoControl != null)
            return false;
        if (clienteRazonsocial != null ? !clienteRazonsocial.equals(facturas.clienteRazonsocial) : facturas.clienteRazonsocial != null)
            return false;
        if (clienteNit != null ? !clienteNit.equals(facturas.clienteNit) : facturas.clienteNit != null) return false;
        if (descuento != null ? !descuento.equals(facturas.descuento) : facturas.descuento != null) return false;
        if (estadoImpresion != null ? !estadoImpresion.equals(facturas.estadoImpresion) : facturas.estadoImpresion != null)
            return false;
        if (estadoFactura != null ? !estadoFactura.equals(facturas.estadoFactura) : facturas.estadoFactura != null)
            return false;
        if (metodoPago != null ? !metodoPago.equals(facturas.metodoPago) : facturas.metodoPago != null) return false;
        if (detalleItem != null ? !detalleItem.equals(facturas.detalleItem) : facturas.detalleItem != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (emisorNombre != null ? emisorNombre.hashCode() : 0);
        result = 31 * result + (emisorNit != null ? emisorNit.hashCode() : 0);
        result = 31 * result + (emisorActividadComercial != null ? emisorActividadComercial.hashCode() : 0);
        result = 31 * result + (emisorDireccion != null ? emisorDireccion.hashCode() : 0);
        result = 31 * result + (emisorTelefonos != null ? emisorTelefonos.hashCode() : 0);
        result = 31 * result + (emisorCiudad != null ? emisorCiudad.hashCode() : 0);
        result = 31 * result + (numeroFactura != null ? numeroFactura.hashCode() : 0);
        result = 31 * result + (numeroAutorizacion != null ? numeroAutorizacion.hashCode() : 0);
        result = 31 * result + (fecha != null ? fecha.hashCode() : 0);
        result = 31 * result + (fechaLimiteEmision != null ? fechaLimiteEmision.hashCode() : 0);
        result = 31 * result + (montoFactura != null ? montoFactura.hashCode() : 0);
        result = 31 * result + (montoCreditoFiscal != null ? montoCreditoFiscal.hashCode() : 0);
        result = 31 * result + (codigoControl != null ? codigoControl.hashCode() : 0);
        result = 31 * result + (clienteRazonsocial != null ? clienteRazonsocial.hashCode() : 0);
        result = 31 * result + (clienteNit != null ? clienteNit.hashCode() : 0);
        result = 31 * result + (descuento != null ? descuento.hashCode() : 0);
        result = 31 * result + (estadoImpresion != null ? estadoImpresion.hashCode() : 0);
        result = 31 * result + (estadoFactura != null ? estadoFactura.hashCode() : 0);
        result = 31 * result + (metodoPago != null ? metodoPago.hashCode() : 0);
        result = 31 * result + (detalleItem != null ? detalleItem.hashCode() : 0);
        return result;
    }
}
