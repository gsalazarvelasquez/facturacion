package ch.swissbytes.fundempresa.billing.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by daniel on 28/03/2016.
 */

@Getter
@Setter
public class PaymentRequestDto implements Serializable {

    protected String tipo;
    protected String userName;
    protected String password;

    protected long orderId;
    protected String monto;
    protected String nit;
    protected String razonSocial;
}