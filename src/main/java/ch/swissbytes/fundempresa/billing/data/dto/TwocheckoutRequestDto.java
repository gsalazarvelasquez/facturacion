package ch.swissbytes.fundempresa.billing.data.dto;

import ch.swissbytes.fundempresa.ventanilla.data.dto.CertificadosRequestDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.FotocopiasRequestDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.HomonimiaRequestDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteRequestDto;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by jose on 22/03/2016.
 */
@Getter
@Setter
public class TwocheckoutRequestDto extends PaymentRequestDto implements Serializable {

    private String solicitante;
    private String ci;
    private String paymentStatus;
    private String message;

    private HomonimiaRequestDto homonimiaRequestDto;
    private FotocopiasRequestDto fotocopiasRequestDto;
    private CertificadosRequestDto certificadosRequestDto;

}
