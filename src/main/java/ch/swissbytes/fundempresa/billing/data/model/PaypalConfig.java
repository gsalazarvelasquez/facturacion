package ch.swissbytes.fundempresa.billing.data.model;

import javax.persistence.*;

/**
 * Created by daniel on 19/05/2016.
 */
@Entity
@Table(name = "paypal_config", schema = "fundempresa_local", catalog = "")
public class PaypalConfig {
    private Integer id;
    private String account;
    private String returnUrl;
    private String tipoCambio;
    private String motivo;
    private String urlError;
    private String urlCorrect;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "account", nullable = true, length = 350)
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Basic
    @Column(name = "return_url", nullable = true, length = 350)
    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    @Basic
    @Column(name = "tipo_cambio", nullable = true, length = 350)
    public String getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(String tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    @Basic
    @Column(name = "motivo", nullable = true, length = 350)
    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    @Basic
    @Column(name = "url_error", nullable = true, length = 350)
    public String getUrlError() {
        return urlError;
    }

    public void setUrlError(String urlError) {
        this.urlError = urlError;
    }

    @Basic
    @Column(name = "url_correct", nullable = true, length = 350)
    public String getUrlCorrect() {
        return urlCorrect;
    }

    public void setUrlCorrect(String urlCorrect) {
        this.urlCorrect = urlCorrect;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaypalConfig that = (PaypalConfig) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (account != null ? !account.equals(that.account) : that.account != null) return false;
        if (returnUrl != null ? !returnUrl.equals(that.returnUrl) : that.returnUrl != null) return false;
        if (tipoCambio != null ? !tipoCambio.equals(that.tipoCambio) : that.tipoCambio != null) return false;
        if (motivo != null ? !motivo.equals(that.motivo) : that.motivo != null) return false;
        if (urlError != null ? !urlError.equals(that.urlError) : that.urlError != null) return false;
        if (urlCorrect != null ? !urlCorrect.equals(that.urlCorrect) : that.urlCorrect != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (account != null ? account.hashCode() : 0);
        result = 31 * result + (returnUrl != null ? returnUrl.hashCode() : 0);
        result = 31 * result + (tipoCambio != null ? tipoCambio.hashCode() : 0);
        result = 31 * result + (motivo != null ? motivo.hashCode() : 0);
        result = 31 * result + (urlError != null ? urlError.hashCode() : 0);
        result = 31 * result + (urlCorrect != null ? urlCorrect.hashCode() : 0);
        return result;
    }
}
