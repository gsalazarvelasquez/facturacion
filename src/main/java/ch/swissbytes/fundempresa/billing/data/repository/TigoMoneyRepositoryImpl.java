package ch.swissbytes.fundempresa.billing.data.repository;

import ch.swissbytes.fundempresa.billing.data.model.TigoMoneyTransacciones;
import ch.swissbytes.shared.persistence.Repository;
import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by daniel on 11/02/2016.
 */
@Stateless
public class TigoMoneyRepositoryImpl extends Repository {

    public boolean save(TigoMoneyTransacciones tigoMoney) {
        saveAndFlush(tigoMoney);
        return true;
    }

    @Override
    protected Class getPersistentClass() {
        return this.getClass();
    }

    public long getLast(String username) {
        long result = 0L;
        try {
            Query query = em.createQuery("SELECT id FROM TigoMoneyTransacciones transaccion " +
                    "WHERE transaccion.requestUsername = :username ORDER BY transaccion.id DESC");
            query.setParameter("username", username);
            List list = query.getResultList();
            if(list != null && !list.isEmpty())
                result = (Long) list.get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
