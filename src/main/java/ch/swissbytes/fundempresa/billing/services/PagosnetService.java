package ch.swissbytes.fundempresa.billing.services;

import ch.swissbytes.fundempresa.billing.exception.InfoAbsentException;
import ch.swissbytes.fundempresa.billing.services.internal.pagosnet.PagoDto;

import javax.ejb.Local;

@Local
public interface PagosnetService {

      String pay(PagoDto pago, boolean recharge) throws InfoAbsentException;
}
