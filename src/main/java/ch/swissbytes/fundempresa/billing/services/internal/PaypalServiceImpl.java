package ch.swissbytes.fundempresa.billing.services.internal;

import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.billing.data.dto.*;
import ch.swissbytes.fundempresa.billing.data.model.*;
import ch.swissbytes.fundempresa.billing.data.repository.PayPalRepositoryImpl;
import ch.swissbytes.fundempresa.billing.exception.*;
import ch.swissbytes.fundempresa.billing.services.DebitoService;
import ch.swissbytes.fundempresa.billing.services.InvoiceService;
import ch.swissbytes.fundempresa.billing.services.PaypalService;
import ch.swissbytes.fundempresa.ventanilla.data.dto.ResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.model.Fotocopia;
import ch.swissbytes.fundempresa.ventanilla.data.model.Homonimia;
import ch.swissbytes.fundempresa.ventanilla.services.*;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by jorgeburgos on 1/30/16.
 */
public class PaypalServiceImpl implements PaypalService {

    @Inject
    public PreciosService preciosService;

    @Inject
    private DebitoService debitoService;

    @Inject
    private InvoiceService invoiceService;

    @Inject
    private PayPalRepositoryImpl payPalRepositoryImpl;

    @Inject
    private HomonimiaService homonimiaService;

    @Inject
    private FotocopiasService fotocopiasService;

    @Inject
    private VentaBDListadoService ventaBDService;

    @Inject
    private CertificadosService certificadosService;

    public PaypalServiceImpl() {
    }

    @Override
    public PaypalConfig config() throws Exception {
        return payPalRepositoryImpl.getConfig();
    }

    @Override
    public PaypalTransacciones pay(PayPalRequestDto request) throws Exception {
        PaypalTransacciones transaction = payPalRepositoryImpl.getById(PaypalTransacciones.class, request.getOrderId()).get();
        transaction.setResponsePaymentStatus(request.getPaymentStatus());
        payPalRepositoryImpl.update(transaction);
        updateTransaction(transaction);

        request.setUserName(transaction.getRequestUsername());
        request.setMonto(transaction.getRequestMonto());
        request.setRazonSocial(transaction.getRequestRazonsocial());
        request.setNit(transaction.getRequestNit());
        boolean recharge = transaction.getRequestTipotramite().equals("FACTURAS");
        registerAdabas(request, recharge);
        if(recharge){
            increaseBalance(request);
        }
        String motivo = "";
        if(request.getHomonimiaRequestDto() != null)
            motivo = ": Control de Homonimia";
        if(request.getFotocopiasRequestDto() != null)
            motivo = ": Fotocopia de Documentos";
        if(request.getCertificadosRequestDto() != null)
            motivo = ": Certificados";
        String detail = (recharge ? "Recarga" : "Pago") + " Ventanilla Virtual" + motivo;
        request.setTipo("3");
        invoiceService.generate(request, detail);
        return transaction;
    }

    private void updateTransaction(PaypalTransacciones transaction) {
        String tipo = transaction.getRequestTipotramite();
        if(tipo.equalsIgnoreCase("HOMONIMIA")){
            //TODO: UPDATE HOMONIMIA TRANSACTION AS PAID
            String pkTramite = transaction.getRequestPktramite();
            System.out.println("Buscando homonimia: " + pkTramite);
            Homonimia homonimia = homonimiaService.get(pkTramite);
            if(homonimia != null){
                homonimia.setPayCode("0");
                homonimia.setEstadoTramite(TramiteResponseDto.EstadoTramite.PAGADO.name());
                homonimiaService.update(homonimia);
            }
            return;
        }
        if(tipo.equalsIgnoreCase("FOTOCOPIA")) {
            //TODO: UPDATE FOTOCOPIAS TRANSACTION AS PAID
            String pkTramite = transaction.getRequestPktramite();
            Fotocopia fotocopia = fotocopiasService.get(pkTramite);
            if(fotocopia != null){
                fotocopia.setPayCode("0");
                fotocopia.setEstadoTramite(TramiteResponseDto.EstadoTramite.PAGADO.name());
                fotocopiasService.update(fotocopia);
            }
            return;
        }
    }

    @Override
    public PaypalTransacciones save(PayPalRequestDto requestDto) throws Exception {
        UserProfileDto userProfileDto = AdabasD.profile(requestDto.getUserName());
        requestDto.setSolicitante(userProfileDto.getFullName());
        requestDto.setCi(userProfileDto.getCi());
        return saveTransaction(requestDto);
    }

    private PaypalTransacciones saveTransaction(PayPalRequestDto requestDto) throws Exception {
        String pkTramite = getPkTramite(requestDto);

        PaypalTransacciones paypal = new PaypalTransacciones();
        paypal.setRequestUsername(requestDto.getUserName());
        paypal.setRequestNombre(requestDto.getSolicitante());
        paypal.setRequestNumerodocumento(requestDto.getCi());
        paypal.setRequestRazonsocial(requestDto.getRazonSocial());
        paypal.setRequestNit(requestDto.getNit());
        paypal.setRequestMonto(requestDto.getMonto());
        paypal.setRequestFechahora(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        paypal.setRequestTipotramite(String.valueOf(requestDto.getTipo()));
        paypal.setRequestPktramite(pkTramite);
        payPalRepositoryImpl.saveAndFlush(paypal);
        paypal.setRequestOrderid(String.valueOf(paypal.getId()));
        return paypal;
    }

    private String getPkTramite(PayPalRequestDto requestDto) {
        String tipo = requestDto.getTipo();

        if(tipo.equalsIgnoreCase("HOMONIMIA")){
            Long pkTramite = homonimiaService.saveDraft(requestDto.getHomonimiaRequestDto());
            return String.valueOf(pkTramite);
        }
        if(tipo.equalsIgnoreCase("FOTOCOPIA")){
            Long pkTramite = fotocopiasService.saveDraft(requestDto.getFotocopiasRequestDto());
            return String.valueOf(pkTramite);
        }
        if(tipo.equalsIgnoreCase("LISTADOEMPRESAS")){
//            Long pkTramite = ventaBDService.saveDraft(requestDto.getTramiteRequestDto());
//            return String.valueOf(pkTramite);
        }
        if(tipo.equalsIgnoreCase("ESTADISTICABASICA")){
//            Long pkTramite = ventaBDService.saveDraft(requestDto.getTramiteRequestDto());
//            return String.valueOf(pkTramite);
        }
        if(tipo.equalsIgnoreCase("CRUCEVARIABLES")){
//            Long pkTramite = ventaBDService.saveDraft(requestDto.getTramiteRequestDto());
//            return String.valueOf(pkTramite);
        }
        if(tipo.equalsIgnoreCase("CERTIFICADOSESPECIALES")){
//            Long pkTramite = certificadosService.saveDraft(requestDto.getTramiteRequestDto());
//            return String.valueOf(pkTramite);
        }
        if(tipo.equalsIgnoreCase("CERTIFICADOSENLINEA")){
//            Long pkTramite = certificadosService.saveDraft(requestDto.getTramiteRequestDto());
//            return String.valueOf(pkTramite);
        }
        return "";
    }

    private void registerAdabas(PayPalRequestDto requestDto, boolean recharge) {
        String username = requestDto.getUserName();
        ResponseDto resp = new ResponseDto();
        resp.setArancel(String.valueOf(requestDto.getMonto()));
        resp.setDescTramite("");

        String matricula = "";
        String serviceRecharge = "07040205";
        String description = recharge ? "Recarga" : "Pago";
        description += " Paypal";
        AdabasD.transaction(resp, username, matricula, serviceRecharge, description);

        registerPayment(username, matricula, description,
                "91", requestDto.getMonto(), serviceRecharge, description);
    }

    private void registerPayment(String username, String matricula, String pago, String orderId, String monto, String servicio, String descripcion) {
        preciosService.registrarPago(username, matricula, pago, orderId, monto, servicio, descripcion);
    }

    private void increaseBalance(PayPalRequestDto requestDto) throws ParametersException, AdabasDRechargeException {
        requestDto.setMonto(requestDto.getMonto());
        debitoService.add(requestDto);
    }
}
