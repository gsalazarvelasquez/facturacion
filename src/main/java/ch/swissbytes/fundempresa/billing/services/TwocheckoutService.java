package ch.swissbytes.fundempresa.billing.services;

import ch.swissbytes.fundempresa.billing.data.dto.TwocheckoutRequestDto;
import ch.swissbytes.fundempresa.billing.data.dto.TwocheckoutResponseDto;
import ch.swissbytes.fundempresa.billing.data.model.TwocheckoutConfig;
import ch.swissbytes.fundempresa.billing.data.model.TwocheckoutTransacciones;
import ch.swissbytes.fundempresa.billing.exception.AdabasDRechargeException;
import ch.swissbytes.fundempresa.billing.exception.DosageException;
import ch.swissbytes.fundempresa.billing.exception.InfoAbsentException;
import ch.swissbytes.fundempresa.billing.exception.ParametersException;
import javax.ejb.Local;
import java.text.ParseException;

/**
 * Created by miguel on 1/26/16.
 */
@Local
public interface TwocheckoutService {

      TwocheckoutTransacciones pay(TwocheckoutRequestDto requestDto);

      TwocheckoutTransacciones save(TwocheckoutRequestDto requestDto) throws Exception;

      TwocheckoutConfig config() throws Exception;
}
