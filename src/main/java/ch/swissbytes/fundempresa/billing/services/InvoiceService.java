package ch.swissbytes.fundempresa.billing.services;

import ch.swissbytes.fundempresa.billing.data.dto.PaymentRequestDto;
import ch.swissbytes.fundempresa.billing.data.model.Facturas;
import ch.swissbytes.fundempresa.billing.exception.DosageException;
import ch.swissbytes.fundempresa.billing.exception.InfoAbsentException;

import javax.ejb.Local;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.List;

/**
 * Created by miguel on 1/26/16.
 */
@Local
public interface InvoiceService {

      List<Facturas> findAll();

      Facturas test();

      Facturas generate(PaymentRequestDto requestDto, String detail) throws DosageException, ParseException, InfoAbsentException;

      Facturas save(PaymentRequestDto requestDto, String detail, int estado) throws InfoAbsentException;

      String printPDF(OutputStream outputStream, Facturas invoice) throws Exception;

}
