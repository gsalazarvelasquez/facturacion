package ch.swissbytes.fundempresa.billing.services.internal;

import ch.swissbytes.ewallet.service.EWalletService;
import ch.swissbytes.ewallet.tigomoney.dto.TMConnectionDto;
import ch.swissbytes.ewallet.tigomoney.dto.TMPaymentRequestDto;
import ch.swissbytes.ewallet.tigomoney.dto.TMPaymentResponseDto;
import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.billing.data.dto.DebitoResponseDto;
import ch.swissbytes.fundempresa.billing.data.dto.TigoMoneyRequestDto;
import ch.swissbytes.fundempresa.billing.data.dto.TigoMoneyResponseDto;
import ch.swissbytes.fundempresa.billing.data.model.Facturas;
import ch.swissbytes.fundempresa.billing.data.model.TigoMoneyConfig;
import ch.swissbytes.fundempresa.billing.data.model.TigoMoneyTransacciones;
import ch.swissbytes.fundempresa.billing.data.repository.InvoiceRepositoryImpl;
import ch.swissbytes.fundempresa.billing.data.repository.TigoMoneyConfigRepositoryImpl;
import ch.swissbytes.fundempresa.billing.data.repository.TigoMoneyErrorsRepositoryImpl;
import ch.swissbytes.fundempresa.billing.data.repository.TigoMoneyRepositoryImpl;
import ch.swissbytes.fundempresa.billing.exception.*;
import ch.swissbytes.fundempresa.billing.services.DebitoService;
import ch.swissbytes.fundempresa.billing.services.InvoiceService;
import ch.swissbytes.fundempresa.billing.services.TigoMoneyService;
import ch.swissbytes.fundempresa.ventanilla.data.dto.ResponseDto;
import ch.swissbytes.fundempresa.ventanilla.services.PreciosService;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by jorgeburgos on 1/30/16.
 */
//@Stateless
public class TigoMoneyServiceImpl implements TigoMoneyService {

    @Inject
    private InvoiceService invoiceService;

    @Inject
    public PreciosService preciosService;

    @Inject
    private DebitoService debitoService;

    @Inject
    private TigoMoneyConfigRepositoryImpl tigoMoneyConfigRepository;

    @Inject
    private TigoMoneyRepositoryImpl tigoMoneyRepository;

    @Inject
    private TigoMoneyErrorsRepositoryImpl tigoMoneyErrorsRepository;

    TMConnectionDto tmConnectionDto;
    EWalletService eWalletService;
    TigoMoneyConfig config;

    public TigoMoneyServiceImpl() {
    }

    @Override
    public TigoMoneyResponseDto pay(TigoMoneyRequestDto requestDto, boolean recharge) throws ParametersException, AdabasDRechargeException, OrderIDException, TMException, InfoAbsentException {
        TigoMoneyResponseDto response = new TigoMoneyResponseDto();
        if(validate(requestDto)){
            setConfig();
            UserProfileDto userProfileDto = AdabasD.profile(requestDto.getUserName());
            TMPaymentRequestDto payment = initRequest(requestDto, userProfileDto);
            TigoMoneyTransacciones tigoMoney = saveTransaction(requestDto);
            long orderId = tigoMoneyRepository.getLast(requestDto.getUserName());

            if(orderId > 0){
                TMPaymentResponseDto paymentResponse = askTigoMoney(payment, orderId);
                updateTransaction(tigoMoney, paymentResponse);

                String errorDesc = mensajeTigoMoney(paymentResponse.getCode());
                errorDesc = errorDesc.isEmpty() ? paymentResponse.getDescription() : errorDesc;
                response.setCodigoError(String.valueOf(paymentResponse.getCode()));
                response.setMensajeError(errorDesc);

                if(paymentResponse.getCode() == 0){
                    response.setPayCode("0");
                    String detail = registerAdabas(userProfileDto, recharge, requestDto, payment);

                    requestDto.setTipo("1");//TIGOMONEY
                    requestDto.setOrderId(orderId);
                    invoiceService.save(requestDto, detail, 1);

                    if(recharge){
                        increaseBalance(requestDto, orderId);
                    }
                }
            }else {
                throw new OrderIDException();
            }
        }else{
            throw new ParametersException();
        }
        return response;
    }

    private boolean validate(TigoMoneyRequestDto requestDto) {
        if(requestDto == null ||
                requestDto.getMensaje() == null ||
                requestDto.getMonto() == null ||
                requestDto.getNit() == null ||
                requestDto.getUserName() == null ||
                requestDto.getPregunta() == null ||
                requestDto.getMonto().isEmpty())
            return false;
        return true;
    }

    private void setConfig() throws TMException {
        try{
            config = tigoMoneyConfigRepository.getConfig();

            String url = config.getTestUrl();
            String encryptionKey = config.getTestEncryptionKey();
            String idKey = config.getTestIdKey();

            tmConnectionDto = new TMConnectionDto(url, encryptionKey, idKey);
            eWalletService = new ch.swissbytes.ewallet.tigomoney.action.TigoMoneyServiceImpl(tmConnectionDto);
        }catch (Exception e) {
            String message = "";
            try{
                Throwable t = e.getCause();
                while (t != null){
                    message += t.getMessage().contains("://")? "" : (t.getMessage() +  " | ");
                    t = t.getCause();
                }
            }catch (Exception ex){}
            throw new TMException(message);
        }
    }

    private TMPaymentRequestDto initRequest(TigoMoneyRequestDto requestDto, UserProfileDto userProfileDto) {
        requestDto.setSolicitante(userProfileDto.getFullName());
        requestDto.setCi(userProfileDto.getCi());

        TMPaymentRequestDto payment = new TMPaymentRequestDto();
        payment.setTimeout(config.getTestTimeout());
        payment.setCorrectUrl(config.getTestUrlSuccess());
        payment.setErrorUrl(config.getTestUrlError());

        payment.setConfirmation(requestDto.getPregunta());
        payment.setNotification(requestDto.getMensaje());
        payment.setName(requestDto.getSolicitante());
        payment.setMessage(requestDto.getMensaje());
        payment.setLine(requestDto.getNumeroTigoMoney());
        payment.setDocumentNumber(requestDto.getCi());
        payment.setBusinessName(requestDto.getNit());
        payment.setNit(requestDto.getNit());
        payment.setAmount(Double.parseDouble(requestDto.getMonto()));

        String razonSocial = requestDto.getRazonSocial();
        razonSocial = (razonSocial == null || requestDto.getNit().equals("0") ||
                razonSocial.isEmpty())? "Sin Nombre" : razonSocial;
        requestDto.setRazonSocial(razonSocial);
        return payment;
    }

    private TigoMoneyTransacciones saveTransaction(TigoMoneyRequestDto requestDto) {
        TigoMoneyTransacciones tigoMoney = new TigoMoneyTransacciones();
        tigoMoney.setRequestUsername(requestDto.getUserName());
        tigoMoney.setRequestNombre(requestDto.getSolicitante());
        tigoMoney.setRequestNumerodocumento(requestDto.getCi());
        tigoMoney.setRequestRazonsocial(requestDto.getRazonSocial());
        tigoMoney.setRequestNit(requestDto.getNit());
        tigoMoney.setRequestNumerolinea(String.valueOf(requestDto.getNumeroTigoMoney()));
        tigoMoney.setRequestMonto(requestDto.getMonto());
        tigoMoney.setRequestFechahora(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        tigoMoneyRepository.save(tigoMoney);
        return tigoMoney;
    }

    private TMPaymentResponseDto askTigoMoney(TMPaymentRequestDto payment, long orderId) {
        payment.setOrderId(String.valueOf(orderId));
        TMPaymentResponseDto paymentResponse = eWalletService.requestPayment(payment);
        return paymentResponse;
    }

    private void updateTransaction(TigoMoneyTransacciones tigoMoney, TMPaymentResponseDto paymentResponse) {
        tigoMoney.setResponseCodigoerror(Long.valueOf(paymentResponse.getCode()));
        tigoMoney.setResponseMensaje(paymentResponse.getMessage());
        tigoMoney.setResponseOrderid(paymentResponse.getOrderId());
        tigoMoney.setResponseDescripcion(paymentResponse.getDescription());
        tigoMoneyRepository.update(tigoMoney);
    }

    private String mensajeTigoMoney(int code) {
        return tigoMoneyErrorsRepository.getbyCode(code);
    }

    private String registerAdabas(UserProfileDto userProfileDto, boolean recharge,
                                TigoMoneyRequestDto requestDto, TMPaymentRequestDto payment) {
        String matricula = userProfileDto.getMatricula();
        matricula = matricula == null ? "0" : matricula;
        String username = requestDto.getUserName();
        ResponseDto resp = new ResponseDto();
        resp.setArancel(String.valueOf(payment.getAmount()));
        resp.setDescTramite("");

        String serviceRecharge = "07040205";
        String description = recharge ? "Recarga" : "Pago";
        description += " de saldo Ventanilla Virtual";
        AdabasD.transaction(resp, username, matricula, serviceRecharge, description);

        description = recharge ? "Recarga" : "Pago";
        description += " Tigo Money";
        registrarPago(username, matricula, description,
                "91", requestDto.getMonto(), serviceRecharge, description);
        return description;
    }

    private void increaseBalance(TigoMoneyRequestDto requestDto, long orderId) throws ParametersException, AdabasDRechargeException {
        requestDto.setOrderId(orderId);
        requestDto.setMonto(requestDto.getMonto());
        debitoService.add(requestDto);
    }

    private void registrarPago(String username, String matricula, String pago, String orderId, String monto, String servicio, String descripcion) {
        preciosService.registrarPago(username, matricula, pago, orderId, monto, servicio, descripcion);
    }
}
