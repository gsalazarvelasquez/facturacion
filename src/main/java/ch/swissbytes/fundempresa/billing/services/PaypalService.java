package ch.swissbytes.fundempresa.billing.services;

import ch.swissbytes.fundempresa.billing.data.dto.PayPalRequestDto;
import ch.swissbytes.fundempresa.billing.data.dto.PayPalResponseDto;
import ch.swissbytes.fundempresa.billing.data.model.PaypalConfig;
import ch.swissbytes.fundempresa.billing.data.model.PaypalTransacciones;
import ch.swissbytes.fundempresa.billing.exception.AdabasDRechargeException;
import ch.swissbytes.fundempresa.billing.exception.DosageException;
import ch.swissbytes.fundempresa.billing.exception.InfoAbsentException;
import ch.swissbytes.fundempresa.billing.exception.ParametersException;
import javax.ejb.Local;
import java.text.ParseException;

/**
 * Created by miguel on 1/26/16.
 */
@Local
public interface PaypalService {

      PaypalTransacciones save(PayPalRequestDto requestDto) throws Exception;

      PaypalTransacciones pay(PayPalRequestDto requestDto) throws Exception;

      PaypalConfig config() throws Exception;
}
