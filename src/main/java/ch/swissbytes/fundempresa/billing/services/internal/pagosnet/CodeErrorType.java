package ch.swissbytes.fundempresa.billing.services.internal.pagosnet;

/**
 * 0 = Successful registration
 * 1 = Error registering payment
 * 2 = Revenue doubled
 * 3 = Reversal duplicate
 * 4 = Unable to reverse, there original txt
 * Another value = is taken as "Severe Error"
 *
 * @author Geremias Gonzalez
 */
public enum CodeErrorType {

    SUCCESSFUL_REGISTRATION(0),
    ERROR_REGISTERING(1),
    REVENUE_DOUBLED(2),
    REVERSAL_DUPLICATE(3),
    UNABLE_TO_REVERSE(4),
    SEVERE_ERROR(5);
    private final int value;

    private CodeErrorType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static CodeErrorType fromValue(int value) {
        CodeErrorType result = SEVERE_ERROR;
        for (CodeErrorType type : values()) {
            if (type.getValue() == value) {
                result = type;
                break;
            }
        }
        return result;
    }
}