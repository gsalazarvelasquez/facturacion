package ch.swissbytes.fundempresa.billing.services;

import ch.swissbytes.fundempresa.billing.data.dto.TigoMoneyRequestDto;
import ch.swissbytes.fundempresa.billing.data.dto.TigoMoneyResponseDto;
import ch.swissbytes.fundempresa.billing.exception.*;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by miguel on 1/26/16.
 */
@Local
public interface TigoMoneyService {

      TigoMoneyResponseDto pay(TigoMoneyRequestDto requestDto, boolean recharge) throws ParametersException, AdabasDRechargeException, OrderIDException, TMException, InfoAbsentException;
}
