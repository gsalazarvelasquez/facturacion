package ch.swissbytes.fundempresa.billing.services.internal.invoicegenerator;

import java.io.Serializable;
import java.util.regex.Pattern;

public class NumberToLiteral implements Serializable{

    private static final String[] UNITS = {"", "un ", "dos ", "tres ", "cuatro ", "cinco ", "seis ", "siete ", "ocho ", "nueve "};
    private  static final String[] DOZENS = {"diez ", "once ", "doce ", "trece ", "catorce ", "quince ", "dieciseis ",
            "diecisiete ", "dieciocho ", "diecinueve", "veinte ", "treinta ", "cuarenta ",
            "cincuenta ", "sesenta ", "setenta ", "ochenta ", "noventa "};
    private  static final String[] HUNDREDS = {"", "ciento ", "doscientos ", "trescientos ", "cuatrocientos ", "quinientos ", "seiscientos ",
            "setecientos ", "ochocientos ", "novecientos "};

    /**
     * Convert a number to literal
     * @param number Number with format #,##0.00
     * @param uppercase True to uppercase the result or false if not
     * @return Literal number
     */
    public static String convert(String number, boolean uppercase) {
        String literal = "";
        String decimalPart;

        if(!number.contains(".")){
            number = number + ".00";
        }
        if (Pattern.matches("(,*|\\d{1,9})*[.]\\d{1,3}", number)) {
            String[] num = number.split("\\.");
            decimalPart = num[1] + "/100 Bolivianos.";
            String integerPart = num[0].replace(",", "");
            if (Integer.parseInt(integerPart) == 0) {
                literal = "cero ";
            } else if (Integer.parseInt(integerPart) > 999999) {
                literal = getMillions(integerPart);
            } else if (Integer.parseInt(integerPart) > 999) {
                literal = getMiles(integerPart);
            } else if (Integer.parseInt(integerPart) > 99) {
                literal = getHundreds(integerPart);
            } else if (Integer.parseInt(integerPart) > 9) {
                literal = getDozens(integerPart);
            } else {
                literal = getUnits(integerPart);
            }
            if (uppercase) {
                return (literal + decimalPart).toUpperCase();
            } else {
                return (literal + decimalPart);
            }
        } else {
            return null;
        }
    }

    private static String getUnits(String num) {
        String numb = num.substring(num.length() - 1);
        return UNITS[Integer.parseInt(numb)];
    }

    private  static String getDozens(String num) {
        int n = Integer.parseInt(num);
        if (n < 10) {
            return getUnits(num);
        } else if (n > 19) {
            String u = getUnits(num);
            if (u.equals("")) {
                return DOZENS[Integer.parseInt(num.substring(0, 1)) + 8];
            } else {
                return DOZENS[Integer.parseInt(num.substring(0, 1)) + 8] + "y " + u;
            }
        } else {
            return DOZENS[n - 10];
        }
    }

    private  static String getHundreds(String num) {
        if( Integer.parseInt(num) > 99 ){
            if (Integer.parseInt(num) == 100) {
                return " cien ";
            } else {
                return HUNDREDS[Integer.parseInt(num.substring(0, 1))] + getDozens(num.substring(1));
            }
        }else{
            return getDozens(Integer.parseInt(num)+"");
        }
    }

    private  static String getMiles(String number) {
        String c = number.substring(number.length() - 3);
        String m = number.substring(0, number.length() - 3);
        String n = "";
        if (Integer.parseInt(m) > 0) {
            n = getHundreds(m);
            return n + "mil " + getHundreds(c);
        } else {
            return "" + getHundreds(c);
        }
    }
    private  static String getMillions(String number) {
        String miles = number.substring(number.length() - 6);
        String million = number.substring(0, number.length() - 6);
        String n = "";
        if(million.length() > 1){
            n = getHundreds(million) + "millones ";
        }else{
            n = getUnits(million) + "millon ";
        }
        return n + getMiles(miles);
    }
}
