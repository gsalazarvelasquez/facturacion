package ch.swissbytes.fundempresa.billing.services.internal;

import ch.swissbytes.ewallet.tigomoney.dto.TMPaymentRequestDto;
import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.billing.data.dto.PagosnetRequestDto;
import ch.swissbytes.fundempresa.billing.data.dto.TigoMoneyRequestDto;
import ch.swissbytes.fundempresa.billing.exception.InfoAbsentException;
import ch.swissbytes.fundempresa.billing.services.InvoiceService;
import ch.swissbytes.fundempresa.billing.services.PagosnetService;
import ch.swissbytes.fundempresa.billing.services.internal.pagosnet.*;
import ch.swissbytes.fundempresa.ventanilla.data.dto.ResponseDto;
import ch.swissbytes.fundempresa.ventanilla.services.PreciosService;
import org.apache.commons.lang3.time.DateFormatUtils;

import javax.inject.Inject;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;

public class PagosnetServiceImpl implements PagosnetService {

    @Inject
    private InvoiceService invoiceService;

    private String pagosNetURL;
    private String companyCodeWs;
    private int expirationMonths;
    private ComelecSoap comelecSoapPort;

    @Inject
    public PreciosService preciosService;

    public PagosnetServiceImpl() throws Exception {
        String username = ParametrosPago.pagosnetUsername;
        String password = ParametrosPago.pagosnetPassword;
        pagosNetURL = ParametrosPago.pagosnetUrl;
        Comelec service = new Comelec(new URL(pagosNetURL), new QName("http://www.openuri.org/", "Comelec"));
        comelecSoapPort = service.getComelecSoap();
        BindingProvider bp = ((BindingProvider) comelecSoapPort);
        bp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, username);
        bp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, password);
        companyCodeWs = ParametrosPago.pagosnetCompany;
        expirationMonths = Integer.parseInt(ParametrosPago.pagosnetExpiration);
    }

    @Override
    public String pay(PagoDto pago, boolean recharge) throws InfoAbsentException {
        PagosnetRequestDto requestDto = new PagosnetRequestDto();
        DPlanilla ePago = new DPlanilla();
        String detail = buildDetail(recharge);
        ePago.setDescripcion(detail);
        ePago.setMontoCreditoFiscal(pago.getMontoTotal().doubleValue());
        ePago.setMontoPago(pago.getMontoTotal().doubleValue());
        ePago.setNitFactura(pago.getNitFacturar());
        ePago.setNombreFactura(pago.getNombreFacturar());
        ePago.setNumeroPago(1); //number of payments: ALWAYS 1
        ArrayOfDPlanilla dPago = new ArrayOfDPlanilla();
        dPago.getDPlanilla().add(ePago);
        requestDto.setTipo("2");//PAGOSNET
        requestDto.setNit(pago.getNitFacturar());
        requestDto.setRazonSocial(pago.getNombreFacturar());
        requestDto.setMonto(String.valueOf(pago.getMontoTotal().doubleValue()));

        // *********************************************************************************
        // PREPARING DATA TO CONSUME PAGOSNET WEB SERVICE...
        DatosPlanilla datosPlanilla = new DatosPlanilla();
        datosPlanilla.setTransaccion("A"); //ALWAYS 'A' TO REQUEST NEW PROCESS
        datosPlanilla.setNombre(ePago.getNombreFactura());
        datosPlanilla.setNitCICliente(ePago.getNitFactura());
        datosPlanilla.setCodigoCliente(pago.getUsername());
        datosPlanilla.setCodigoEmpresa(Integer.parseInt(companyCodeWs));

        Calendar c = Calendar.getInstance();
        datosPlanilla.setFecha(toIntDate(c.getTime()));
        datosPlanilla.setHora(toIntDate(c.getTime()));

        datosPlanilla.setCorreoElectronico(pago.getEmailCliente());
        datosPlanilla.setMoneda(ParametrosPago.pagosnetCurrency);
        datosPlanilla.setCodigoRecaudacion(pago.getCodRecaudacion());
        datosPlanilla.setDescripcionRecaudacion(detail);

        c.add(Calendar.MONTH, expirationMonths);
        datosPlanilla.setFechaVencimiento(toIntDate(c.getTime()));
        datosPlanilla.setHoraVencimiento(toIntTime(c.getTime()));
        datosPlanilla.setCodigoProducto(ParametrosPago.pagosnetProduct);
        datosPlanilla.setPlanillas(dPago);
        // *********************************************************************************

        System.out.println(comelecSoapPort.toString());
        RespPlanilla respPlanilla = comelecSoapPort.cmeRegistroPlan(datosPlanilla, ParametrosPago.pagosnetUsername);
        System.out.println("***************************************************************");
        System.out.println("Codigo de Respuesta/Error:" + CodeErrorType.fromValue(respPlanilla.getCodError()));
        System.out.println("Descripcion:" + respPlanilla.getDescripcionError());
        System.out.println("IdTransaccion:" + respPlanilla.getIdTransaccion());
        System.out.println("***************************************************************");

        String transaccion = respPlanilla.getIdTransaccion();
        long orderId = transaccion == null || transaccion.isEmpty() ? 0L : Long.parseLong(transaccion);
        requestDto.setOrderId(orderId);
        invoiceService.save(requestDto, detail, 2);
        return respPlanilla.getIdTransaccion();
    }

    private void registerAdabas(String matricula, boolean recharge, String monto,
                                String username, TMPaymentRequestDto payment) {
        ResponseDto resp = new ResponseDto();
        resp.setArancel(String.valueOf(payment.getAmount()));
        resp.setDescTramite("");

        String serviceRecharge = "07040205";
        String description = recharge ? "Recarga" : "Pago";
        description += " de saldo Ventanilla Virtual";
        AdabasD.transaction(resp, username, matricula, serviceRecharge, description);

        description = recharge ? "Recarga" : "Pago";
        description += " PagosNet";
        registrarPago(username, matricula, description, "91", monto, serviceRecharge, description);
    }

    private void registrarPago(String username, String matricula, String pago, String orderId, String monto, String servicio, String descripcion) {
        preciosService.registrarPago(username, matricula, pago, orderId, monto, servicio, descripcion);
    }

    private static int toIntDate(Date date) {
        return toInt(date, "yyyyMMdd");
    }

    private static int toIntTime(Date date) {
        return toInt(date, "HHmmss");
    }

    private static int toInt(Date date, String dateFormat) {
        int result = 0;
        String strDate = format(date, dateFormat);
        if (strDate != null) {
            result = Integer.parseInt(strDate);
        }
        return result;
    }

    private static String format(final Date date, final String dateFormat) {
        return date == null ? null : DateFormatUtils.format(date, dateFormat);
    }

    private String buildDetail(boolean recharge) {
        StringBuilder strDetail = new StringBuilder();
        strDetail.append(recharge ? "Recarga" : "Pago");
        strDetail.append(" PagosNet");
        return strDetail.toString();
    }
}
