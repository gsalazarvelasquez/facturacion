package ch.swissbytes.fundempresa.billing.services.internal;

import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.billing.data.dto.DebitoResponseDto;
import ch.swissbytes.fundempresa.billing.data.dto.PaymentRequestDto;
import ch.swissbytes.fundempresa.billing.exception.AdabasDRechargeException;
import ch.swissbytes.fundempresa.billing.exception.ParametersException;
import ch.swissbytes.fundempresa.billing.exception.PasswordException;
import ch.swissbytes.fundempresa.billing.services.DebitoService;
import com.consist.seguridad.common.BTEncriptacion;

public class DebitoServiceImpl implements DebitoService {

    public DebitoServiceImpl() {
    }

    @Override
    public DebitoResponseDto pay(PaymentRequestDto requestDto) throws PasswordException, ParametersException, AdabasDRechargeException {
        DebitoResponseDto response = new DebitoResponseDto();
        if(validate(requestDto, true)){
            String username = requestDto.getUserName().toUpperCase();
            String password = requestDto.getPassword().toUpperCase();

            UserProfileDto userProfileDto = AdabasD.profile(username);

            BTEncriptacion encriptacion = new BTEncriptacion();
            String encrypted = "";

            try {
                encrypted = encriptacion.encriptarClave(password,
                        username, userProfileDto.getProfile().toUpperCase());
            } catch (Exception e) {}

            if(userProfileDto.getPassword().equals(encrypted)){
                String monto = requestDto.getMonto();
                boolean processed = AdabasD.debitar(username, monto);
                if(processed){
                    response.setPayCode("0");
                    response.setCodigoError(String.valueOf(0));
                    response.setMensajeError("Procesado correctamente");
                }else{
                    throw new AdabasDRechargeException();
                }
            }else {
                throw new PasswordException();
            }
        }else{
            throw new ParametersException();
        }
        return response;
    }

    @Override
    public DebitoResponseDto add(PaymentRequestDto requestDto) throws AdabasDRechargeException, ParametersException {
        DebitoResponseDto response = new DebitoResponseDto();
        if(validate(requestDto, false)){
            String username = requestDto.getUserName();
            String monto = requestDto.getMonto();
            boolean processed = AdabasD.abonar(username, monto);
            if(processed){
                response.setCodigoError(String.valueOf(0));
                response.setMensajeError("Procesado correctamente");
            }else{
                throw new AdabasDRechargeException();
            }
        }else{
            throw new ParametersException();
        }
        return response;
    }

    private boolean validate(PaymentRequestDto requestDto, boolean password) {
        if(requestDto == null ||
                requestDto.getMonto() == null ||
//                (password && requestDto.getPassword() == null) ||
                requestDto.getUserName() == null)
            return false;
        return true;
    }
}
