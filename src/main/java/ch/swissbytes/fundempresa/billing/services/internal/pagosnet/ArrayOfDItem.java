
package ch.swissbytes.fundempresa.billing.services.internal.pagosnet;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfDItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfDItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DItem" type="{http://www.openuri.org/}DItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDItem", propOrder = {
    "dItem"
})
public class ArrayOfDItem {

    @XmlElement(name = "DItem", nillable = true)
    protected List<DItem> dItem;

    /**
     * Gets the value of the dItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DItem }
     * 
     * 
     */
    public List<DItem> getDItem() {
        if (dItem == null) {
            dItem = new ArrayList<DItem>();
        }
        return this.dItem;
    }

}
