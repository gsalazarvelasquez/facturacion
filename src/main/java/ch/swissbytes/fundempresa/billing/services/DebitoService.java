package ch.swissbytes.fundempresa.billing.services;

import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.billing.data.dto.DebitoResponseDto;
import ch.swissbytes.fundempresa.billing.data.dto.PaymentRequestDto;
import ch.swissbytes.fundempresa.billing.exception.AdabasDRechargeException;
import ch.swissbytes.fundempresa.billing.exception.ParametersException;
import ch.swissbytes.fundempresa.billing.exception.PasswordException;

import javax.ejb.Local;

/**
 * Created by miguel on 1/26/16.
 */
@Local
public interface DebitoService {

      DebitoResponseDto pay(PaymentRequestDto requestDto) throws PasswordException, ParametersException, AdabasDRechargeException;

      DebitoResponseDto add(PaymentRequestDto requestDto) throws AdabasDRechargeException, ParametersException;
}
