
package ch.swissbytes.fundempresa.billing.services.internal.pagosnet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cmeRegistroPlanResult" type="{http://www.openuri.org/}RespPlanilla" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cmeRegistroPlanResult"
})
@XmlRootElement(name = "cmeRegistroPlanResponse")
public class CmeRegistroPlanResponse {

    protected RespPlanilla cmeRegistroPlanResult;

    /**
     * Gets the value of the cmeRegistroPlanResult property.
     * 
     * @return
     *     possible object is
     *     {@link RespPlanilla }
     *     
     */
    public RespPlanilla getCmeRegistroPlanResult() {
        return cmeRegistroPlanResult;
    }

    /**
     * Sets the value of the cmeRegistroPlanResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RespPlanilla }
     *     
     */
    public void setCmeRegistroPlanResult(RespPlanilla value) {
        this.cmeRegistroPlanResult = value;
    }

}
