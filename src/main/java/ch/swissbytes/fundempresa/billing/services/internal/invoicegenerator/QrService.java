package ch.swissbytes.fundempresa.billing.services.internal.invoicegenerator;

import ch.swissbytes.fundempresa.billing.data.model.Facturas;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class QrService implements Serializable {

    private static final String SEPARATOR_INVOICE = "|";

    public InputStream getQR(final Facturas invoiceDoc) {
        BufferedImage buffer = getQrImage(invoiceDoc);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        InputStream is = null;
        try {
            ImageIO.write(buffer, "png", os);
            is = new ByteArrayInputStream(os.toByteArray());
        } catch (IOException e) {
        }
        return is;
    }

    private BufferedImage getQrImage(final Facturas invoiceDoc) {
        return new QrScanner().encodeTextToBuffer(generateInformation(invoiceDoc));
    }

    private String generateInformation(final Facturas invoiceDoc) {
        String companyNit = invoiceDoc.getEmisorNit();
        long documentNumber = invoiceDoc.getNumeroFactura();
        long authorizationNumber = Long.parseLong(invoiceDoc.getNumeroAutorizacion());
        Date date = new Date(invoiceDoc.getFecha().getTime());
        BigDecimal total = new BigDecimal(invoiceDoc.getMontoFactura());
        BigDecimal totalFiscalCredit = new BigDecimal(invoiceDoc.getMontoCreditoFiscal());
        String controlCode = invoiceDoc.getCodigoControl();
        String customerNit = invoiceDoc.getClienteNit();
        BigDecimal totalICE = BigDecimal.ZERO;
        BigDecimal totalRateZero = BigDecimal.ZERO;
        BigDecimal totalNotFiscalCredit = total.subtract(totalFiscalCredit);
        BigDecimal totalDiscount = BigDecimal.ZERO;
        return generateQrData(companyNit, documentNumber, authorizationNumber, date, total, totalFiscalCredit, controlCode, customerNit, totalICE, totalRateZero, totalNotFiscalCredit, totalDiscount);
    }

    private String generateQrData(String companyNit, long documentNumber, long authorizationNumber, Date date, BigDecimal total, BigDecimal totalFiscalCredit, String controlCode, String customerNit,
                                  BigDecimal totalICE, BigDecimal totalRateZero, BigDecimal totalNotFiscalCredit, BigDecimal totalDiscount) {
        StringBuilder sb = new StringBuilder();
        sb.append(companyNit);
        sb.append(SEPARATOR_INVOICE);
        sb.append(documentNumber);
        sb.append(SEPARATOR_INVOICE);
        sb.append(authorizationNumber);
        sb.append(SEPARATOR_INVOICE);
        sb.append(new SimpleDateFormat("dd/MM/yyyy").format(date));
        sb.append(SEPARATOR_INVOICE);
        sb.append(NumberUtils.format(total, NumberUtils.FORMAT_DECIMAL_DEFAULT));
        sb.append(SEPARATOR_INVOICE);
        sb.append(NumberUtils.format(totalFiscalCredit, NumberUtils.FORMAT_DECIMAL_DEFAULT));
        sb.append(SEPARATOR_INVOICE);
        sb.append(controlCode);
        sb.append(SEPARATOR_INVOICE);
        sb.append(customerNit);
        sb.append(SEPARATOR_INVOICE);
        sb.append(totalICE);
        sb.append(SEPARATOR_INVOICE);
        sb.append(totalRateZero);
        sb.append(SEPARATOR_INVOICE);
        sb.append(totalNotFiscalCredit);
        sb.append(SEPARATOR_INVOICE);
        sb.append(totalDiscount);
        return sb.toString();
    }

}
