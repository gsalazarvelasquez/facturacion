package ch.swissbytes.fundempresa.billing.services.internal;

import ch.swissbytes.fundempresa.adabasd.AdabasD;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.billing.data.dto.*;
import ch.swissbytes.fundempresa.billing.data.model.TwocheckoutConfig;
import ch.swissbytes.fundempresa.billing.data.model.TwocheckoutTransacciones;
import ch.swissbytes.fundempresa.billing.data.repository.TwocheckoutRepositoryImpl;
import ch.swissbytes.fundempresa.billing.services.DebitoService;
import ch.swissbytes.fundempresa.billing.services.InvoiceService;
import ch.swissbytes.fundempresa.billing.services.TwocheckoutService;
import ch.swissbytes.fundempresa.ventanilla.data.dto.ResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.model.Fotocopia;
import ch.swissbytes.fundempresa.ventanilla.data.model.Homonimia;
import ch.swissbytes.fundempresa.ventanilla.services.*;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by jorgeburgos on 1/30/16.
 */
public class TwocheckoutServiceImpl implements TwocheckoutService {

    @Inject
    public PreciosService preciosService;

    @Inject
    private DebitoService debitoService;

    @Inject
    private InvoiceService invoiceService;

    @Inject
    private TwocheckoutRepositoryImpl entityRepositoryImpl;

    @Inject
    private HomonimiaService homonimiaService;

    @Inject
    private FotocopiasService fotocopiasService;

    @Inject
    private VentaBDListadoService ventaBDService;

    @Inject
    private CertificadosService certificadosService;

    public TwocheckoutServiceImpl() {
    }

    @Override
    public TwocheckoutTransacciones pay(TwocheckoutRequestDto requestDto){
        TwocheckoutTransacciones transaction = entityRepositoryImpl.getById(TwocheckoutTransacciones.class, requestDto.getOrderId()).get();
        transaction.setResponseMessage(requestDto.getMessage());
        entityRepositoryImpl.update(transaction);
        updateTransaction(transaction);

        requestDto.setUserName(transaction.getRequestUsername());
        requestDto.setMonto(transaction.getRequestMonto());
        requestDto.setRazonSocial(transaction.getRequestRazonsocial());
        requestDto.setNit(transaction.getRequestNit());
        boolean recharge = transaction.getRequestTipotramite().equals("FACTURAS");
        registerAdabas(requestDto, recharge);
        if (recharge) {
            try{
                debitoService.add(requestDto);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        String motivo = "";
        if(requestDto.getHomonimiaRequestDto() != null)
            motivo = ": Control de Homonimia";
        if(requestDto.getFotocopiasRequestDto() != null)
            motivo = ": Fotocopia de Documentos";
        if(requestDto.getCertificadosRequestDto() != null)
            motivo = ": Certificados";
        String detail = (recharge ? "Recarga" : "Pago") + " Ventanilla Virtual" + motivo;
        try{
            invoiceService.generate(requestDto, detail);
        }catch (Exception e){
            e.printStackTrace();
        }
        return transaction;
    }

    @Override
    public TwocheckoutConfig config() throws Exception {
        return entityRepositoryImpl.getConfig();
    }

    @Override
    public TwocheckoutTransacciones save(TwocheckoutRequestDto requestDto) throws Exception {
        UserProfileDto userProfileDto = AdabasD.profile(requestDto.getUserName());
        requestDto.setSolicitante(userProfileDto.getFullName());
        requestDto.setCi(userProfileDto.getCi());
        return saveTransaction(requestDto);
    }

    private TwocheckoutTransacciones saveTransaction(TwocheckoutRequestDto requestDto) throws Exception {
        String pkTramite = getPkTramite(requestDto);

        TwocheckoutTransacciones entity = new TwocheckoutTransacciones();
        entity.setRequestUsername(requestDto.getUserName());
        entity.setRequestNombre(requestDto.getSolicitante());
        entity.setRequestNumerodocumento(requestDto.getNit());
        entity.setRequestRazonsocial(requestDto.getRazonSocial());
        entity.setRequestNit(requestDto.getNit());
        entity.setRequestMonto(requestDto.getMonto());
        entity.setRequestTipotramite(requestDto.getTipo());
        entity.setRequestFechahora(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        entity.setRequestPktramite(pkTramite);
        entityRepositoryImpl.saveAndFlush(entity);
        entity.setRequestOrderid(String.valueOf(entity.getId()));
        return entity;
    }

    private void updateTransaction(TwocheckoutTransacciones transaction) {
        String tipo = transaction.getRequestTipotramite();
        if(tipo.equalsIgnoreCase("HOMONIMIA")){
            //TODO: UPDATE HOMONIMIA TRANSACTION AS PAID
            String pkTramite = transaction.getRequestPktramite();
            System.out.println("Buscando homonimia: " + pkTramite);
            Homonimia homonimia = homonimiaService.get(pkTramite);
            if(homonimia != null){
                homonimia.setPayCode("0");
                homonimia.setEstadoTramite(TramiteResponseDto.EstadoTramite.PAGADO.name());
                homonimiaService.update(homonimia);
            }
            return;
        }
        if(tipo.equalsIgnoreCase("FOTOCOPIA")) {
            //TODO: UPDATE FOTOCOPIAS TRANSACTION AS PAID
            String pkTramite = transaction.getRequestPktramite();
            Fotocopia fotocopia = fotocopiasService.get(pkTramite);
            if(fotocopia != null){
                fotocopia.setPayCode("0");
                fotocopia.setEstadoTramite(TramiteResponseDto.EstadoTramite.PAGADO.name());
                fotocopiasService.update(fotocopia);
            }
            return;
        }
    }

    private void registerAdabas(TwocheckoutRequestDto requestDto, boolean recharge) {
        String matricula = "";
        String username = requestDto.getUserName();
        ResponseDto resp = new ResponseDto();
        resp.setArancel(String.valueOf(requestDto.getMonto()));
        resp.setDescTramite("");

        String serviceRecharge = "07040205";
        String description = recharge ? "Recarga" : "Pago";
        AdabasD.transaction(resp, username, matricula, serviceRecharge, description);

        description = recharge ? "Recarga" : "Pago";
        registrarPago(username, matricula, description,
                "91", requestDto.getMonto(), serviceRecharge, description);
    }

    private String getPkTramite(TwocheckoutRequestDto requestDto) {
        String tipo = requestDto.getTipo();

        if(tipo.equalsIgnoreCase("HOMONIMIA")){
            Long pkTramite = homonimiaService.saveDraft(requestDto.getHomonimiaRequestDto());
            return String.valueOf(pkTramite);
        }
        if(tipo.equalsIgnoreCase("FOTOCOPIA")){
            Long pkTramite = fotocopiasService.saveDraft(requestDto.getFotocopiasRequestDto());
            return String.valueOf(pkTramite);
        }
        if(tipo.equalsIgnoreCase("LISTADOEMPRESAS")){
//            Long pkTramite = ventaBDService.saveDraft(requestDto.getTramiteRequestDto());
//            return String.valueOf(pkTramite);
        }
        if(tipo.equalsIgnoreCase("ESTADISTICABASICA")){
//            Long pkTramite = ventaBDService.saveDraft(requestDto.getTramiteRequestDto());
//            return String.valueOf(pkTramite);
        }
        if(tipo.equalsIgnoreCase("CRUCEVARIABLES")){
//            Long pkTramite = ventaBDService.saveDraft(requestDto.getTramiteRequestDto());
//            return String.valueOf(pkTramite);
        }
        if(tipo.equalsIgnoreCase("CERTIFICADOSESPECIALES")){
//            Long pkTramite = certificadosService.saveDraft(requestDto.getTramiteRequestDto());
//            return String.valueOf(pkTramite);
        }
        if(tipo.equalsIgnoreCase("CERTIFICADOSENLINEA")){
//            Long pkTramite = certificadosService.saveDraft(requestDto.getTramiteRequestDto());
//            return String.valueOf(pkTramite);
        }
        return "";
    }

    private void registrarPago(String username, String matricula, String pago, String orderId, String monto, String servicio, String descripcion) {
        preciosService.registrarPago(username, matricula, pago, orderId, monto, servicio, descripcion);
    }
}