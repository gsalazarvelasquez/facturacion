package ch.swissbytes.fundempresa.billing.exception;

/**
 * Created by daniel on 31/03/2016.
 */
public class ParametersException extends Exception {

    public ParametersException(){
        super("Parametros incorrectos.");
    }

    public ParametersException(Throwable throwable){
        super(throwable);
    }
}
