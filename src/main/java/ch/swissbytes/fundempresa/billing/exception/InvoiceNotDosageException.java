package ch.swissbytes.fundempresa.billing.exception;

import javax.ejb.ApplicationException;

/**
 * Created by jorgeburgos on 2/1/16.
 */
@ApplicationException
public class InvoiceNotDosageException extends RuntimeException {
}
