package ch.swissbytes.fundempresa.billing.exception;

/**
 * Created by daniel on 31/03/2016.
 */
public class AdabasDRechargeException extends Exception {

    public AdabasDRechargeException(){
        super("No se pudo procesar su transaccion.");
    }

    public AdabasDRechargeException(Throwable throwable){
        super(throwable);
    }
}
