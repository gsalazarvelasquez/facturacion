package ch.swissbytes.fundempresa.billing.exception;

/**
 * Created by daniel on 31/03/2016.
 */
public class DosageException extends Exception {

    public DosageException(){
        super("No se pudo generar su factura.");
    }

    public DosageException(String message){
        super(message);
    }

    public DosageException(Throwable throwable){
        super(throwable);
    }
}
