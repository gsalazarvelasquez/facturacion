package ch.swissbytes.fundempresa.billing.rest.tigomoney;

import ch.swissbytes.fundempresa.billing.data.dto.TigoMoneyRequestDto;
import ch.swissbytes.fundempresa.billing.data.dto.TigoMoneyResponseDto;
import ch.swissbytes.fundempresa.billing.services.DebitoService;
import ch.swissbytes.fundempresa.billing.services.TigoMoneyService;
import ch.swissbytes.fundempresa.ventanilla.data.dto.DocumentoFotocopiasDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.FotocopiasRequestDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.FotocopiasResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.JsonResult;
import com.google.gson.JsonObject;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by daniel on 08-03-16.
 */

@Stateless
@Path("/billing/tigomoney")
@Produces(MediaType.APPLICATION_JSON)
public class TigomoneyResource {

    @Inject
    private TigoMoneyService tigoMoneyService;

    @POST
    @Path("/pagar")
    public JsonResult pagar(TigoMoneyRequestDto requestDto){
        JsonResult result = new JsonResult();
        try{
            TigoMoneyResponseDto response = tigoMoneyService.pay(requestDto, false);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;

    }

    @POST
    @Path("/recargar")
    public JsonResult recargar(TigoMoneyRequestDto requestDto){
        JsonResult result = new JsonResult();
        try{
            TigoMoneyResponseDto response = tigoMoneyService.pay(requestDto, true);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;

    }
}
