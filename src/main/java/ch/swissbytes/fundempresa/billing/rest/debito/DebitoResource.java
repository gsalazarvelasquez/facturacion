package ch.swissbytes.fundempresa.billing.rest.debito;

import ch.swissbytes.fundempresa.billing.data.dto.DebitoResponseDto;
import ch.swissbytes.fundempresa.billing.data.dto.PaymentRequestDto;
import ch.swissbytes.fundempresa.billing.services.DebitoService;
import ch.swissbytes.fundempresa.ventanilla.data.dto.JsonResult;
import com.google.gson.JsonObject;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Random;

/**
 * Created by daniel on 08-03-16.
 */

@Stateless
@Path("/billing/debito")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DebitoResource {

    @Inject
    private DebitoService debitoService;

    @GET
    @Path("/processDebit")
    public String processDebit(){
        Random rand = new Random();
        int  payCode = rand.nextInt(9999) + 1;
        JsonObject response = new JsonObject();
        response.addProperty("success", true);
        response.addProperty("payCode", payCode);
        response.addProperty("message", "succefully");

        return response.toString();
    }

    @POST
    @Path("/pagar")
    public JsonResult pagar(PaymentRequestDto requestDto){
        JsonResult result = new JsonResult();
        try{
            DebitoResponseDto response = debitoService.pay(requestDto);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }
}
