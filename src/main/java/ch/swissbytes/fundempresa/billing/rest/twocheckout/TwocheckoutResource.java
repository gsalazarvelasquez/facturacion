package ch.swissbytes.fundempresa.billing.rest.twocheckout;

import ch.swissbytes.fundempresa.billing.data.dto.TwocheckoutRequestDto;
import ch.swissbytes.fundempresa.billing.data.dto.TwocheckoutResponseDto;
import ch.swissbytes.fundempresa.billing.data.model.TwocheckoutConfig;
import ch.swissbytes.fundempresa.billing.data.model.TwocheckoutTransacciones;
import ch.swissbytes.fundempresa.billing.exception.AdabasDRechargeException;
import ch.swissbytes.fundempresa.billing.exception.DosageException;
import ch.swissbytes.fundempresa.billing.exception.InfoAbsentException;
import ch.swissbytes.fundempresa.billing.exception.ParametersException;
import ch.swissbytes.fundempresa.billing.services.TwocheckoutService;
import ch.swissbytes.fundempresa.ventanilla.data.dto.JsonResult;
import com.twocheckout.TwocheckoutReturn;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel on 08-03-16.
 */

@Stateless
@Path("/billing/twocheckout")
@Produces(MediaType.APPLICATION_JSON)
public class TwocheckoutResource {

    @Inject
    private TwocheckoutService twocheckoutService;

    @POST
    @Path("/response")
    public Response response(String param){
        java.net.URI location = null;
        try {
            TwocheckoutConfig config = twocheckoutService.config();
            Map<String, String> values = parse(param);
            String twocheckouttipotramite = values.get("twocheckouttipotramite");
            String twocheckoutorderid = values.get("twocheckoutorderid");
            String order_number = values.get("order_number");
            String total = values.get("total");
            String key = values.get("key");
            String montoBs = values.get("twocheckoutmonto");
            String clienteNit = values.get("twocheckoutnit");
            String clienteRazon = values.get("twocheckoutrazon");
            String username = values.get("twocheckoutusername");
            order_number = config.getDemo().equalsIgnoreCase("Y") ? "1" : order_number;

            if(!validateKey(key, config.getSecretWord(), config.getSid(), order_number, total)){
                System.out.println("ERROR VALIDATING 2CHECKOUT RESPONSE");
                location = new java.net.URI(config.getUrlError());
            }
            else{
                System.out.println("VALID RESPONSE FROM 2CHECKOUT");
                TwocheckoutRequestDto requestDto = new TwocheckoutRequestDto();
                requestDto.setTipo("4");
                requestDto.setUserName(username);
                requestDto.setMonto(montoBs);
                requestDto.setNit(clienteNit);
                requestDto.setRazonSocial(clienteRazon);
                requestDto.setMessage("2Checkout completed: " + order_number);
                requestDto.setOrderId(Long.parseLong(twocheckoutorderid));
                TwocheckoutTransacciones tramite = twocheckoutService.pay(requestDto);

                if(tramite != null){
                    String params = prepare(tramite);
                    URI redirectLocation = new URI (config.getUrlReturn() + params);
                    return Response.seeOther(redirectLocation).build();
                }
                location = new java.net.URI(config.getUrlError());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.temporaryRedirect(location).build();
    }

    private boolean validateKey(String key, String secretWord, String sid, String orderId, String total) throws Exception {
        HashMap params = new HashMap();
        params.put("sid", sid);
        params.put("total", total);
        params.put("order_number", orderId);
        params.put("key", key);
        return TwocheckoutReturn.check(params, secretWord);
    }

    private Map<String, String> parse(String param) {
        Map<String, String> response = new HashMap<String, String>();
        String[] split = param.split("&");
        for(String splitted : split){
            String[] item = splitted.replace("&", "").split("=");
            if(item.length > 1)
                response.put(item[0], item[1]);
        }
        return response;
    }

    @POST
    @Path("/saveDraft")
    public JsonResult save(TwocheckoutRequestDto requestDto){
        JsonResult result = new JsonResult();
        try{
            TwocheckoutTransacciones response = twocheckoutService.save(requestDto);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @GET
    @Path("/config")
    public JsonResult config(){
        JsonResult result = new JsonResult();
        try {
            TwocheckoutConfig response = twocheckoutService.config();
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    private String prepare(TwocheckoutTransacciones transaccion) {
        String tipo = transaccion.getRequestTipotramite().toLowerCase();
        String pkTramite = transaccion.getRequestPktramite();
        pkTramite = pkTramite == null || pkTramite.isEmpty() ? "" : ("/" + pkTramite + "-3-TWOCHECKOUT");
        return "/#/" + tipo + pkTramite;
    }
}
