package ch.swissbytes.fundempresa.billing.rest.invoice;

import ch.swissbytes.fundempresa.billing.data.model.Facturas;
import ch.swissbytes.fundempresa.billing.services.InvoiceService;
import ch.swissbytes.fundempresa.ventanilla.data.dto.JsonResult;
import ch.swissbytes.shared.rest.HttpCode;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.*;
import java.util.List;

/**
 * Created by jorgeburgos on 2/1/16.
 */
@Stateless
@Path("/invoice")
//@Produces(MediaType.APPLICATION_JSON)
public class InvoiceResource {

    @Inject
    public InvoiceService invoiceService;

    @GET
    @Path("/getAll")
    public JsonResult getAll() {
        JsonResult result = new JsonResult();
        try{
            List<Facturas> invoiceList = invoiceService.findAll();
            result.setSuccess(true);
            result.setData(invoiceList);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/getPdf")
    @Produces("application/pdf")
    public Response getPdf(final Facturas requestDto) {
        StreamingOutput stream = new StreamingOutput() {
            @Override
            public void write(OutputStream os) throws IOException, WebApplicationException {
                try {
                    invoiceService.printPDF(os, requestDto);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        return Response.ok(stream).build();
    }
}
