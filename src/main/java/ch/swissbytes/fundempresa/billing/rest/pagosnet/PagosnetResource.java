package ch.swissbytes.fundempresa.billing.rest.pagosnet;

import ch.swissbytes.fundempresa.billing.services.PagosnetService;
import ch.swissbytes.fundempresa.billing.services.internal.pagosnet.PagoDto;
import com.google.gson.JsonObject;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Random;

/**
 * Created by daniel on 08-03-16.
 */

@Path("/billing/pagosnet")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
public class PagosnetResource {

    @Inject
    public PagosnetService pagosnetService;

    @POST
    @Path("/pagar")
    public JsonObject pagar(){
//    public JsonObject pagar(PagoDto pago){

        // ************** DATOS DE PRUEBA ********************
        PagoDto pago = new PagoDto();
        pago.setUsername("PREPBYTE");
        pago.setEmailCliente("alvarezh@gmail.com");
        pago.setNombreFacturar("Heidy Alvarez");
        pago.setNitFacturar("3186579016");
        pago.setMontoTotal(new BigDecimal("5.48"));
        pago.setCodRecaudacion(Calendar.getInstance() + "196");
        // ***************************************************

        JsonObject response = new JsonObject();
        try{
            String payCode = pagosnetService.pay(pago, false);
            response.addProperty("success", true);
            response.addProperty("payCode", payCode);
            response.addProperty("message", "succefully");
        }catch (Exception e){
            response.addProperty("success", false);
            response.addProperty("payCode", "");
            response.addProperty("message", "error procesando pago");
        }
        return response;
    }

    @POST
    @Path("/recargar")
    public JsonObject recargar(){
//    public JsonObject recargar(PagoDto pago){

        // ************** DATOS DE PRUEBA ********************
        PagoDto pago = new PagoDto();
        pago.setUsername("PREPBYTE");
        pago.setEmailCliente("alvarezh@gmail.com");
        pago.setNombreFacturar("Heidy Alvarez");
        pago.setNitFacturar("3186579016");
        pago.setMontoTotal(new BigDecimal("5.48"));
        pago.setCodRecaudacion(Calendar.getInstance() + "196");
        // ***************************************************

        JsonObject response = new JsonObject();
        try{
            String payCode = pagosnetService.pay(pago, true);
            response.addProperty("success", true);
            response.addProperty("payCode", payCode);
            response.addProperty("message", "succefully");
        }catch (Exception e){
            response.addProperty("success", false);
            response.addProperty("payCode", "");
            response.addProperty("message", "error procesando pago");
        }
        return response;
    }
}
