package ch.swissbytes.fundempresa.billing.rest.paypal;

import ch.swissbytes.fundempresa.billing.data.dto.PayPalRequestDto;
import ch.swissbytes.fundempresa.billing.data.dto.PayPalResponseDto;
import ch.swissbytes.fundempresa.billing.data.dto.TigoMoneyRequestDto;
import ch.swissbytes.fundempresa.billing.data.dto.TigoMoneyResponseDto;
import ch.swissbytes.fundempresa.billing.data.model.PaypalConfig;
import ch.swissbytes.fundempresa.billing.data.model.PaypalTransacciones;
import ch.swissbytes.fundempresa.billing.services.PaypalService;
import ch.swissbytes.fundempresa.billing.services.TigoMoneyService;
import ch.swissbytes.fundempresa.ventanilla.data.dto.JsonResult;
import com.google.gson.JsonObject;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by daniel on 08-03-16.
 */

@Stateless
@Path("/billing/paypal")
@Produces(MediaType.APPLICATION_JSON)
public class PaypalResource {

    @Inject
    private PaypalService paypalService;

    @POST
    @Path("/saveDraft")
    public JsonResult save(PayPalRequestDto requestDto){
        JsonResult result = new JsonResult();
        try{
            PaypalTransacciones response = paypalService.save(requestDto);
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    @POST
    @Path("/response")
    @Produces({MediaType.TEXT_HTML})
    public Response response(String param){
        java.net.URI location = null;
        try {
            PaypalConfig config = paypalService.config();
            Map<String, String> values = parse(param);
            String invoice = values.get("invoice");
            String status = values.get("payment_status");
            String txnId = values.get("txn_id");
            String verify = values.get("verify_sign");

            //TODO: ADD ANOTHER VALIDATION WITH 'verify'
            if (!status.equalsIgnoreCase("completed") || false) {
                System.out.println("ERROR IN PAYPAL RESPONSE");
                location = new java.net.URI(config.getUrlError());
            } else {
                System.out.println("VALID RESPONSE FROM PAYPAL");
                PayPalRequestDto request = new PayPalRequestDto();
                request.setOrderId(Long.parseLong(invoice));
                request.setPaymentStatus("Paypal completed: " + txnId);
                PaypalTransacciones transaction = paypalService.pay(request);

                if(transaction != null){
                    String params = prepare(transaction);
                    URI redirectLocation = new URI (config.getUrlCorrect() + params);
                    return Response.seeOther(redirectLocation).build();
                }
                location = new java.net.URI(config.getUrlError());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.temporaryRedirect(location).build();
    }

    @GET
    @Path("/config")
    public JsonResult config(){
        JsonResult result = new JsonResult();
        try {
            PaypalConfig response = paypalService.config();
            result.setSuccess(true);
            result.setData(response);
            result.setMessage("Procesado correctamente");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setData(null);
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    private Map<String, String> parse(String param) {
        Map<String, String> response = new HashMap<String, String>();
        String[] split = param.split("&");
        for(String splitted : split){
            String[] item = splitted.replace("&", "").split("=");
            if(item.length > 1)
                response.put(item[0], item[1]);
        }
        return response;
    }

    private String prepare(PaypalTransacciones transaction) {
        String tipo = transaction.getRequestTipotramite().toLowerCase();
        String pkTramite = transaction.getRequestPktramite();
        pkTramite = pkTramite == null || pkTramite.isEmpty() ? "" : ("/" + pkTramite + "-3-PAYPAL");
        return "/#/" + tipo + pkTramite;
    }
}
