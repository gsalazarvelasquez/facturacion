package ch.swissbytes.fundempresa.adabasc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* 
 * Copyright 2013 (C) <SwissBytes Ltda> 
 *  
 * Created on : Jul 1, 2013
 * @author    : Rory Sandoval <rory.sandoval@swissbytes.ch> 
 *
 */
public class EXQuery {

	private final static Logger log = LoggerFactory.getLogger(EXQuery.class);

	private String queryName;
	private Map<String, String> parameters = new HashMap<String, String>();

	/**
	 * Constructor
	 *
	 * @param queryName
	 *            Name of the query that needs to be loaded
	 */
	public EXQuery(String queryName) {
		this.queryName = queryName;
	}

	public EXQuery setParameter(String paramName, String paramValue) {
		parameters.put(paramName, paramValue);
		return this;
	}

	/**
	 * Call this method with form name set on constructor
	 * i.e.: new EXQuery(EXConfig.formSearchName()).execute();
	 * @return response for query sent
	 */
	public EXResult execute() throws Throwable {
		log.info("Executing query: " + queryName + "...");
		String xmlDocument = EXUtil.loadQuery(queryName);
		xmlDocument = EXUtil.replace(xmlDocument, parameters);
		log.info(" XML QUERY >>>: " + xmlDocument);
		return EXManager.getConnector().invokeDocument(xmlDocument);
	}

	@Override
	public String toString() {
		String strParameters = "";
		if(!parameters.isEmpty()){
			Iterator<Map.Entry<String, String>> iterator = parameters.entrySet().iterator();
			while(iterator.hasNext()){
				Map.Entry<String, String> entry = iterator.next();
				strParameters += entry.getKey() + "=" + entry.getValue();
				strParameters += iterator.hasNext() ? "|" : "";
			}
		}
		return "EXQuery{" +
				"queryName=" + queryName +
				" | parameters={" + strParameters + "}}";
	}
}
