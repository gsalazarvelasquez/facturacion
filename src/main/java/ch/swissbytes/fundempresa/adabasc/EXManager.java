package ch.swissbytes.fundempresa.adabasc;

import net.sf.jasperreports.engine.JRRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;

/* 
 * Copyright 2013 (C) <SwissBytes Ltda> 
 *  
 * Created on : Jul 1, 2013
 * @author    : Rory Sandoval <rory.sandoval@swissbytes.ch> 
 *
 */
public class EXManager {

	private static final Logger log = LoggerFactory.getLogger(EXManager.class);

	private static EXConnector connector = null;

	/**
	 * @return the connector
	 * @throws EXException
	 */
	public static EXConnector getConnector() throws EXException {
		if (connector == null) {
			try {
				connector = new EXConnector("/servicios.xmm");
				connector.init();
			} catch (Throwable e) {
				log.error("Error Trying to connect to EntireX:");
				log.debug(" -- TODO: connection info here -- ");
				e.printStackTrace();
				throw new EXException(e);
			}
		}
		return connector;
	}

}
