package ch.swissbytes.fundempresa.adabasc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.Properties;

/**
 * Get class loader assuming you want to load assets/rpc.properties located in WEB-INF/classes/assets/
 * 
 * @author Rory Sandoval <rory.sandoval@swissbytes.ch>
 * 
 */
public class EXConfig {

	final static Logger log = LoggerFactory.getLogger(EXConfig.class);

	public static final String CONFIG_FILE = "assets/rpc.properties";

	public static final String ENTIREX_USERID = "entirex.userid";
	public static final String ENTIREX_NATURALLOGON = "entirex.naturallogon";
	public static final String ENTIREX_RPCUSERID = "entirex.rpcuserid";
	public static final String ENTIREX_RPCPASSWORD = "entirex.rpcpassword";
	public static final String ENTIREX_NATURALLIBRARY = "entirex.naturallibrary";
	public static final String ENTIREX_BROKERID = "entirex.brokerid";
	public static final String ENTIREX_SERVICE = "entirex.service";
	public static final String ENTIREX_SECURITY = "entirex.security";
	public static final String ENTIREX_ENCRIPTIONLEVEL = "entirex.encryptionlevel";
	public static final String ENTIREX_PASSWORD = "entirex.password";
	public static final String ENTIREX_USERCODEPAGE = "entirex.usecodepage";

	private static Properties prop = new Properties();

	static {
		loadProperties();
	}

	private static void loadProperties() {
		URL url = findClassLoader().getResource(CONFIG_FILE);
		try {
			prop.load(url.openStream());
		} catch (Exception e) {
			log.error("Could not load configuration file: " + CONFIG_FILE);
		}
	}

	private EXConfig() {
	}

	private static ClassLoader findClassLoader() {
		ClassLoader loader = EXConfig.class.getClassLoader();
		if (loader == null)
			loader = ClassLoader.getSystemClassLoader();
		return loader;
	}

	/**
	 * Get the property value in String representation
	 * 
	 * @param key
	 * @return
	 */
	public static String getProperty(String key) {
		return prop.getProperty(key);
	}

	public static String formHomonimiaIngresar() {
		return prop.getProperty("forms.homonimia.ingresar");
	}

	public static String formPrecio() {
		return prop.getProperty("forms.precio");
	}

	public static String formHomonimiaEstado() {
		return prop.getProperty("forms.homonimia.estado");
	}

	public static String formHomonimiaReingresar() {
		return prop.getProperty("forms.homonimia.reingresar");
	}

	public static String formFotocopiasListado() {
		return prop.getProperty("forms.fotocopias.listado");
	}

	public static String formEntidadesListado() {
		return prop.getProperty("forms.entidades.listado");
	}

	public static String formEntidadMatricula() {
		return prop.getProperty("forms.entidad.matricula");
	}

	public static String formFotocopiasIngresar() {
		return prop.getProperty("forms.fotocopias.ingresar");
	}

	public static String formEspeciales() {
		return prop.getProperty("forms.certificados.especiales");
	}

	public static String formValidar() {
		return prop.getProperty("forms.certificados.validar");
	}

	public static String formEnLinea() {
		return prop.getProperty("forms.certificados.enlinea");
	}

	public static String formVentaBD() {
		return prop.getProperty("forms.ventabd.ingresar");
	}

	public static String formRegistroPago() {
		return prop.getProperty("forms.registrar.pagos");
	}

	/**
	 * Get the property key value in String representation if the value is not
	 * found then you can define a default return value
	 * 
	 * @param key
	 *            Key, to identify the property
	 * @param defValue
	 *            Default value if the property is not found.
	 * @return
	 */
	public static String getProperty(String key, String defValue) {
		String value = getProperty(key);
		if (value == null) {
			return defValue;
		}
		return value;
	}

}
