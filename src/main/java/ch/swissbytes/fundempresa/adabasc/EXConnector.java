package ch.swissbytes.fundempresa.adabasc;

import com.softwareag.entirex.aci.Broker;
import com.softwareag.entirex.aci.BrokerException;
import com.softwareag.entirex.xml.rt.XMLException;
import com.softwareag.entirex.xml.rt.XMLRPCService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main connection provider for EntireX software dependan on the Core System in
 * Fundempresa
 * 
 * Copyright 2013 (C) <SwissBytes Ltda>
 * 
 * Created on : Jun 28, 2013
 * 
 * @author : Rory Sandoval <rory.sandoval@swissbytes.ch>
 * 
 */
public class EXConnector {

	final Logger log = LoggerFactory.getLogger(EXConnector.class);

	private XMLRPCService xmlService;
	private Broker broker;
	private String xmmFile;

	public EXConnector(String xmmFile) throws Throwable {
		this.xmmFile = xmmFile;
	}

	public void init() throws Throwable {
		String userID = EXConfig.getProperty(EXConfig.ENTIREX_USERID, "xml-user");
		String brokerID = EXConfig.getProperty(EXConfig.ENTIREX_BROKERID, "localhost:1971");
		String serverAddress = EXConfig.getProperty(EXConfig.ENTIREX_SERVICE, "RPC/SRV1/CALLNAT");
		String useCodePage = EXConfig.getProperty(EXConfig.ENTIREX_USERCODEPAGE, "false");

		// Configure the service
		xmlService = new XMLRPCService(xmmFile);
		xmlService.setBroker(new Broker(brokerID, userID));
		xmlService.setServerAddress(serverAddress);
		xmlService.useCodePage(Boolean.valueOf(useCodePage));

		// TODO: Cambiar por LOGGING
		log.info("xmm: " + xmmFile);
		log.info("brokerID: " + brokerID);
		log.info("serverAddr: " + serverAddress);

		brokerLogon();
		naturalLogon();
	}

	private void brokerLogon() throws BrokerException {
		Boolean securityEnabled = Boolean.valueOf(EXConfig.getProperty(EXConfig.ENTIREX_SECURITY, "false"));
		if (securityEnabled) {
			Integer encriptionLevel = Integer.valueOf(EXConfig.getProperty(EXConfig.ENTIREX_ENCRIPTIONLEVEL, "0"));
			xmlService.getBroker().useEntireXSecurity(encriptionLevel);
		}
		String password = EXConfig.getProperty(EXConfig.ENTIREX_PASSWORD, "");
		if (password.isEmpty()) {
			xmlService.getBroker().logon();
		}
		xmlService.getBroker().logon(password);
	}

	private void naturalLogon() throws Exception {
		Boolean naturalLogonEnabled = Boolean.valueOf(EXConfig.getProperty(EXConfig.ENTIREX_NATURALLOGON, "false"));
		if (naturalLogonEnabled) {
			String rpcUser = EXConfig.getProperty(EXConfig.ENTIREX_RPCUSERID, EXConfig.getProperty(EXConfig.ENTIREX_RPCUSERID, "xml-user"));
			String rpcPassword = EXConfig.getProperty(EXConfig.ENTIREX_RPCPASSWORD, EXConfig.getProperty(EXConfig.ENTIREX_PASSWORD, ""));
			String naturalLibrary = EXConfig.getProperty(EXConfig.ENTIREX_NATURALLIBRARY, "");
			if (!naturalLibrary.isEmpty()) {
				this.xmlService.setLibraryName(naturalLibrary);
			}
			this.xmlService.setRPCUserId(rpcUser);
			this.xmlService.setRPCPassword(rpcPassword);
			this.xmlService.setNaturalLogon(true);
		}
	}

	public void close() {
		if (this.broker != null)
			try {
				this.broker.logoff();
			} catch (BrokerException bE) {
				bE.printStackTrace();
			}
	}

	public EXResult invokeDocument(String xmlDocument) throws Throwable {
		EXResult result = EXResult.EMPTY;
		if (this.xmlService != null) {
			try {				
				ensureConnected();
				System.out.println("XML QUERY >>>:" + xmlDocument);
				String xml = this.xmlService.invokeXML(xmlDocument);
				System.out.println("XML RESULT >>>:" + xml);
				result = EXResult.create(xml, 0); //SUCCESSFUL RESPONSE
				log.debug(" XML Result from Call: " + xml);
			} catch (BrokerException bE) {
				result.setErrorCode(bE.getErrorCode()); //BROKER EXCEPTION
				log.error("BrokerException during invokeXML:", bE);
				bE.printStackTrace();
				throw bE;
			} catch (XMLException xmlE) {
				result.setErrorCode(xmlE.getErrorCode()); //XML EXCEPTION
				log.error("XMLException during invokeXML", xmlE);
				xmlE.printStackTrace();
				throw xmlE;
			} catch (Exception ee) {
				result.setErrorCode(-1); //EXCEPTION
				log.error("General Error on EntireX call: ", ee);
				ee.printStackTrace();
				throw ee;
			}
		} else {
			result.setErrorCode(-2); //NULL INSTANCE
			log.error("Sorry, no XMLRPCService object available!");			
		}
		return result;
	}

	/**
	 * @throws Throwable 
	 * 
	 */
	private void ensureConnected() throws Throwable {
		try {
			this.xmlService.ping();
		} catch (BrokerException e) {
			log.error("Not Connected/ Trying to connect again");
			init();			
		}
		
	}

	/**
	 * @return the xmlService
	 */
	public XMLRPCService getXmlService() {
		return xmlService;
	}

	
	

}
