package ch.swissbytes.fundempresa.adabasc;

/**
 * 
 * Copyright 2013 (C) <SwissBytes Ltda>
 * 
 * Created on : Jun 28, 2013
 * 
 * @author : Rory Sandoval <rory.sandoval@swissbytes.ch>
 * 
 */
public class EXResult {
	public static final EXResult EMPTY = new EXResult("");
	private String xmlResult;
	private int errorCode;

	private EXResult(String xmlResult) {
		this.xmlResult = xmlResult;
	}

	private EXResult(String xmlResult, int errorCode) {
		this.xmlResult = xmlResult;
		this.errorCode = errorCode;
	}

	public static EXResult create(String xmlResult) {
		return new EXResult(xmlResult);
	}

	public static EXResult create(String xmlResult, int errorCode) {
		return new EXResult(xmlResult, errorCode);
	}

	public static boolean isEmpty(EXResult exResult) {
		return exResult.equals(exResult);
	}

	public boolean equals(EXResult obj) {
		return this.xmlResult.equals(obj.xmlResult);
	}

	/**
	 * @return the xmlResult
	 */
	public String getXmlResult() {
		return xmlResult;
	}

	/**
	 * @param xmlResult
	 *            the xmlResult to set
	 */
	public void setXmlResult(String xmlResult) {
		this.xmlResult = xmlResult;
	}

	@Override
	public String toString() {
		return this.xmlResult;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
}
