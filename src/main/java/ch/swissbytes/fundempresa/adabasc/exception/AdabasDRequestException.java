package ch.swissbytes.fundempresa.adabasc.exception;

/**
 * Created by daniel on 31/03/2016.
 */
public class AdabasDRequestException extends Exception {

    public AdabasDRequestException(){
        super("La solicitud al servidor ADABAS-D tuvo un error");
    }

    public AdabasDRequestException(Throwable throwable){
        super(throwable);
    }
}
