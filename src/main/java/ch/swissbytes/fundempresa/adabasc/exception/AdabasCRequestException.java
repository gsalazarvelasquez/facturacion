package ch.swissbytes.fundempresa.adabasc.exception;

/**
 * Created by daniel on 31/03/2016.
 */
public class AdabasCRequestException extends Exception {

    public AdabasCRequestException(){
        super("La solicitud al servidor ADABAS-C tuvo un error");
    }

    public AdabasCRequestException(Throwable throwable){
        super(throwable);
    }
}
