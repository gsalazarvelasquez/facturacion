package ch.swissbytes.fundempresa.adabasc;

/* 
 * Copyright 2013 (C) <SwissBytes Ltda> 
 *  
 * Created on : Jul 1, 2013
 * @author    : Rory Sandoval <rory.sandoval@swissbytes.ch> 
 *
 */
public class EXException extends Exception {

	/**
	 * serial id
	 */
	private static final long serialVersionUID = -784108592984599896L;

	public EXException(String message) {
		super(message);
	}

	public EXException(String message, Throwable cause) {
		super(message, cause);
	}

	public EXException(Throwable cause) {
		super(cause);
	}

}
