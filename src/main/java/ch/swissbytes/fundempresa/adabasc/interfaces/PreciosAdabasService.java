package ch.swissbytes.fundempresa.adabasc.interfaces;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.ventanilla.data.dto.CertificadosRequestDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.PreciosResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteResponseDto;

/**
 * Created by daniel on 31/03/2016.
 */
public interface PreciosAdabasService {

    PreciosResponseDto tramite(String matricula, String servicio) throws AdabasCRequestException;

    TramiteResponseDto registrarPago(String username, String matricula, String pago,
                                     String orderId, String monto, String servicio, String descripcion);

}
