package ch.swissbytes.fundempresa.adabasc.interfaces;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.CertificadosRequestDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.CertificadosResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.FotocopiasRequestDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteResponseDto;

/**
 * Created by daniel on 31/03/2016.
 */
public interface CertificadosAdabasService {

    CertificadosResponseDto especial(CertificadosRequestDto certificadosRequestDto, UserProfileDto userProfileDto) throws AdabasCRequestException;

    CertificadosResponseDto enlinea(CertificadosRequestDto certificadosRequestDto, UserProfileDto userProfileDto) throws AdabasCRequestException;

    CertificadosResponseDto validar(CertificadosRequestDto certificadosRequestDto) throws AdabasCRequestException;
}
