package ch.swissbytes.fundempresa.adabasc.interfaces;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.ventanilla.data.dto.VentaBDRequestAdabasDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.VentaBDResponseDto;

/**
 * Created by daniel on 31/03/2016.
 */
public interface VentaBDAdabasService {

    VentaBDResponseDto solicitar(VentaBDRequestAdabasDto ventaBdRequestDto) throws AdabasCRequestException;
}
