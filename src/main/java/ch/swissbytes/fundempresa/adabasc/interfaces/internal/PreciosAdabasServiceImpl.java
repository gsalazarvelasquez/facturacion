package ch.swissbytes.fundempresa.adabasc.interfaces.internal;

import ch.swissbytes.fundempresa.adabasc.EXConfig;
import ch.swissbytes.fundempresa.adabasc.EXQuery;
import ch.swissbytes.fundempresa.adabasc.EXResult;
import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasc.interfaces.CertificadosAdabasService;
import ch.swissbytes.fundempresa.adabasc.interfaces.PreciosAdabasService;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import com.softwareag.tamino.db.api.response.sax.helper.sodom.Document;
import com.softwareag.tamino.db.api.response.sax.helper.sodom.Element;
import com.softwareag.tamino.db.api.response.sax.helper.sodom.input.SAXBuilder;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by daniel on 31/03/2016.
 */
public class PreciosAdabasServiceImpl implements PreciosAdabasService {

    @Override
    public PreciosResponseDto tramite(String matricula, String servicio) throws AdabasCRequestException {
        PreciosResponseDto arancel = new PreciosResponseDto();
        String formPrecioHomonimia = EXConfig.formPrecio();

        String GR_SERV = servicio.substring(0,2);
        String ID_SGRU = servicio.substring(2,4);
        String TIP_REG = servicio.substring(4,6);
        String CONS_SE = servicio.substring(6,8);

        EXQuery exQuery = new EXQuery(formPrecioHomonimia);
        exQuery.setParameter("grserv", GR_SERV);
        exQuery.setParameter("idsgrupo", ID_SGRU);
        exQuery.setParameter("tiporeg", TIP_REG);
        exQuery.setParameter("consserv", CONS_SE);
        exQuery.setParameter("idmatricula", matricula);

        try {
            EXResult result = exQuery.execute();

            ByteArrayInputStream inputStream = new ByteArrayInputStream (result.getXmlResult().getBytes());
            SAXBuilder builder = new SAXBuilder(false);
            Document document = builder.build(inputStream);
            Element rootElement = document.getRootElement();
            String qtDeuda = rootElement.getChild("_QT-DEUDA").getText();
            String codError = rootElement.getChild("_NU-MSG-ERR").getText();
            String msgError = rootElement.getChild("_DS-MSG-ERR").getText();
            arancel.setCodigoError(codError);
            arancel.setMensajeError(msgError);
            arancel.setArancel(qtDeuda);

            arancel.setRequestXml(exQuery.toString());
            arancel.setResponseXml(result.getXmlResult());

            System.out.println("_NU-MSG-ERR: " + codError + " _DS-MSG-ERR: " + msgError + " _QT-DEUDA: " + qtDeuda);
        } catch (Throwable throwable) {
            throw new AdabasCRequestException(throwable);
        }
        return arancel;
    }

    @Override
    public TramiteResponseDto registrarPago(String username, String matricula, String pago, String orderId, String monto, String servicio, String descripcion){
        System.out.println(matricula + "-" + pago + "-" + orderId + "-" + monto);
        TramiteResponseDto response = new TramiteResponseDto();
        String formRegistroPago = EXConfig.formRegistroPago();

        Calendar cal = Calendar.getInstance();
        String strMesDia = new SimpleDateFormat("MMdd").format(cal.getTime());
        String fecha = new SimpleDateFormat("yyyyMMdd").format(cal.getTime());
        String hora = new SimpleDateFormat("HH:mm:ss").format(cal.getTime());

        EXQuery exQuery = new EXQuery(formRegistroPago);
        exQuery.setParameter("hora", hora);
        exQuery.setParameter("fecha", fecha);
        exQuery.setParameter("mesdia", strMesDia);
        exQuery.setParameter("numero", orderId);
        exQuery.setParameter("transaccion", orderId);
        exQuery.setParameter("tipo", pago);
        exQuery.setParameter("monto", monto);
        exQuery.setParameter("username", "99");
        exQuery.setParameter("matricula", matricula);
        exQuery.setParameter("servicio", servicio);
        exQuery.setParameter("descripcion", descripcion);

        try {
            EXResult result = exQuery.execute();
            ByteArrayInputStream inputStream = new ByteArrayInputStream (result.getXmlResult().getBytes());
            SAXBuilder builder = new SAXBuilder(false);
            Document document = builder.build(inputStream);
            Element rootElement = document.getRootElement();
            String codError = rootElement.getChild("_NU-MSG-ERR").getText();
            String msgError = rootElement.getChild("_DS-MSG-ERR").getText();
            response.setCodigoError(codError);
            response.setMensajeError(msgError);
            response.setRequestXml(exQuery.toString());
            response.setResponseXml(result.getXmlResult());
            System.out.println("_NU-MSG-ERR: " + codError + " _DS-MSG-ERR: " + msgError);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return response;
    }
}
