package ch.swissbytes.fundempresa.adabasc.interfaces.internal;

import ch.swissbytes.fundempresa.adabasc.EXConfig;
import ch.swissbytes.fundempresa.adabasc.EXQuery;
import ch.swissbytes.fundempresa.adabasc.EXResult;
import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasc.interfaces.HomonimiaAdabasService;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.HomonimiaRequestDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.HomonimiaResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.ResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteResponseDto;
import com.softwareag.tamino.db.api.response.sax.helper.sodom.Document;
import com.softwareag.tamino.db.api.response.sax.helper.sodom.Element;
import com.softwareag.tamino.db.api.response.sax.helper.sodom.input.SAXBuilder;

import java.io.ByteArrayInputStream;

/**
 * Created by daniel on 31/03/2016.
 */
public class HomonimiaAdabasServiceImpl implements HomonimiaAdabasService {

    @Override
    public TramiteResponseDto ingresarHomonimia(HomonimiaRequestDto homonimiaRequestDto, UserProfileDto userProfileDto) throws AdabasCRequestException {
        HomonimiaResponseDto homonimiaResponseDto = new HomonimiaResponseDto();

        try {
            String formIngresoHomonimia = EXConfig.formHomonimiaIngresar();
            EXQuery exQuery = getTramiteRequestParameters(homonimiaRequestDto, userProfileDto, formIngresoHomonimia);

            EXResult exResult = exQuery.execute();

            ByteArrayInputStream inputStream = new ByteArrayInputStream (exResult.getXmlResult().getBytes());

            SAXBuilder builder = new SAXBuilder(false);
            Document document = builder.build (inputStream);
            Element rootElement = document.getRootElement();
            String codError = rootElement.getChild("_NU-MSG-ERR").getText();
            String msgError = rootElement.getChild("_DS-MSG-ERR").getText();
            String codTramite = rootElement.getChild("_COD-TRAMITE").getText();
            String pinQuiosco = rootElement.getChild("_PIN-CERTIF").getText();

            homonimiaResponseDto.setCodigoError(codError);
            homonimiaResponseDto.setMensajeError(msgError);
            homonimiaResponseDto.setCodigoTramite(codTramite);
            homonimiaResponseDto.setPinQuiosco(pinQuiosco);

            homonimiaResponseDto.setRequestXml(exQuery.toString());
            homonimiaResponseDto.setResponseXml(exResult.getXmlResult());

            String strResponse = "_NU-MSG-ERR: " + codError + " _DS-MSG-ERR: " + msgError + " _COD-TRAMITE: " + codTramite + " _PIN-CERTIF: " + pinQuiosco;
            System.out.println(strResponse);
        } catch (Throwable throwable) {
            throw new AdabasCRequestException(throwable);
        }

        return homonimiaResponseDto;
    }

    @Override
    public TramiteResponseDto reingresarHomonimia(HomonimiaRequestDto homonimiaRequestDto) throws AdabasCRequestException {
        HomonimiaResponseDto homonimiaResponseDto = new HomonimiaResponseDto();

        try {
            String formReingresoHomonimia = EXConfig.formHomonimiaReingresar();
            EXQuery exQuery = getReingresoRequestParameter(homonimiaRequestDto, formReingresoHomonimia);
            EXResult exResult = exQuery.execute();

            ByteArrayInputStream inputStream = new ByteArrayInputStream (exResult.getXmlResult().getBytes());
            SAXBuilder builder = new SAXBuilder(false);
            Document document = builder.build (inputStream);
            Element rootElement = document.getRootElement();
            String codError = rootElement.getChild("_NU-MSG-ERR").getText();
            String msgError = rootElement.getChild("_DS-MSG-ERR").getText();

            homonimiaResponseDto.setCodigoError(codError);
            homonimiaResponseDto.setMensajeError(msgError);

            homonimiaResponseDto.setRequestXml(exQuery.toString());
            homonimiaResponseDto.setResponseXml(exResult.getXmlResult());

            String strResponse = "_NU-MSG-ERR: " + codError + " _DS-MSG-ERR: " + msgError;
            System.out.println(strResponse);
        } catch (Throwable throwable) {
            throw new AdabasCRequestException(throwable);
        }

        return homonimiaResponseDto;
    }

    @Override
    public HomonimiaResponseDto getEstadoHomonimia(String codigoTramite) throws AdabasCRequestException {
        HomonimiaResponseDto homonimiaResponseDto = new HomonimiaResponseDto();

        try {
            String formEstadoHomonimia = EXConfig.formHomonimiaEstado();
            EXQuery exQuery = new EXQuery(formEstadoHomonimia);
            exQuery.setParameter("codTramite", codigoTramite);

            EXResult exResult = exQuery.execute();

            ByteArrayInputStream inputStream = new ByteArrayInputStream (exResult.getXmlResult().getBytes());
            SAXBuilder builder = new SAXBuilder(false);
            Document document = builder.build(inputStream);
            Element rootElement = document.getRootElement();
            String codError = rootElement.getChild("_NU-MSG-ERR").getText();
            String msgError = rootElement.getChild("_DS-MSG-ERR").getText();
            String fechaActualizado = rootElement.getChild("_FEC-ULT-ESTADO").getText();
            String estadoHomonimia = rootElement.getChild("_CTR-ESTADO").getText();
            homonimiaResponseDto.setCodigoError(codError);
            homonimiaResponseDto.setMensajeError(msgError);
            homonimiaResponseDto.setFechaActualizacion(fechaActualizado);
            homonimiaResponseDto.setEstadoTramite(TramiteResponseDto.getEstadoTramite(Integer.parseInt(estadoHomonimia)));

            homonimiaResponseDto.setRequestXml(exQuery.toString());
            homonimiaResponseDto.setResponseXml(exResult.getXmlResult());

            String strResponse = "_NU-MSG-ERR: " + codError + " _DS-MSG-ERR: " + msgError + " _CTR-ESTADO: " + estadoHomonimia;
            System.out.println(strResponse);
        } catch (Throwable throwable) {
            throw new AdabasCRequestException(throwable);
        }

        return homonimiaResponseDto;
    }

    private EXQuery getTramiteRequestParameters(HomonimiaRequestDto homonimiaRequestDto, UserProfileDto userProfileDto, String formIngresoHomonimia) {
        EXQuery exQuery = new EXQuery(formIngresoHomonimia);

        exQuery.setParameter("solicitante", userProfileDto.getFullName());
        exQuery.setParameter("ci", userProfileDto.getCi());
        exQuery.setParameter("expci", getExpci(userProfileDto));
        exQuery.setParameter("telefono", userProfileDto.getPhone());

        exQuery.setParameter("tiposocietario", homonimiaRequestDto.getTipoSocietario().getId() + "");
//        exQuery.setParameter("tiposocietario", homonimiaRequestDto.getTipoSocietario().getValor());
        exQuery.setParameter("opcion1", homonimiaRequestDto.getOpcion1());
        exQuery.setParameter("opcion2", homonimiaRequestDto.getOpcion2());
        exQuery.setParameter("opcion3", homonimiaRequestDto.getOpcion3());
        exQuery.setParameter("precio", homonimiaRequestDto.getPayAmount());
        setParameterActividadEconomica(homonimiaRequestDto, exQuery);

        return exQuery;
    }

    private String getExpci(UserProfileDto userProfileDto) {
        return userProfileDto.getExpci() == null ? "Santa Cruz" : userProfileDto.getExpci();
    }

    private EXQuery getReingresoRequestParameter(HomonimiaRequestDto homonimiaRequestDto, String formReingresoHomonimia) {
        EXQuery exQuery = new EXQuery(formReingresoHomonimia);
        exQuery.setParameter("codTramite", homonimiaRequestDto.getCodigoTramite());
        exQuery.setParameter("opcion1", homonimiaRequestDto.getOpcion1());
        exQuery.setParameter("opcion2", homonimiaRequestDto.getOpcion2());
        exQuery.setParameter("opcion3", homonimiaRequestDto.getOpcion3());
        setParameterActividadEconomica(homonimiaRequestDto, exQuery);

        return exQuery;
    }

    private void setParameterActividadEconomica(HomonimiaRequestDto homonimiaRequestDto, EXQuery exQuery) {
        /******************* REGISTRANDO ARRAY DE ACTIVIDADES *************/
        int maximo = 75;
        int variables = 9;
        String actividad = homonimiaRequestDto.getActividadEconomica();
        actividad = actividad.length() > (maximo * variables) ? actividad.substring(0,(maximo * variables)) : actividad;
        int i = 1;
        int n = actividad.length() / maximo;
        int sobrante = actividad.length() % maximo;
        for (i = 1; i <= n; i++) {
            String act = actividad.substring((i-1)*maximo, i*maximo);
            exQuery.setParameter("actividad" + i, act);
        }
        if(sobrante > 0){
            int quedo = (i-1)*maximo;
            String act = actividad.substring(quedo, quedo + sobrante);
            exQuery.setParameter("actividad" + i, act);
            i++;
        }
        while(i <= variables){
            exQuery.setParameter("actividad" + i, "");
            i++;
        }
    }
}
