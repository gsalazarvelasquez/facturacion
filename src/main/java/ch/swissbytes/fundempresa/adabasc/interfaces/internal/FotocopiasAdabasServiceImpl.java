package ch.swissbytes.fundempresa.adabasc.interfaces.internal;

import ch.swissbytes.fundempresa.adabasc.EXConfig;
import ch.swissbytes.fundempresa.adabasc.EXQuery;
import ch.swissbytes.fundempresa.adabasc.EXResult;
import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasc.interfaces.FotocopiasAdabasService;
import ch.swissbytes.fundempresa.adabasc.interfaces.HomonimiaAdabasService;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import com.softwareag.tamino.db.api.response.sax.helper.sodom.Document;
import com.softwareag.tamino.db.api.response.sax.helper.sodom.Element;
import com.softwareag.tamino.db.api.response.sax.helper.sodom.input.SAXBuilder;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel on 31/03/2016.
 */
public class FotocopiasAdabasServiceImpl implements FotocopiasAdabasService {

    @Override
    public TramiteResponseDto ingresarFotocopia(FotocopiasRequestDto fotocopiasRequestDto) throws AdabasCRequestException {
        TramiteResponseDto tramite = new TramiteResponseDto();
        String formIngresoHomonimia = EXConfig.formFotocopiasIngresar();

        EXQuery exQuery = new EXQuery(formIngresoHomonimia);
        exQuery.setParameter("solicitante", fotocopiasRequestDto.getSolicitante());
        exQuery.setParameter("ci", fotocopiasRequestDto.getCi());
//        exQuery.setParameter("expci", fotocopiasRequestDto.getExpci());
        exQuery.setParameter("expci", getExpci(fotocopiasRequestDto.getExpci()));
        exQuery.setParameter("telefono", fotocopiasRequestDto.getTelefono());
        exQuery.setParameter("matricula", fotocopiasRequestDto.getMatricula());
//        exQuery.setParameter("monto", fotocopiasRequestDto.getMontoPagado());
        exQuery.setParameter("monto", fotocopiasRequestDto.getPayAmount());
        exQuery.setParameter("descripcion", fotocopiasRequestDto.getDescripcion());
        exQuery.setParameter("sucursal", fotocopiasRequestDto.getSucursal().getIdSucursal());
//        exQuery.setParameter("descripcion", "algo");
//        exQuery.setParameter("sucursal", "01");

        /******************* REGISTRANDO ARRAY DE DOCUMENTOS *************/
        String strDocumentos = "";
        List<DocumentoFotocopiasDto> documentoList = fotocopiasRequestDto.getDocumentos();
        if(documentoList != null && !documentoList.isEmpty()){
            for (int i = 0; i < documentoList.size(); i++) {
                DocumentoFotocopiasDto doc = documentoList.get(i);
                strDocumentos += "<VVAS04SF-IN>" +
                        "<_ID-LIBRO>" + doc.getIdLibro() + "</_ID-LIBRO>" +
                        "<_NUM-REGISTRO>" + doc.getNumRegistro() + "</_NUM-REGISTRO>" +
                        "</VVAS04SF-IN>";
            }
        }else{
            // DOCUMENTS MUST NOT BE EMPTY BUT, JUST IN CASE
            strDocumentos += "<VVAS04SF-IN>" +
                    "<_ID-LIBRO></_ID-LIBRO>" +
                    "<_NUM-REGISTRO></_NUM-REGISTRO>" +
                    "</VVAS04SF-IN>";
        }
        exQuery.setParameter("documentos", strDocumentos);
        /********************************************************************/

        try {
            EXResult result = exQuery.execute();

            ByteArrayInputStream inputStream = new ByteArrayInputStream (result.getXmlResult().getBytes());
            SAXBuilder builder = new SAXBuilder(false);
            Document document = builder.build (inputStream);
            Element rootElement = document.getRootElement();
            String codError = rootElement.getChild("_NU-MSG-ERR").getText();
            String msgError = rootElement.getChild("_DS-MSG-ERR").getText();
            String codTramite = rootElement.getChild("_COD-TRAMITE").getText();

            tramite.setCodigoError(codError);
            tramite.setMensajeError(msgError);
            tramite.setCodigoTramite(codTramite);

            tramite.setRequestXml(exQuery.toString());
            tramite.setResponseXml(result.getXmlResult());
            System.out.println("_NU-MSG-ERR: " + codError + " _DS-MSG-ERR: " + msgError + " _COD-TRAMITE: " + codTramite);
        } catch (Throwable throwable) {
            throw new AdabasCRequestException(throwable);
        }
        return tramite;
    }

    @Override
//    public TramiteResponseDto consultaEntidades(String razonsocial, String ultimoId) throws AdabasCRequestException {
    public EntidadListDto consultaEntidades(String razonsocial, String ultimoId) throws AdabasCRequestException {
//        FotocopiasResponseDto tramite = new FotocopiasResponseDto();
        EntidadListDto entidadListDto = new EntidadListDto();
        String formEntidadesListado = EXConfig.formEntidadesListado();

        EXQuery exQuery = new EXQuery(formEntidadesListado);
        exQuery.setParameter("razonSocial", razonsocial);
        exQuery.setParameter("ultimoId", ultimoId);

        try {
            EXResult result = exQuery.execute();

            ByteArrayInputStream inputStream = new ByteArrayInputStream (result.getXmlResult().getBytes());
            SAXBuilder builder = new SAXBuilder(false);
            Document document = builder.build(inputStream);
            Element rootElement = document.getRootElement();
            String codError = rootElement.getChild("_NU-MSG-ERR").getText();
            String msgError = rootElement.getChild("_DS-MSG-ERR").getText();
            Element array = rootElement.getChild("SLAF03S1-OUTs");
            ultimoId = rootElement.getChild("_ISN-DDE").getText();

            if(array != null && array.hasChildren()){
                try{
                    List<EntidadDto> entidades = new ArrayList<EntidadDto>();
                    List listado = array.getChildren();
                    for (int i = 0; i < listado.size(); i++) {
                        Element item = (Element) array.getChildren().get(i);
                        EntidadDto entidad = new EntidadDto();
                        entidad.setMatricula(item.getChild("_ID-MATRICULA").getText());
                        entidad.setNombreEmpresa(item.getChild("_NOM-INSC1").getText());
                        entidad.setEstadoMatricula(item.getChild("_DS-CTR-EST-MATRICULA").getText());
                        entidades.add(entidad);

                        System.out.println("****************** " + entidad.getNombreEmpresa());
                    }
                    entidadListDto.setEntidades(entidades);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            entidadListDto.setUltimoId(ultimoId);
//            tramite.setCodigoError(codError);
//            tramite.setMensajeError(msgError);
//
//            tramite.setRequestXml(exQuery.toString());
//            tramite.setResponseXml(result.getXmlResult());
            System.out.println("_NU-MSG-ERR: " + codError + " _DS-MSG-ERR: " + msgError);
        } catch (Throwable throwable) {
            throw new AdabasCRequestException(throwable);
        }
        return entidadListDto;
    }

    @Override
    public EntidadDto consultaEntidad(String matricula) throws AdabasCRequestException {
        EntidadDto entidadDto = new EntidadDto();
        String formEntidadMatricula = EXConfig.formEntidadMatricula();
        EXQuery exQuery = new EXQuery(formEntidadMatricula);
        exQuery.setParameter("matricula", matricula);

        try {
            EXResult result = exQuery.execute();

            ByteArrayInputStream inputStream = new ByteArrayInputStream (result.getXmlResult().getBytes());
            SAXBuilder builder = new SAXBuilder(false);
            Document document = builder.build(inputStream);
            Element rootElement = document.getRootElement();
            String codError = rootElement.getChild("_NU-MSG-ERR").getText();
            String msgError = rootElement.getChild("_DS-MSG-ERR").getText();
            Element array = rootElement.getChild("SLAF03S2-OUTs");

            if(array != null && array.hasChildren()){
                try{
                    Element item = (Element) array.getChildren().get(0);
                    entidadDto.setMatricula(item.getChild("_ID-MATRICULA").getText());
                    entidadDto.setNombreEmpresa(item.getChild("_NOM-INSC1").getText());
                    entidadDto.setEstadoMatricula(item.getChild("_DS-CTR-EST-MATRICULA").getText());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
//            tramite.setCodigoError(codError);
//            tramite.setMensajeError(msgError);
//
//            tramite.setRequestXml(exQuery.toString());
//            tramite.setResponseXml(result.getXmlResult());
            System.out.println("_NU-MSG-ERR: " + codError + " _DS-MSG-ERR: " + msgError);
        } catch (Throwable throwable) {
            throw new AdabasCRequestException(throwable);
        }
        return entidadDto;
    }

    @Override
    public FotocopiaListDto consultaFotocopias(String matricula, String ultimoId) throws AdabasCRequestException {
        FotocopiaListDto fotocopiaListDto = new FotocopiaListDto();
        String formListaFotocopias = EXConfig.formFotocopiasListado();
        EXQuery exQuery = new EXQuery(formListaFotocopias);
        exQuery.setParameter("matricula", matricula);
        exQuery.setParameter("ultimoId", ultimoId);

        try {
            EXResult result = exQuery.execute();

            ByteArrayInputStream inputStream = new ByteArrayInputStream (result.getXmlResult().getBytes());
            SAXBuilder builder = new SAXBuilder(false);
            Document document = builder.build(inputStream);
            Element rootElement = document.getRootElement();
            String codError = rootElement.getChild("_NU-MSG-ERR").getText();
            String msgError = rootElement.getChild("_DS-MSG-ERR").getText();
            String lasId = rootElement.getChild("_ISN-DDE").getText();
            Element array = rootElement.getChild("SLAF10C0-OUTs");

            if(array != null && array.hasChildren()){
                try{
                    List<DocumentoFotocopiasDto> documentos = new ArrayList<DocumentoFotocopiasDto>();
                    List listado = array.getChildren();
                    for (int i = 0; i < listado.size(); i++) {
                        Element item = (Element) listado.get(i);
                        DocumentoFotocopiasDto doc = new DocumentoFotocopiasDto();
                        doc.setIdLibro(item.getChild("_ID-LIBRO").getText());
                        doc.setNumRegistro(item.getChild("_NUM-REG").getText());
                        doc.setDescripcion(item.getChild("_TXT-OBS").getText());
                        documentos.add(doc);

                        System.out.println("****************** " + doc.getDescripcion());
                    }
                    fotocopiaListDto.setDocumentos(documentos);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

//            tramite.setCodigoError(codError);
//            tramite.setMensajeError(msgError);
            fotocopiaListDto.setUltimoId(lasId);

//            tramite.setRequestXml(exQuery.toString());
//            tramite.setResponseXml(result.getXmlResult());
            System.out.println("_NU-MSG-ERR: " + codError + " _DS-MSG-ERR: " + msgError);
        } catch (Throwable throwable) {
            throw new AdabasCRequestException(throwable);
        }
        return fotocopiaListDto;
    }

    private String getExpci(String expci) {
        return expci == null || expci.equals("")? "Santa Cruz" : expci;
    }

}
