package ch.swissbytes.fundempresa.adabasc.interfaces;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;

/**
 * Created by daniel on 31/03/2016.
 */
public interface FotocopiasAdabasService {

    TramiteResponseDto ingresarFotocopia(FotocopiasRequestDto fotocopiasRequestDto) throws AdabasCRequestException;

//    TramiteResponseDto consultaEntidades(String razonsocial, String ultimoId) throws AdabasCRequestException;
    EntidadListDto consultaEntidades(String razonsocial, String ultimoId) throws AdabasCRequestException;

    EntidadDto consultaEntidad(String matricula) throws AdabasCRequestException;

    FotocopiaListDto consultaFotocopias(String matricula, String ultimoId) throws AdabasCRequestException;
}
