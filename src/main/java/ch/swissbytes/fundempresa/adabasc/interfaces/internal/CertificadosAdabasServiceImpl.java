package ch.swissbytes.fundempresa.adabasc.interfaces.internal;

import ch.swissbytes.fundempresa.adabasc.EXConfig;
import ch.swissbytes.fundempresa.adabasc.EXQuery;
import ch.swissbytes.fundempresa.adabasc.EXResult;
import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasc.interfaces.CertificadosAdabasService;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import com.softwareag.tamino.db.api.response.sax.helper.sodom.Document;
import com.softwareag.tamino.db.api.response.sax.helper.sodom.Element;
import com.softwareag.tamino.db.api.response.sax.helper.sodom.input.SAXBuilder;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by daniel on 31/03/2016.
 */
public class CertificadosAdabasServiceImpl implements CertificadosAdabasService {

    @Override
    public CertificadosResponseDto especial(CertificadosRequestDto certificadosRequestDto, UserProfileDto userProfileDto) throws AdabasCRequestException {
        CertificadosResponseDto tramite = new CertificadosResponseDto();
        String formulario = EXConfig.formEspeciales();

        EXQuery exQuery = new EXQuery(formulario);
//        exQuery.setParameter("solicitante", certificadosRequestDto.getSolicitante());
//        exQuery.setParameter("ci", certificadosRequestDto.getCi());
//        exQuery.setParameter("expci", certificadosRequestDto.getExpci());
//        exQuery.setParameter("telefono", certificadosRequestDto.getTelefono());
//        exQuery.setParameter("direccion", certificadosRequestDto.getDireccion());
//        exQuery.setParameter("ciudad", certificadosRequestDto.getCiudad());
//        exQuery.setParameter("provincia", certificadosRequestDto.getProvincia());

        exQuery.setParameter("solicitante", userProfileDto.getFullName());
        exQuery.setParameter("ci", userProfileDto.getCi());
        exQuery.setParameter("expci", userProfileDto.getExpci());
        exQuery.setParameter("telefono", userProfileDto.getPhone());
        exQuery.setParameter("direccion", userProfileDto.getAddress());
        exQuery.setParameter("ciudad", userProfileDto.getCity());
        exQuery.setParameter("provincia", userProfileDto.getCity());// debe ser provincia

        exQuery.setParameter("matricula", certificadosRequestDto.getMatricula());
//        exQuery.setParameter("monto", certificadosRequestDto.getMontoPagado());
        exQuery.setParameter("monto", certificadosRequestDto.getPayAmount());
        exQuery.setParameter("cantidad", certificadosRequestDto.getEjemplares());
        exQuery.setParameter("certificado", certificadosRequestDto.getCertificado());
        exQuery.setParameter("descripcion", certificadosRequestDto.getDescripcion());
        exQuery.setParameter("sucursal", certificadosRequestDto.getSucursal());

        try {
            EXResult result = exQuery.execute();

            ByteArrayInputStream inputStream = new ByteArrayInputStream (result.getXmlResult().getBytes());
            SAXBuilder builder = new SAXBuilder(false);
            Document document = builder.build (inputStream);
            Element rootElement = document.getRootElement();
            String codError = rootElement.getChild("_NU-MSG-ERR").getText();
            String msgError = rootElement.getChild("_DS-MSG-ERR").getText();
            String codTramite = rootElement.getChild("_COD-TRAMITE").getText();

            tramite.setCodigoError(codError);
            tramite.setMensajeError(msgError);
            tramite.setCodigoTramite(codTramite);

            tramite.setRequestXml(exQuery.toString());
            tramite.setResponseXml(result.getXmlResult());
            System.out.println("_NU-MSG-ERR: " + codError + " _DS-MSG-ERR: " + msgError + " _COD-TRAMITE: " + codTramite);
        } catch (Throwable throwable) {
            throw new AdabasCRequestException(throwable);
        }
        return tramite;
    }

    @Override
    public CertificadosResponseDto enlinea(CertificadosRequestDto certificadosRequestDto, UserProfileDto userProfileDto) throws AdabasCRequestException {
        CertificadosResponseDto tramite = new CertificadosResponseDto();
        String formulario = EXConfig.formEnLinea();

        EXQuery exQuery = new EXQuery(formulario);

        exQuery.setParameter("solicitante", userProfileDto.getFullName());
        exQuery.setParameter("ci", userProfileDto.getCi());
        exQuery.setParameter("expci", userProfileDto.getExpci());
        exQuery.setParameter("telefono", userProfileDto.getPhone());
        exQuery.setParameter("monto", certificadosRequestDto.getPayAmount());

        /******************* REGISTRANDO ARRAY DE DOCUMENTOS *************/
        String strDocumentos = "";
        List<CertificadoDto> documentos = certificadosRequestDto.getDocumentos();
        if(documentos != null && !documentos.isEmpty()){
            for (int i = 0; i < documentos.size(); i++) {
                CertificadoDto doc = documentos.get(i);
                strDocumentos += "<VVAS06SC-INOUT>" +
                        "<_ID-SERVICIO>" + doc.getCodigo() + "</_ID-SERVICIO>" +
                        "<_ID-MATRICULA>" + doc.getMatricula() + "</_ID-MATRICULA>" +
                        "<_ID-LIBRO>" + doc.getLibro() + "</_ID-LIBRO>" +
                        "<_NUM-REGISTRO>" + doc.getRegistro() + "</_NUM-REGISTRO>" +
                        "<_CANTIDAD-SOLICITADA>" + doc.getEjemplares() + "</_CANTIDAD-SOLICITADA>" +
                        "<_SUBTOTAL-PAGADO>" + doc.getSubtotal() + "</_SUBTOTAL-PAGADO>" +
                        "<_CODIGO-SEGURIDAD></_CODIGO-SEGURIDAD>" +
                        "<_COD-IMPRESION></_COD-IMPRESION>" +
                        "<_PIN-CERTIF></_PIN-CERTIF>" +
                        "</VVAS06SC-INOUT>";
//                "<_CODIGO-SEGURIDAD>" + doc.getCodSeguridad() + "</_CODIGO-SEGURIDAD>" +
            }
        }else{
            // DOCUMENTS MUST NOT BE EMPTY BUT, JUST IN CASE
            strDocumentos += "<VVAS06SC-INOUT>" +
                    "<_ID-SERVICIO></_ID-SERVICIO>" +
                    "<_ID-MATRICULA></_ID-MATRICULA>" +
                    "<_ID-LIBRO></_ID-LIBRO>" +
                    "<_NUM-REGISTRO></_NUM-REGISTRO>" +
                    "<_CANTIDAD-SOLICITADA></_CANTIDAD-SOLICITADA>" +
                    "<_SUBTOTAL-PAGADO></_SUBTOTAL-PAGADO>" +
                    "<_CODIGO-SEGURIDAD></_CODIGO-SEGURIDAD>" +
                    "<_COD-IMPRESION></_COD-IMPRESION>" +
                    "<_PIN-CERTIF></_PIN-CERTIF>" +
                    "</VVAS06SC-INOUT>";
        }
        exQuery.setParameter("documentos", strDocumentos);
        /********************************************************************/

        try {
            EXResult result = exQuery.execute();

            ByteArrayInputStream inputStream = new ByteArrayInputStream (result.getXmlResult().getBytes());
            SAXBuilder builder = new SAXBuilder(false);
            Document document = builder.build(inputStream);
            Element rootElement = document.getRootElement();
            String codError = rootElement.getChild("_NU-MSG-ERR").getText();
            String msgError = rootElement.getChild("_DS-MSG-ERR").getText();

            Element array = rootElement.getChild("VVAS06SC-INOUTs");
            if(array != null && array.hasChildren()){
                try{
                    List listado = array.getChildren();
                    for (int i = 0; i < documentos.size(); i++) {
                        Element item = (Element) listado.get(i);
                        CertificadoDto doc = documentos.get(i);
                        String codTramite = item.getChild("_COD-IMPRESION").getText();
                        String pinTramite = item.getChild("_PIN-CERTIF").getText();
                        doc.setCodImpresion(codTramite);
                        doc.setPinImpresion(pinTramite);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            tramite.setCertificados(documentos);
            tramite.setCodigoError(codError);
            tramite.setMensajeError(msgError);

            tramite.setRequestXml(exQuery.toString());
            tramite.setResponseXml(result.getXmlResult());
            System.out.println("_NU-MSG-ERR: " + codError + " _DS-MSG-ERR: " + msgError);
        } catch (Throwable throwable) {
            throw new AdabasCRequestException(throwable);
        }
        return tramite;
    }

    @Override
    public CertificadosResponseDto validar(CertificadosRequestDto certificadosRequestDto) throws AdabasCRequestException {
        CertificadosResponseDto tramite = new CertificadosResponseDto();
        String formulario = EXConfig.formValidar();

        EXQuery exQuery = new EXQuery(formulario);
        exQuery.setParameter("solicitante", certificadosRequestDto.getSolicitante());
        exQuery.setParameter("ci", certificadosRequestDto.getCi());
        exQuery.setParameter("expci", certificadosRequestDto.getExpci());
        exQuery.setParameter("email", certificadosRequestDto.getEmail());
        exQuery.setParameter("uso", certificadosRequestDto.getUsoValidacion());
        exQuery.setParameter("matricula", certificadosRequestDto.getMatricula());
        exQuery.setParameter("codigo", certificadosRequestDto.getCodigoValidacion());

        long fecha = certificadosRequestDto.getFechaCertificado();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String fechaStr = format.format(new Date(fecha));
        exQuery.setParameter("fecha", fechaStr);

        try {
            EXResult result = exQuery.execute();

            ByteArrayInputStream inputStream = new ByteArrayInputStream (result.getXmlResult().getBytes());
            SAXBuilder builder = new SAXBuilder(false);
            Document document = builder.build(inputStream);
            Element rootElement = document.getRootElement();

            String textoCertif = "";
            Element array = rootElement.getChild("_GR-TXT-CERTIF-Ds");
            if(array != null && array.hasChildren()){
                try{
                    List listado = array.getChildren();
                    for (int i = 0; i < listado.size(); i++) {
                        Element item = (Element) listado.get(i);
                        textoCertif += "\n" + item.getChild("_TXT-CERTIF-D").getText();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            String strSolicitadas = rootElement.getChild("_CNT-VAL-SOLICITADAS").getText();
            String strRestantes = rootElement.getChild("_CNT-VAL-RESTANTES").getText();

            tramite.setTextCertificado(textoCertif.trim());
            tramite.setValidRestantes(strRestantes == null || strRestantes.isEmpty() ? 0 : Integer.parseInt(strRestantes));
            tramite.setValidSolicitadas(strSolicitadas == null || strSolicitadas.isEmpty() ? 0 : Integer.parseInt(strSolicitadas));

            tramite.setRequestXml(exQuery.toString());
            tramite.setResponseXml(result.getXmlResult());
            System.out.println("_CNT-VAL-SOLICITADAS: " + strSolicitadas + " _CNT-VAL-RESTANTES: " + strRestantes + " _TXT-CERTIF-D: " + textoCertif);
        } catch (Throwable throwable) {
            throw new AdabasCRequestException(throwable);
        }
        return tramite;
    }
}
