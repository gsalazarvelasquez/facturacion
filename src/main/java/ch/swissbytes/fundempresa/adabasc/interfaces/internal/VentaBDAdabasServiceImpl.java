package ch.swissbytes.fundempresa.adabasc.interfaces.internal;

import ch.swissbytes.fundempresa.adabasc.EXConfig;
import ch.swissbytes.fundempresa.adabasc.EXQuery;
import ch.swissbytes.fundempresa.adabasc.EXResult;
import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasc.interfaces.VentaBDAdabasService;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import com.softwareag.tamino.db.api.response.sax.helper.sodom.Document;
import com.softwareag.tamino.db.api.response.sax.helper.sodom.Element;
import com.softwareag.tamino.db.api.response.sax.helper.sodom.input.SAXBuilder;

import java.io.ByteArrayInputStream;
import java.util.List;

/**
 * Created by daniel on 31/03/2016.
 */
public class VentaBDAdabasServiceImpl implements VentaBDAdabasService {

    @Override
    public VentaBDResponseDto solicitar(VentaBDRequestAdabasDto ventaBdRequestDto) throws AdabasCRequestException {
        VentaBDResponseDto tramite = new VentaBDResponseDto();
        String formulario = EXConfig.formVentaBD();

        EXQuery exQuery = new EXQuery(formulario);
        exQuery.setParameter("servicio", ventaBdRequestDto.getCodServicio());
        exQuery.setParameter("solicitante", ventaBdRequestDto.getSolicitante());
        exQuery.setParameter("ci", ventaBdRequestDto.getCi());
        exQuery.setParameter("expci", ventaBdRequestDto.getExpci());
        exQuery.setParameter("telefono", ventaBdRequestDto.getTelefono());
        exQuery.setParameter("email", ventaBdRequestDto.getEmail());
        exQuery.setParameter("uso", ventaBdRequestDto.getUsoInformacion());
        exQuery.setParameter("monto", ventaBdRequestDto.getPayAmount());
        exQuery.setParameter("descripcion", ventaBdRequestDto.getDescripcion());
        exQuery.setParameter("sucursal", ventaBdRequestDto.getSucursal());

        /******************* REGISTRANDO ARRAY DE DOCUMENTOS *************/
        String strVariables = "";
        List<VentaBDVariablesAdabasDto> variables = ventaBdRequestDto.getVariables();
        if(variables != null && !variables.isEmpty()){
            for (int i = 0; i < variables.size(); i++) {
                VentaBDVariablesAdabasDto doc = variables.get(i);
                strVariables += "<VVNS07SE-IN>" +
                        "<_VARIABLE-SOLICITA>" + doc.getVariable() + "</_VARIABLE-SOLICITA>" +
                        "<_VALOR-SOLICITA>" + doc.getValor() + "</_VALOR-SOLICITA>" +
                        "</VVNS07SE-IN>";
            }
        }else{
            // DOCUMENTS MUST NOT BE EMPTY BUT, JUST IN CASE
            strVariables += "<VVNS07SE-IN>" +
                    "<_VARIABLE-SOLICITA></_VARIABLE-SOLICITA>" +
                    "<_VALOR-SOLICITA></_VALOR-SOLICITA>" +
                    "</VVNS07SE-IN>";
        }
        exQuery.setParameter("variables", strVariables);
        /********************************************************************/

        try {
            EXResult result = exQuery.execute();

            ByteArrayInputStream inputStream = new ByteArrayInputStream (result.getXmlResult().getBytes());
            SAXBuilder builder = new SAXBuilder(false);
            Document document = builder.build(inputStream);
            Element rootElement = document.getRootElement();

            String codError = rootElement.getChild("_NU-MSG-ERR").getText();
            String msgError = rootElement.getChild("_DS-MSG-ERR").getText();
            String codTramite = rootElement.getChild("_COD-TRAMITE").getText();

            tramite.setCodigoTramite(codTramite);
            tramite.setCodigoError(codError);
            tramite.setMensajeError(msgError);

            tramite.setRequestXml(exQuery.toString());
            tramite.setResponseXml(result.getXmlResult());
            System.out.println("_NU-MSG-ERR: " + codError + " _DS-MSG-ERR: " + msgError + " _COD-TRAMITE: " + codTramite);
        } catch (Throwable throwable) {
            throw new AdabasCRequestException(throwable);
        }
        return tramite;
    }

    private String getExpci(String expci) {
        return expci == null || expci.equals("")? "0" : expci;
    }

}
