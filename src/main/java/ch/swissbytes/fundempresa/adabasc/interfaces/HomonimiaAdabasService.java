package ch.swissbytes.fundempresa.adabasc.interfaces;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasCRequestException;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.HomonimiaRequestDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.ResponseDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.TramiteResponseDto;

/**
 * Created by daniel on 31/03/2016.
 */
public interface HomonimiaAdabasService {

    TramiteResponseDto ingresarHomonimia(HomonimiaRequestDto homonimiaRequestDto, UserProfileDto userProfileDto) throws AdabasCRequestException;

    TramiteResponseDto reingresarHomonimia(HomonimiaRequestDto homonimiaRequestDto) throws AdabasCRequestException;

    TramiteResponseDto getEstadoHomonimia(String codigoTramite) throws AdabasCRequestException;
}
