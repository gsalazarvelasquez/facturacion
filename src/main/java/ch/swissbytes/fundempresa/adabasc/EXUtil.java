package ch.swissbytes.fundempresa.adabasc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

/* 
 * Copyright 2013 (C) <SwissBytes Ltda> 
 *  
 * Created on : Jul 1, 2013
 * @author    : Rory Sandoval <rory.sandoval@swissbytes.ch> 
 *
 */
public class EXUtil {

	private final static Logger log = LoggerFactory.getLogger(EXUtil.class);

	private static ClassLoader findClassLoader() {
		ClassLoader loader = EXConfig.class.getClassLoader();
		if (loader == null)
			loader = ClassLoader.getSystemClassLoader();
		return loader;
	}

	private static String loadFile(String filename) {
		URL url = findClassLoader().getResource(filename);
		String content = "";
		try {
			content = new Scanner(url.openStream()).useDelimiter("\\Z").next();
		} catch (IOException e) {
			log.error("Error reading file: " + filename);
		}
		return content;

	}

	/**
	 * @param queryName
	 * @return
	 */
	public static String loadQuery(String queryName) {
		return loadFile(queryName);
	}

	/**
	 * @param xmlDocument
	 * @param parameters
	 * @return
	 */	
	public static String replace(String xml, Map<String, String> parameters) {
		log.info("Parameter Count: " + parameters.size() );
		String parsed = xml;
		for (Entry<String, String> entry : parameters.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			parsed = parsed.replace("{"+key+"}", value);
		}
		return parsed;
	}

}
