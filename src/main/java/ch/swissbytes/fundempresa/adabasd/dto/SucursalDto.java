package ch.swissbytes.fundempresa.adabasd.dto;

import lombok.Data;

/**
 * Created by jose on 22/03/2016.
 */
@Data
public class SucursalDto {

    private String idSucursal;
    private String descSucursal;
    private String direccion;

}
