package ch.swissbytes.fundempresa.adabasd.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jose on 01/06/2016.
 */
@Getter
@Setter
public class EmpresasDto {

    private String matricula;
    private String razonSocial;
    private String tipoSocietario;
    private String departamento;
    private String municipio;
    private String zona;
    private String direccionComercial;
    private String telefono;
    private String fax;
    private String actividad;

    @Override
    public String toString() {
        return "EmpresasDto{" +
                "matricula='" + matricula + '\'' +
                ", razonSocial='" + razonSocial + '\'' +
                ", tipoSocietario='" + tipoSocietario + '\'' +
                ", departamento='" + departamento + '\'' +
                ", municipio='" + municipio + '\'' +
                ", zona='" + zona + '\'' +
                ", direccionComercial='" + direccionComercial + '\'' +
                ", telefono='" + telefono + '\'' +
                ", fax='" + fax + '\'' +
                ", actividad='" + actividad + '\'' +
                '}';
    }
}
