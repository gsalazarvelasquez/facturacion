package ch.swissbytes.fundempresa.adabasd.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
public class UserProfileDto {

    private String username;
    private String fullName;
    private String password;
    private String ci;
    private String expci;
    private String matricula;
    private String phone;
    private String email;
    private String city;
    private String address;
    private String profile;
    private String balance;

}
