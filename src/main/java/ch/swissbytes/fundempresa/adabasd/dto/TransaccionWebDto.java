package ch.swissbytes.fundempresa.adabasd.dto;

import lombok.Data;

/**
 * Created by jose on 24/03/2016.
 */
@Data
public class TransaccionWebDto {

    private String idOperacion;
    private String numOperacion;
    private String idServicio;
    private String descServicio;
    private String montoPagado;
    private String matricula;
    private String codCliente;
    private String username;

}
