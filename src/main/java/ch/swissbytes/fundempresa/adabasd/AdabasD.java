package ch.swissbytes.fundempresa.adabasd;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ch.swissbytes.fundempresa.adabasc.exception.AdabasDRequestException;
import ch.swissbytes.fundempresa.adabasd.dto.EmpresasDto;
import ch.swissbytes.fundempresa.ventanilla.data.dto.*;
import ch.swissbytes.fundempresa.adabasd.dto.SucursalDto;
import ch.swissbytes.fundempresa.adabasd.dto.TransaccionWebDto;
import ch.swissbytes.fundempresa.adabasd.dto.UserProfileDto;
import ch.swissbytes.fundempresa.ventanilla.data.model.TipoSociedad;
import com.consist.commonFunctions.ConsistEnv;
import com.consist.seguridad.common.BTEncriptacion;

public class AdabasD {

    public static boolean login(String username, String password) {
        boolean response = false;
        try{
            UserProfileDto u = getUser(username.toUpperCase());
            if(u != null){
                BTEncriptacion encriptacion = new BTEncriptacion();
                String encrypted = encriptacion.encriptarClave(password.toUpperCase(), username.toUpperCase(), u.getProfile().toUpperCase());
                response = u.getPassword().equals(encrypted);
            }else{
                System.out.println("User not found in database!");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return response;
    }

    public static UserProfileDto autenticar(String username, String password) {
//        boolean response = false;
        UserProfileDto result = null;
        try{
            result = getUser(username.toUpperCase());
            if(result != null){
                BTEncriptacion encriptacion = new BTEncriptacion();
                String encrypted = encriptacion.encriptarClave(password.toUpperCase(), username.toUpperCase(), result.getProfile().toUpperCase());
//                response = result.getPassword().equals(encrypted);
                if (!result.getPassword().equals(encrypted)){
                    result = null;
                }
            }else{
                System.out.println("User not found in database!");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    public static String codigoServicio(String servicio) {
        String codigo = "";
        Connection connection = null;
        try{
            //Open connection
            connection = init();

            //Create statement for openned connection
            String sql = "SELECT CD_SERV FROM T005 WHERE CD_SERV_INT = '" + servicio + "'";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);
            if(rs != null){
                if(rs.next()){
                    codigo = rs.getString(1);
                }
                rs.close();
            }
            st.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return codigo;
    }

    public static UserProfileDto profile(String username) {
        Connection connection = null;
        UserProfileDto response = new UserProfileDto();
        try{
            //Open connection
            connection = init();

            //Create statement for openned connection
            String sql =
                    "SELECT U.DS_NOMBRE, U.DS_APELLIDO, U.DS_TELEFONO, U.DS_MAIL, C.QT_SALDO, C.DS_CIUDAD, " +
                    "C.DS_DIRECCION, C.CD_NUMERO_IDENTIF, U.CD_CLAVE, U.CD_PERFIL " +
                    "FROM USUARIO U, CLIENTE C " +
                    "WHERE U.CD_USUARIO = '" + username.toUpperCase() + "' " +
                    "AND U.CD_ACTIVO IS TRUE AND U.CD_UF = C.CD_CLIENTE";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);

            if(rs != null){
                if(rs.next()){
                    response.setFullName(rs.getString(1) + " " + rs.getString(2));
                    response.setPhone(rs.getString(3));
                    response.setEmail(rs.getString(4));
                    response.setBalance(rs.getString(5));
                    response.setCity(rs.getString(6));
                    response.setAddress(rs.getString(7));
                    response.setCi(rs.getString(8));
                    response.setPassword(rs.getString(9));
                    response.setProfile(rs.getString(10));
                    response.setExpci(rs.getString(6));
                }
                rs.close();
            }
            st.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }

    public static List<SucursalDto> sucursales() throws AdabasDRequestException {
        List<SucursalDto> response = new ArrayList<SucursalDto>();
        Connection connection = null;

        try{
            connection = init();

            //Create statement for openned connection
            String sql = "SELECT * FROM SUCURSALES";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);

            if(rs != null){
                while(rs.next()){
                    SucursalDto item = new SucursalDto();
                    item.setIdSucursal(rs.getString("ID_SUCURSAL"));
                    item.setDescSucursal(rs.getString("DESC_SUCURSAL"));
                    item.setDireccion(rs.getString("DIRECCION_SUCURSAL"));
                    response.add(item);
                }
                rs.close();
            }
            st.close();
        } catch (Throwable throwable) {
            throw new AdabasDRequestException(throwable);
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }

    public static List<PerfilAdabasDto> perfiles() throws AdabasDRequestException {
        List<PerfilAdabasDto> response = new ArrayList<PerfilAdabasDto>();
        Connection connection = null;

        try{
            connection = init();

            //Create statement for openned connection
            String sql = "SELECT * FROM PERFIL";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);

            if(rs != null){
                while(rs.next()){
                    PerfilAdabasDto item = new PerfilAdabasDto();
                    item.setCodigo(rs.getString("CD_PERFIL"));
                    item.setDescripcion(rs.getString("DS_PERFIL"));
                    response.add(item);
                }
                rs.close();
            }
            st.close();
        } catch (Throwable throwable) {
            throw new AdabasDRequestException(throwable);
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }

    public static List<TipoSociedad> tiposSocietarios() throws AdabasDRequestException {
        List<TipoSociedad> response = new ArrayList<TipoSociedad>();
        Connection connection = null;

        try{
            //Open connection
            connection = init();

            //Create statement for openned connection
            String sql = "SELECT * FROM TIPOS_SOCIETARIOS";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);

            if(rs != null){
                while(rs.next()){
                    TipoSociedad item = new TipoSociedad();
                    item.setId(Long.parseLong(rs.getString("CRT_TPS")));
                    item.setValor(rs.getString("TIPOSOCIETARIO"));
                    response.add(item);
                }
                rs.close();
            }
            st.close();
        } catch (Throwable throwable) {
            throw new AdabasDRequestException(throwable);
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }

    public static TipoSociedad tipoSocietarioById(Long id) throws AdabasDRequestException {
//        List<TipoSociedad> response = new ArrayList<TipoSociedad>();
        TipoSociedad response = null;
        Connection connection = null;

        try{
            //Open connection
            connection = init();

            //Create statement for openned connection
            String sql = "SELECT * FROM TIPOS_SOCIETARIOS WHERE CRT_TPS = '" + formatTipoId(id) + "'";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);

            if(rs != null){
//                while(rs.next()){
//                    TipoSociedad item = new TipoSociedad();
//                    item.setId(Long.parseLong(rs.getString("CRT_TPS")));
//                    item.setValor(rs.getString("TIPOSOCIETARIO"));
////                    item.setFactor(rs.getString("FACTOR"));
////                    response.add(item);
//                }
                rs.next();
                response = new TipoSociedad();
                response.setId(Long.parseLong(rs.getString("CRT_TPS")));
                response.setValor(rs.getString("TIPOSOCIETARIO"));

                rs.close();
            }
            st.close();
        } catch (Throwable throwable) {
            throw new AdabasDRequestException(throwable);
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }

    private static String formatTipoId(Long id){
        String result = (id < 10) ? "0" + id : id + "";

        return result;
    }

    public static void transaction(ResponseDto response, String username, String matricula, String service, String descrip) {
        Connection connection = null;
        try{ //Open connection
            connection = init();

            //Getting LastId in transactions table
            Calendar calendar = Calendar.getInstance();
            int mes = calendar.get(Calendar.MONTH) + 1;
            int dia = calendar.get(Calendar.DATE) - 3;
            TransaccionWebDto transaccion = ultimoId(mes, dia, connection);
            transaccion.setUsername(username);
            transaccion.setMatricula(matricula);
            if(transaccion.getNumOperacion() != null) {
                //Reserve lastId + 1 in transaction table
                reservar(transaccion, mes, dia, connection);
            }

            //Getting serviceId according to actual request (TRAMITE)
            if(response.getDescTramite().isEmpty()){
                transaccion.setIdServicio(service);
                transaccion.setDescServicio(descrip);
            }else{
                String tramite = response.getDescTramite();
                TransaccionWebDto servicio = servicio(tramite, connection);
                transaccion.setIdServicio(servicio.getIdServicio());
                transaccion.setDescServicio(tramite);
            }

            //Getting user information
            String codCliente = usuario(username, connection);
            transaccion.setCodCliente(codCliente);

            String mesDia = (mes < 10 ? "0" : "") + mes;
            mesDia += (dia < 10 ? "0" : "") + dia;
            String idOper = transaccion.getIdOperacion();
            String numOper = transaccion.getNumOperacion();
            String idServ = transaccion.getIdServicio();
            String descServ = transaccion.getDescServicio();
            String monto = response.getArancel();

            //Saving transaction information
            String sqlTransaccionesWeb = "INSERT INTO TRANSACCIONES_WEB (ID_SUCUR, ID_OPE, FEC_MES_DIA, " +
                    "NUM_OPERA, FEC_OPERA, CTR_ANUL, HOR_PAGO, TOT_PAGO, CTR_RELIQ, " +
                    "ID_SERV, ID_SERV_DESC, VR_SERV, CNT_SERV, CTR_GTO_ADMT, " +
                    "ID_MATRICULA, CTR_PAGO, CTR_FORMAS_PAGO, COD_CLIENTE, " +
                    "CD_USUARIO_ALTA, DT_ALTA, CD_USUARIO_ACTUAL, DT_ACTUAL) VALUES " +
                    "('99','" + idOper + "','" + mesDia + "','" + numOper + "',NOW(),'0',NOW(),0.0,0," +
                    "'" + idServ + "','" + descServ + "'," + monto + ",1,0,'" + matricula + "','1','0'," +
                    "'" + codCliente + "','" + username + "',NOW(),'" + username + "',NOW())";
            Statement st = connection.createStatement();
            st.executeUpdate(sqlTransaccionesWeb);
            st.close();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
    }

    public static boolean debitar(String username, String monto) {
        boolean response = false;
        Connection connection = null;
        try{
            //Open connection
            connection = init();

            String sqlDebitar = "UPDATE CLIENTE SET QT_SALDO=(QT_SALDO-" + monto + ") WHERE CD_CLIENTE = (SELECT CD_UF FROM USUARIO WHERE CD_USUARIO = '" + username + "')";
            Statement st = connection.createStatement();
            int affected = st.executeUpdate(sqlDebitar);
            st.close();
            response = affected > 0;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }

    public static boolean abonar(String username, String monto) {
        boolean response = false;
        Connection connection = null;
        try{
            //Open connection
            connection = init();

            String sqlDebitar = "UPDATE CLIENTE SET QT_SALDO=(QT_SALDO+" + monto + ") WHERE CD_CLIENTE = (SELECT CD_UF FROM USUARIO WHERE CD_USUARIO = '" + username + "')";
            Statement st = connection.createStatement();
            int affected = st.executeUpdate(sqlDebitar);
            st.close();
            response = affected > 0;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }

    private static Connection init() throws Exception {
        ConsistEnv xConsistEnv = new ConsistEnv();
        String driver = xConsistEnv.getProperty("SQL_DRIVER");
        String connectURL = xConsistEnv.getProperty("SQL_URL");
        String user = xConsistEnv.getProperty("SQL_USER");
        String password = xConsistEnv.getProperty("SQL_PASSWORD");
        //Register JDBC driver
        Class.forName(driver);

        //Open a connection
        return DriverManager.getConnection(connectURL, user, password);
    }

    private static TransaccionWebDto ultimoId(int mes, int dia, Connection connection) {
        TransaccionWebDto response = new TransaccionWebDto();
        try {
            String sqlUltimoId = "SELECT CD_OPERADOR, ULT_NUMERO FROM NUMERADOR_OPERAC WHERE " +
                    "MES=" + mes + " AND DIA=" + dia + "  ORDER BY DT_ALTA DESC";

            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(sqlUltimoId);

            if(rs != null && rs.next()){
                String operador = rs.getString("CD_OPERADOR");
                int ultimoId = rs.getInt("ULT_NUMERO") + 1;
                if(ultimoId == 1000){
                    ultimoId = 1;
                    operador = "Z92";
                }
                response.setIdOperacion(operador);
                response.setNumOperacion(String.valueOf(ultimoId));
                rs.close();
            }else{
                response.setIdOperacion("Z91");
                response.setNumOperacion("1");
            }
            st.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public static List<ListadoCalcDto> listadoEmpresas(VentaBDListadoDto requestDto) {
        List<String> departamentos = requestDto.getDepartamentos();
        List<String> municipios = requestDto.getMunicipios();
        List<String> tiposSocietarios = requestDto.getTiposSociedad();
        List<String> estados = requestDto.getEstados();
        List<String> secciones = requestDto.getSecciones();
        List<String> clases = requestDto.getClasesCIIU();

        boolean todosDepartamentos = requestDto.isTodosDepartamentos();
        boolean todosTiposSociedad = requestDto.isTodosTiposSociedad();
        boolean todosEstados = requestDto.isTodosEstados();
        boolean todasSecciones = requestDto.isTodasSecciones();

        List<ListadoCalcDto> response = new ArrayList<ListadoCalcDto>();
        Connection connection = null;
        try {
            String sql = "SELECT T.CRT_TPS, T.FACTOR, COUNT(*) AS CANTIDAD ";
            sql += "FROM MATRICULADOS M, TIPOS_SOCIETARIOS T ";
            sql += "WHERE M.CRT_TPS = T.CRT_TPS ";

            if(!todosDepartamentos){
                String dptosList = listToString(departamentos);
                String municpsList = listToString(municipios);
                municpsList = municpsList.isEmpty() ? "''" : municpsList;
                sql += "AND (M.ID_DPTO IN (" + dptosList + ") OR M.ID_MUNICIPIO IN (" + municpsList + ")) ";
            }
            if(!todosTiposSociedad){
                String tipSocList = listToString(tiposSocietarios);
                sql += "AND M.CRT_TPS IN (" + tipSocList + ") ";
            }
            if(!todasSecciones){
                String seccionesList = listToString(secciones);
                String clasesList = listToString(clases);
                clasesList = clasesList.isEmpty() ? "''" : clasesList;
                sql += "AND (M.ID_SECCION IN (" + seccionesList + ") OR M.ID_CLASE IN (" + clasesList + ")) ";
            }
            if(!todosEstados) {
                String anhosInscritas = requestDto.getAnhosInscritas();
                String anhosCanceladas = requestDto.getAnhosCanceladas();
                anhosInscritas = anhosInscritas == null ? "" : anhosInscritas;
                anhosCanceladas = anhosCanceladas == null ? "" : anhosCanceladas;

                String auxSql = "";
                String inscritas = "";
                String canceladas = "";
                if(!anhosInscritas.isEmpty()){
                    inscritas = "M.ANO_INSC IN (" + anhosInscritas + ") ";
                    auxSql = inscritas;
                }
                if(!anhosCanceladas.isEmpty()) {
                    auxSql = auxSql.isEmpty() ? "AND (" : ("AND (" + auxSql + "OR ");
                    canceladas = "M.ANO_CANC IN (" + anhosCanceladas + ")";
                    auxSql += canceladas;
                }else{
                    auxSql = auxSql.isEmpty() ? "" : ("AND (" + auxSql + " ");
                }
                auxSql += auxSql.isEmpty()? "" : ")";

                String estadosList = listToString(estados);
                sql += "AND (";
                sql += "M.CTR_ESTADO IN (" + estadosList + ") ";
                sql += auxSql;
                sql += ") ";
            }
            sql += "GROUP BY T.CRT_TPS, T.FACTOR ORDER BY T.CRT_TPS ";

            connection = init();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if(rs != null){
                while(rs.next()){
                    String tipoSocietario = rs.getString("CRT_TPS");
                    String cantidad = rs.getString("CANTIDAD");
                    String factor = rs.getString("FACTOR");

                    ListadoCalcDto item = new ListadoCalcDto();
                    item.setIdTipoSocietario(tipoSocietario);
                    item.setCantidad(cantidad);
                    item.setFactor(factor);
                    response.add(item);
                }
                rs.close();
            }
            st.close();

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }

    private static String listToString(List<String> list) {
        String response = "";
        if(list != null){
            for (int i = 0; i < list.size(); i++) {
                response += "'" + list.get(i) + "'";
                response += (i + 1 < list.size()) ? "," : "";
            }
        }
        return response;
    }

    private static void reservar(TransaccionWebDto transaccion, int mes, int dia, Connection connection) {
        try{
            String ultimoId = transaccion.getNumOperacion();
            String usuarioActual = transaccion.getUsername();
            String operador = transaccion.getIdOperacion();

            String sqlReservar = "UPDATE NUMERADOR_OPERAC SET ULT_NUMERO=" + ultimoId + ", " +
                    "CD_USUARIO_ACTUAL='" + usuarioActual + "', DT_ACTUAL=NOW() " +
                    "WHERE CD_SUCURSAL = '99' AND CD_OPERADOR = '" + operador + "'";
            if(ultimoId.equals("1")){
                sqlReservar = "INSERT INTO NUMERADOR_OPERAC VALUES ('99','Z91'," + mes +
                              "," + dia + "," + ultimoId + ",'" + usuarioActual +
                              "',NOW(),'" + usuarioActual + "',NOW())";
            }

            Statement st = connection.createStatement();
            st.executeUpdate(sqlReservar);
            st.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private static TransaccionWebDto servicio(String tramite, Connection connection) {
        TransaccionWebDto response = new TransaccionWebDto();
        try{
            String sqlServicio = "SELECT CD_SERV, DS_SERV FROM T005 WHERE CD_SERV_INT = '" + tramite + "'";
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(sqlServicio);

            if(rs != null && rs.next()){
                response.setIdServicio(rs.getString("CD_SERV"));
                response.setDescServicio(rs.getString("DS_SERV"));
                rs.close();
            }
            st.close();

        }catch (Exception e){
            e.printStackTrace();
        }
        return response;
    }

    private static String usuario(String username, Connection connection) {
        String response = "";
        try{
            String sqlUsuario = "SELECT CD_UF FROM USUARIO WHERE CD_USUARIO = '" + username + "'";
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(sqlUsuario);

            if(rs != null && rs.next()){
                response = rs.getString("CD_UF");
                rs.close();
            }
            st.close();

        }catch (Exception e){
            e.printStackTrace();
        }
        return response;
    }

    private static UserProfileDto getUser(String username) {
        Connection connection = null;
        UserProfileDto response = null;
        try{
            //Open connection
            connection = init();

            //Create statement for openned connection
            String sql = "SELECT CD_CLAVE, CD_PERFIL FROM USUARIO WHERE CD_USUARIO = '" + username + "'  AND CD_ACTIVO IS TRUE";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);
            if(rs != null){
                if(rs.next()){
                    response = new UserProfileDto();
                    response.setPassword(rs.getString(1));
                    response.setProfile(rs.getString(2));
                }
                rs.close();
            }
            st.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }

    public static List<Integer> variablesCruceVars() {
        Connection connection = null;
        List<Integer> response = new ArrayList<Integer>();
        try{
            //Open connection
            connection = init();
            response.add(departamentos(connection));
            response.add(municipios(connection));
            response.add(tiposSocietarios(connection));
            response.add(clasesCIIU(connection));
            response.add(5);//STATIC FINANTIAL VARS
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }

    private static int departamentos(Connection connection) {
        int response = 0;
        try{
            //Create statement for openned connection
            String sql = "SELECT COUNT(*) FROM DEPARTAMENTOS";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);
            if(rs != null){
                if(rs.next()){
                    response = rs.getInt(1);
                }
                rs.close();
            }
            st.close();
        }catch (Exception e){}
        return response;
    }

    public static List<Departamento> departamentos() {
        Connection connection = null;
        List<Departamento> response = new ArrayList<Departamento>();
        try{
            //Open connection
            connection = init();
            //Create statement for openned connection
            String sql = "SELECT * FROM DEPARTAMENTOS";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);
            if(rs != null){
                while(rs.next()) {
                    Departamento item = new Departamento();
                    item.setId(rs.getString(1));
                    item.setDescripcion(rs.getString(2));
                    response.add(item);
                }
                rs.close();
            }
            st.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }

    public static List<Municipio> municipios() {
        Connection connection = null;
        List<Municipio> response = new ArrayList<Municipio>();
        try{
            //Open connection
            connection = init();
            //Create statement for openned connection
            String sql = "SELECT * FROM MUNICIPIOS";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);
            if(rs != null){
                while(rs.next()) {
                    Municipio item = new Municipio();
                    item.setIdDpto(rs.getString(1));
                    item.setId(rs.getString(2));
                    item.setDescripcion(rs.getString(3));
                    response.add(item);
                }
                rs.close();
            }
            st.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }

    public static List<Seccion> secciones()     {
        Connection connection = null;
        List<Seccion> response = new ArrayList<Seccion>();
        try{
            //Open connection
            connection = init();
            //Create statement for openned connection
            String sql = "SELECT * FROM SECCION_CIIU";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);
            if(rs != null){
                while(rs.next()) {
                    Seccion item = new Seccion();
                    item.setId(rs.getString(1));
                    item.setDescripcion(rs.getString(2));
                    response.add(item);
                }
                rs.close();
            }
            st.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }

    public static List<ClasesCIIU> clasesCIIU()     {
        Connection connection = null;
        List<ClasesCIIU> response = new ArrayList<ClasesCIIU>();
        try{
            //Open connection
            connection = init();
            //Create statement for openned connection
            String sql = "SELECT * FROM CLASE_CIIU";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);
            if(rs != null){
                while(rs.next()) {
                    ClasesCIIU item = new ClasesCIIU();
                    item.setIdSeccion(rs.getString(1));
                    item.setId(rs.getString(2));
                    item.setDescripcion(rs.getString(3));
                    response.add(item);
                }
                rs.close();
            }
            st.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }

    private static int municipios(Connection connection) {
        int response = 0;
        try{
            //Create statement for openned connection
            String sql = "SELECT COUNT(*) FROM MUNICIPIOS";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);
            if(rs != null){
                if(rs.next()){
                    response = rs.getInt(1);
                }
                rs.close();
            }
            st.close();
        }catch (Exception e){}
        return response;
    }

    private static int tiposSocietarios(Connection connection) {
        int response = 0;
        try{
            //Create statement for openned connection
            String sql = "SELECT COUNT(*) FROM TIPOS_SOCIETARIOS";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);
            if(rs != null){
                if(rs.next()){
                    response = rs.getInt(1);
                }
                rs.close();
            }
            st.close();
        }catch (Exception e){}
        return response;
    }

    private static int clasesCIIU(Connection connection) {
        int response = 0;
        try{
            //Create statement for openned connection
            String sql = "SELECT COUNT(*) FROM CLASE_CIIU";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);
            if(rs != null){
                if(rs.next()){
                    response = rs.getInt(1);
                }
                rs.close();
            }
            st.close();
        }catch (Exception e){}
        return response;
    }

    public static int clases(List<String> secciones) {
        Connection connection = null;
        int response = 0;
        try{
            //Open connection
            connection = init();
            //Create statement for openned connection
            String sql = "SELECT COUNT(*) FROM CLASE_CIIU WHERE ID_SECCION IN (";
            sql += listToString(secciones) + ")";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);
            if(rs != null){
                if(rs.next()){
                    response = rs.getInt(1);
                }
                rs.close();
            }
            st.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }

    public static int municipios(List<String> departamentos) {
        Connection connection = null;
        int response = 0;
        try{
            //Open connection
            connection = init();
            //Create statement for openned connection
            String sql = "SELECT COUNT(*) FROM MUNICIPIOS WHERE ID_DPTO IN (";
            sql += listToString(departamentos) + ")";
            Statement st = connection.createStatement();

            //Execute a query
            ResultSet rs = st.executeQuery(sql);
            if(rs != null){
                if(rs.next()){
                    response = rs.getInt(1);
                }
                rs.close();
            }
            st.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }

    public static List<EmpresasDto> download(VentaBDListadoDto requestDto) {
        List<String> departamentos = requestDto.getDepartamentos();
        List<String> municipios = requestDto.getMunicipios();
        List<String> tiposSocietarios = requestDto.getTiposSociedad();
        List<String> estados = requestDto.getEstados();
        List<String> secciones = requestDto.getSecciones();
        List<String> clases = requestDto.getClasesCIIU();

        boolean todosDepartamentos = requestDto.isTodosDepartamentos();
        boolean todosTiposSociedad = requestDto.isTodosTiposSociedad();
        boolean todosEstados = requestDto.isTodosEstados();
        boolean todasSecciones = requestDto.isTodasSecciones();

        List<EmpresasDto> response = new ArrayList<EmpresasDto>();
        Connection connection = null;
        try {
            String sql = "SELECT M.ID_MATRICULA, M.RAZON_SOCIAL, M.TIPOSOCIETARIO, M.ID_DPTO, M.DEPARTAMENTO, " +
                         "M.MUNICIPIO, M.ZONA, M.DIRECCION, M.TELEFONO, M.FAX, M.ACTIVIDAD ";
            sql += "FROM MATRICULADOS M, TIPOS_SOCIETARIOS T ";
            sql += "WHERE M.CRT_TPS = T.CRT_TPS ";

            if(!todosDepartamentos){
                String dptosList = listToString(departamentos);
                String municpsList = listToString(municipios);
                municpsList = municpsList.isEmpty() ? "''" : municpsList;
                sql += "AND (M.ID_DPTO IN (" + dptosList + ") OR M.ID_MUNICIPIO IN (" + municpsList + ")) ";
            }
            if(!todosTiposSociedad){
                String tipSocList = listToString(tiposSocietarios);
                sql += "AND M.CRT_TPS IN (" + tipSocList + ") ";
            }
            if(!todasSecciones){
                String seccionesList = listToString(secciones);
                String clasesList = listToString(clases);
                clasesList = clasesList.isEmpty() ? "''" : clasesList;
                sql += "AND (M.ID_SECCION IN (" + seccionesList + ") OR M.ID_CLASE IN (" + clasesList + ")) ";
            }
            if(!todosEstados) {
                String anhosInscritas = requestDto.getAnhosInscritas();
                String anhosCanceladas = requestDto.getAnhosCanceladas();
                anhosInscritas = anhosInscritas == null ? "" : anhosInscritas;
                anhosCanceladas = anhosCanceladas == null ? "" : anhosCanceladas;

                String auxSql = "";
                String inscritas = "";
                String canceladas = "";
                if(!anhosInscritas.isEmpty()){
                    inscritas = "M.ANO_INSC IN (" + anhosInscritas + ") ";
                    auxSql = inscritas;
                }
                if(!anhosCanceladas.isEmpty()) {
                    auxSql = auxSql.isEmpty() ? "AND (" : ("AND (" + auxSql + "OR ");
                    canceladas = "M.ANO_CANC IN (" + anhosCanceladas + ")";
                    auxSql += canceladas;
                }else{
                    auxSql = auxSql.isEmpty() ? "" : ("AND (" + auxSql + " ");
                }
                auxSql += auxSql.isEmpty()? "" : ")";

                String estadosList = listToString(estados);
                sql += "AND (";
                sql += "M.CTR_ESTADO IN (" + estadosList + ") ";
                sql += auxSql;
                sql += ") ";
            }

            System.out.println("Executing query: " + sql);
            long inicio = System.currentTimeMillis();
            connection = init();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(sql);

            long delta = System.currentTimeMillis() - inicio;
            System.out.println("QUERY executed in " + (delta / 1000) + " seconds");

            if(rs != null){
                inicio = System.currentTimeMillis();
                while(rs.next()){
                    EmpresasDto item = new EmpresasDto();
                    item.setMatricula(rs.getString(1));
                    item.setRazonSocial(rs.getString(2));
                    item.setTipoSocietario(rs.getString(3));
                    item.setDepartamento(rs.getString(4) + "-" + rs.getString(5));
                    item.setMunicipio(rs.getString(6));
                    item.setZona(rs.getString(7));
                    item.setDireccionComercial(rs.getString(8));
                    item.setTelefono(rs.getString(9));
                    item.setFax(rs.getString(10));
                    item.setActividad(rs.getString(11));
                    response.add(item);
                }
                delta = System.currentTimeMillis() - inicio;
                System.out.println("Data iterated in " + (delta / 1000) + " seconds");
                rs.close();
            }
            System.out.println("ListadoEmpresas (Length): " + response.size());
            st.close();

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try{
                connection.close();
            }catch (Exception e){}
        }
        return response;
    }
}