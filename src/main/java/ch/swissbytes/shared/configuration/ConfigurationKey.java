package ch.swissbytes.shared.configuration;

public enum ConfigurationKey {

     MAIL_ENABLESENDING("mail.enablesending"),
     SMTP_SERVER("smtp.server"),
     SMTP_SENDER_ADDRESS("smtp.sender.address"),
     SMTP_USERNAME("smtp.username"),
     SMTP_PASSWORD("smtp.password"),
     SMTP_PORT("smtp.port"),
     SMTP_SSL("smtp.ssl"),
     EMAIL_SENDER_NAME("mail.sender.name"),
     WEB_ROOT_URL("web.root.url"),
     MAIL_SUPPORTEDLOCALES("mail.supportedlocales"),
     MAIL_AMOUNT_MAILING("mailAmountMailing"),
     USER_ADMIN_ID("userAdmin.id"),

     FACEBOOOK_CALLBACK_URL("flyer.facebook.callbak_url"),
     FACEBOOK_APP_ID("flyer.facebook.app_id"),
     FACEBOOK_APP_SECRET("flyer.facebook.app_secret"),
     FACEBOOK_APP_SCOPE("flyer.facebook.scope"),

     MAX_SIZE_FILE("file.max.size"),
     LOGO_SIZE_FILE("logo.max.size"),
     PAYMENT_SIZE_FILE("payment.max.size"),
     MAX_SIZE_FILE_REPORT("file.report.max.size"),
     INITIAL_FLYER_AMOUNT("flyer.load.initial.amount"),
     LOAD_MORE_AMOUNT("flyer.load.more.amount"),
     LIKE_BUTTON_APP_URL("app.url.like.button"),

     IPAY_IFRAME_URL("iPay.iframe.url"),
     IPAY_IFRAME_CIPHER_KEY("iPay.iframe.cipherKey"),
     IPAY_IFRAME_CLIENT_CODE("iPay.iframe.clientCode"),
     IPAY_IFRAME_ENTITY_CODE("iPay.iframe.entityCode"),
     IPAY_WS_URL("iPay.ws.url"),
     IPAY_WS_COMPANY_CODE("iPay.ws.companyCode"),
     IPAY_WS_USERNAME("iPay.ws.username"),
     IPAY_WS_PASSWORD("iPay.ws.password"),
     IPAY_IFRAME_EXPIRATION_MONTHS("iPay.iframe.expiration.months"),

     DOWNLOAD_SMARTPHONE_ANDROID("downloadSmartphone.android"),
     DOWNLOAD_SMARTPHONE_IOS("downloadSmartphone.ios"),
     DOWNLOAD_SMARTPHONE_BLACKBERRY("downloadSmartphone.blackberry"),

     BUSINESS_NAME("business.name"),
     BUSINESS_NIT("business.nit"),

     FTP_FLYERSERVER("ftp.flyerserver"),
     FTP_FLYERSERVER_USER("ftp.flyerserver.user"),
     FTP_FLYERSERVER_PASSWORD("ftp.flyerserver.password"),
     FTP_FLYERSERVER_ROOT_FOLDER("ftp.flyerserver.root.folder"),
     PAYMENT_POINTS_PAGOSNET("paymentPoints.PagosNet"),

    //RSA_KEY_PRIVATE_FILE
     RSA_KEY_PUBLIC_FILE("rsa.key.public.file.dir"),
     RSA_KEY_PRIVATE_FILE("rsa.key.private.file.dir"),

     FILES_ROOT_FOLDER("files.img.root.folder"),//flyerserver
     FILES_FLYERS_FOLDER("files.img.flyers.folder"), //flyer-imgs
     FILES_ACCOUNTS_FOLDER("files.img.accounts.folder"), //account-imgs
     FILES_CATEGORIES_FOLDER("files.img.categories.folder"),//category_img
     FILES_SUSCRIPTIONS_FOLDER("files.img.suscriptions.folder"),//subscriptions-imgs
     FILES_OUTLETS_FOLDER("files.img.outlets.folder"),//outlets-imgs
     FILES_REMOTE_PROTOCOL("files.img.remote.protocol"),//outlets-imgs

     FTP_ENABLED("s3.enabled"),//true
     FTP_SERVER("ftp.server"), //host
     FTP_USER("ftp.user"),//user
     FTP_PASSWORD("ftp.password"), //password
     FTP_ROOT_FOLDER("ftp.root.folder"),

     S3_ENABLED("s3.enabled"),//true
     S3_PROFILE("s3.profile"),// dev / prod
     S3_USER("s3.user"),//id
     S3_PASSWORD("s3.password"), //key
     S3_BUCKET_NAME("s3.bucket.name"),//sincuentos
     S3_ROOT_FOLDER_NAME("s3.root.folder.name"),//flyerserver-dev

     //Tigo Money
     TM_URL_PAYMENT_SUCESS("tm.url.success"),
     TM_URL_PAYMENT_ERROR("tm.url.error"),
     TM_WS_URL_DEV("tm.ws.url.dev"),
     TM_WS_URL_PROD("tm.ws.url.prod"),
     TM_PROFILE("tm.profile"),
     TM_E_WALLET_DEV("tm.ewallet.dev"),
     TM_E_WALLET_PROD("tm.ewallet.prod"),
     TM_PAYMENT_REQUEST_TIMEOUT("tm.payment.request.timeout");

    private final String key;

    private ConfigurationKey(final String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
