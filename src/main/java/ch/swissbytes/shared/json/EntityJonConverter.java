package ch.swissbytes.shared.json;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.util.List;

/**
 * Created by jorgeburgos on 1/9/16.
 */
public abstract class EntityJonConverter<T> {
    public abstract T convertFrom(String Json);

    public abstract JsonElement convertToJsonElement(T entity);

    public JsonElement convertToJsonElement(List<T> entities) {
        JsonArray jsonArray = new JsonArray();

        for (T entity: entities) {
            jsonArray.add(convertToJsonElement(entity));
        }

        return jsonArray;
    }
}
