package ch.swissbytes.shared.security.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by daniel on 17/05/2016.
 */
@Getter
@Setter
public class AccountDto {

    private String userName;
    private String role;
    private String token;
}
