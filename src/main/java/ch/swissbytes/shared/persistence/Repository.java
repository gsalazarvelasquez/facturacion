package ch.swissbytes.shared.persistence;



import ch.swissbytes.shared.filter.PaginatedData;
import ch.swissbytes.shared.filter.PaginationData;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.*;

/**
 * Created by timoteo on 9/9/15.
 */
public abstract class Repository {

    @PersistenceContext
    public EntityManager em;

    protected abstract Class getPersistentClass();

    public <T> Optional<T> findById(final Class<T> clazz, final Long id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> query = cb.createQuery(clazz);
        Root<T> root = query.from(clazz);
        query.where(cb.equal(root.get("id"), id));
        List<T> result = em.createQuery(query).getResultList();
        return result.isEmpty() ? (Optional<T>) Optional.empty() : Optional.of(result.get(0));
    }

    public <T> Optional<T> getById(final Class<T> clazz, final Long id) {
        return findById(clazz, id);
    }


//    public <T> Optional<T> getById(final Class<T> clazz, final long id) {
//        List<T> result = findById(clazz, id);
//        return result.isEmpty() ? Optional.empty() : Optional.of(result.get(0));
//    }

    public <T> Optional<T> getBy(final Class<T> clazz, final String propertyName, final Object value) {
        List<T> list = findEquals(clazz, propertyName, value);
        return list.isEmpty() ? (Optional<T>) Optional.empty() : Optional.of(list.get(0));
    }

    public <T> List<T> findEquals(final Class<T> clazz, final String propertyName, final Object value) {
        return findBy(clazz, new HashMap<String, Object>() {{
            put(propertyName, value);
        }});
    }

    public <T> List<T> findAll(final Class<T> clazz) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> query = cb.createQuery(clazz);
        query.from(clazz);
        return em.createQuery(query).getResultList();
    }

    public <T> void save(final T entity) {
        em.persist(entity);
    }

    public <T> void saveAll(final Collection<T> entities) {
        for (T entity : entities) {
            em.persist(entity);
        }
//        entities.stream().forEach(t -> em.persist(t));
    }

    public <T> void saveAndFlush(final T entity) {
        em.persist(entity);
        em.flush();
    }

    public <T> void saveAndFlushAll(final Collection<T> entities) {
        em.persist(entities);
        em.flush();
    }

    public <T> T merge(final T entity) {
        return em.merge(entity);
    }

    public <T> void deleteAll(final Collection<T> entities) {
//        entities.stream()
//                .filter(t -> t != null)
//                .forEach(t -> em.remove(t));
        for (T t : entities){
            if (t != null){
                em.remove(t);
            }
        }
    }

    public <T> void deleteAndFlush(final T entity) {
        em.remove(entity);
        em.flush();
    }

    public <T> void deleteAndFlushAll(final Collection<T> entities) {
        deleteAll(entities);
        em.flush();
    }

    public <T> Optional<T> getBy(final Class<T> targetClass, final Map<String, Object> properties) {
        Collection<T> list = findBy(targetClass, properties);
        return list.isEmpty() ? (Optional<T>) Optional.empty() : Optional.of(list.iterator().next());
    }

    public <T> Collection<T> findBy(final Class<T> targetClass, final String property, final Object value) {
        return findBy(targetClass, new HashMap<String, Object>() {{
            put(property, value);
        }});
    }

    public <T> List<T> findBy(final Class<T> targetClass, final Map<String, Object> properties) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> query = cb.createQuery(targetClass);
        Root<T> root = query.from(targetClass);
        //query.where(cb.equal(root.get("id"),id));
//        properties.entrySet().stream().forEach(
//                entry -> {
//                    query.where(cb.equal(root.get(entry.getKey()), entry.getValue()));
//                }
//        );

        Iterator<Map.Entry<String, Object>> i = properties.entrySet().iterator();
        while (i.hasNext()){
            Map.Entry<String, Object> entry = i.next();
            query.where(cb.equal(root.get(entry.getKey()), entry.getValue()));
        }

        return em.createQuery(query).getResultList();
    }

    public <T> Number countBy(final Class<T> targetClass, final String property, final Object value) {
        return findBy(targetClass, property, value).size();
    }

    public <T> Number countBy(final Class<T> targetClass, final Map<String, Object> properties) {
        return findBy(targetClass, properties).size();
    }

//    public <T> T retrieveById(final Class<T> targetClass, final long id) {
//        return getById(targetClass, id).get();
//    }

    public <T> T retrieveBy(final Class<T> targetClass, final String propertyName, final Object value) {
        return getBy(targetClass, propertyName, value).get();
    }

    public void clear() {
        em.flush();
        em.clear();
    }

    public void flush() {
        em.flush();
    }

    public <T> List<T> query(String jpaQuery, Map<String, Object> params) {
        Query query = em.createQuery(jpaQuery);
//        params.entrySet().stream().forEach(
//                t -> query.setParameter(t.getKey(), t.getValue())
//        );

        Iterator<Map.Entry<String,Object>> iterator = params.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String,Object> t = iterator.next();
            query.setParameter(t.getKey(), t.getValue());
        }

        return query.getResultList();
    }

    public int update(String jpaQuery, Map<String, Object> params) {
        Query query = em.createQuery(jpaQuery);
//        params.entrySet().stream().forEach(
//                t -> query.setParameter(t.getKey(), t.getValue())
//        );

        Iterator<Map.Entry<String,Object>> iterator = params.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String,Object> t = iterator.next();
            query.setParameter(t.getKey(), t.getValue());
        }

        return query.executeUpdate();
    }


    public <T> T refresh(T entity) {
        em.refresh(entity);
        return entity;
    }


    public <T> T add(T entity) {
        em.persist(entity);
        return entity;
    }

    public <T> void update(T entity) {
        em.merge(entity);
        em.flush();
    }

    public boolean alreadyExists(String propertyName, String propertyValue, Long id) {
        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT 1 FROM " + getPersistentClass().getSimpleName() + " e WHERE e." + propertyName + " = :propertyValue");
        if (id != null) {
            jpql.append(" AND e.id != :id");
        }

        Query query = em.createQuery(jpql.toString());
        query.setParameter("propertyValue", propertyValue);
        if (id != null) {
            query.setParameter("id", id);
        }

        return query.setMaxResults(1).getResultList().size() > 0;
    }

    public boolean alreadyExists(Map<String, Object> properties, Long id) {
        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT 1 FROM " + getPersistentClass().getSimpleName() + " e" );

        if(properties.size() > 0)
            jpql.append(" WHERE 1=1 ");

//        properties.forEach((propertyName, propertyValue) -> {
//            jpql.append("AND e." + propertyName + " = :" + propertyName);
//        });

        Iterator<Map.Entry<String,Object>> iterator = properties.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String,Object> entry = iterator.next();
            jpql.append("AND e." + entry.getKey() + " = :" + entry.getValue());
        }

        if (id != null)
            jpql.append(" AND e.id != :id");

        Query query = em.createQuery(jpql.toString());

//        properties.forEach((propertyName, propertyValue) -> {
//            query.setParameter(propertyName, propertyValue);
//        });

        Iterator<Map.Entry<String,Object>> i = properties.entrySet().iterator();
        while (i.hasNext()){
            Map.Entry<String,Object> entry = i.next();
            query.setParameter(entry.getKey(), entry.getValue());
        }

        if (id != null)
            query.setParameter("id", id);

        return query.setMaxResults(1).getResultList().size() > 0;
    }

    public boolean existsById(Long id) {
        return em.createQuery("SELECT 1 FROM "+getPersistentClass().getSimpleName()+" e WHERE e.id = :id")
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().size() > 0;
    }


    protected <T> PaginatedData<T> findByParameters(String clause,
                                                    PaginationData paginationData, Map<String, Object> queryParameters,
                                                    String defaultSortFieldWithDirection) {


        String clauseSort = " ORDER BY e." + getSortField(paginationData, defaultSortFieldWithDirection);
        Query queryEntities = em.createQuery("SELECT e FROM " + getPersistentClass().getSimpleName() +
                " e " + clause + " " + clauseSort);

        applyQueryParametersOnQuery(queryParameters, queryEntities);
        applyPaginationOnQuery(paginationData, queryEntities);

        List<T> entities = queryEntities.getResultList();

        return new PaginatedData<T>(countWithFilter(clause, queryParameters), entities);

    }

    private int countWithFilter(String clause, Map<String, Object> queryParameters) {
        Query queryCount = em.createQuery("SELECT COUNT(e) FROM " + getPersistentClass().getSimpleName() + " e " + clause);
        applyQueryParametersOnQuery(queryParameters, queryCount);
        return ((Long) queryCount.getSingleResult()).intValue();
    }

    private void applyPaginationOnQuery(PaginationData paginationData, Query query) {
        if(paginationData != null) {
            query.setFirstResult(paginationData.getFirstResult());
            query.setMaxResults(paginationData.getMaxResults());
        }
    }

    private String getSortField(PaginationData paginationData, String defaultSortField) {
        if (paginationData == null || paginationData.getOrderField() == null) {
            return defaultSortField;
        }

        return paginationData.getOrderField() + " " + getSortDirection(paginationData);
    }

    private String getSortDirection(PaginationData paginationData) {
        return paginationData.isAscending() ? "ASC" : "DESC";
    }

    private void applyQueryParametersOnQuery(Map<String, Object> queryParameters, Query query) {
        for (Map.Entry<String, Object> entryMap : queryParameters.entrySet()) {
            query.setParameter(entryMap.getKey(), entryMap.getValue());
        }
    }


}
